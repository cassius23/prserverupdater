#if defined(WIN32)
#pragma warning(disable:4996)
#endif

#include <iostream>
#include <string>
#include <vector>

#include <curl/curl.h>
#include <boost/process.hpp>
#include <boost/filesystem.hpp>

#include "Download.h"
#include "../PRServerUpdater-Core/ConsoleUtils.h"
#include "../PRServerUpdater-Core/ErrorCodes.h"

void Exit(int exitCode)
{
  std::cout << "\nPress enter to continue..." << std::endl;
  std::cin.get();
  exit(exitCode);
}

void PrintHeader()
{
  std::cout << ConsoleUtils::White << "Project Reality: BF2 Server Updater" << ConsoleUtils::Default << "\n";
  std::cout << "Compiled for ";
#if defined(WIN32)
  std::cout << ConsoleUtils::White << "win32" << ConsoleUtils::Default;
#elif defined(LINUX)
  std::cout << ConsoleUtils::White << "linux32" << ConsoleUtils::Default;
#elif defined(LINUX64)
  std::cout << ConsoleUtils::White << "linux64" << ConsoleUtils::Default;
#else
  std::cerr << ConsoleUtils::Red << "No build target defined!!!" << ConsoleUtils::Default << std::endl;
  Exit(PRServerUpdater::ERROR_INVALID_BUILD_TARGET);
#endif
  std::cout << " at " << __TIME__ << " " << __DATE__ << "\n" << std::endl;
}

void DownloadUpdater()
{
  namespace fs = boost::filesystem;

  std::cout << "Downloading latest version of the server updater core..." << std::endl;

#if defined(WIN32)
  int error = Download::ToFile("http://files.realitymod.com/resources/PRServerUpdater-Core-win32.exe", "PRServerUpdater-Core.exe");
#elif defined(LINUX)
  int error = Download::ToFile("http://files.realitymod.com/resources/PRServerUpdater-Core-linux32", "PRServerUpdater-Core");
#elif defined(LINUX64)
  int error = Download::ToFile("http://files.realitymod.com/resources/PRServerUpdater-Core-linux64", "PRServerUpdater-Core");
#endif

  if (error > 0) {
    std::cerr << ConsoleUtils::Red << "Error " << error << ConsoleUtils::Default << std::endl;
    Exit(PRServerUpdater::ERROR_DOWNLOAD_CORE);
  }
  
#if defined(LINUX) | defined(LINUX64)
  fs::permissions("PRServerUpdater-Core", fs::add_perms | fs::owner_exe);
#endif
}

void RunUpdater(int argc, char* argv[])
{
  std::cout << std::endl << "Running server updater core process..." << std::endl;

  namespace bp = boost::process;
  
#if defined(WIN32)
  std::string filename = "PRServerUpdater-Core.exe";
#elif defined(LINUX)
  std::string filename = "PRServerUpdater-Core";
#elif defined(LINUX64)
  std::string filename = "PRServerUpdater-Core";
#endif

  try {
    std::vector<std::string> args(argc + 1);
    args.push_back("--update");
    for (int i = 0; i < argc; i++)
      args.push_back(argv[i]);

    std::cout << std::endl;

    bp::child coreUpdater = bp::execute(
      bp::initializers::run_exe(filename),
      bp::initializers::set_args(args),
      bp::initializers::throw_on_error()
    );
    
    bp::wait_for_exit(coreUpdater);

    std::cout << std::endl;
  } catch (boost::system::system_error const& e) {
    std::cerr << ConsoleUtils::Red << "Error running core process: " << e.what() << ConsoleUtils::Default << std::endl;
    Exit(PRServerUpdater::ERROR_RUN_CORE);
  }
}

void DeleteUpdater()
{
  std::cout << "Deleting server updater core..." << std::endl;

  namespace fs = boost::filesystem;
#if defined(WIN32)
  fs::remove("PRServerUpdater-Core.exe");
#elif defined(LINUX)
  fs::remove("PRServerUpdater-Core");
#elif defined(LINUX64)
  fs::remove("PRServerUpdater-Core");
#endif
}

int main(int argc, char* argv[])
{
  try {
    boost::filesystem::current_path(boost::filesystem::system_complete(argv[0]).parent_path());

    ConsoleUtils::InitializeColors();

    PrintHeader();

    DownloadUpdater();
    RunUpdater(argc, argv);
    DeleteUpdater();
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "\nUnhandled exception!\n" << e.what() << ConsoleUtils::Default << std::endl;
    Exit(PRServerUpdater::ERROR_UNKNOWN);
  }
  
  std::cout << ConsoleUtils::White << "\nDone!" << ConsoleUtils::Default << std::endl;
  Exit(PRServerUpdater::SUCCESS);

  return PRServerUpdater::SUCCESS;
}
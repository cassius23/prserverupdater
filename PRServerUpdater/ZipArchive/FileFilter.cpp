////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#if defined _MSC_VER && _MSC_VER < 1300	
	// STL warnings 	
	#pragma warning (push, 3) 
#endif

#include "FileFilter.h"

namespace ZipArchiveLib
{

bool CGroupFileFilter::Accept(LPCTSTR lpszParentDir, LPCTSTR lpszName, const CFileInfo& info)
{
	bool conditionToBreak;
	bool valueToReturn;

	// handle the evaluation as quickly as possible
	if (m_iType == CGroupFileFilter::And)
	{
		conditionToBreak = false;
		valueToReturn = m_bInverted;
	}
	else
	{
		conditionToBreak = true;
		valueToReturn = !m_bInverted;
	}

	for (ZIP_ARRAY_SIZE_TYPE i = 0; i < m_filters.GetSize(); i++)
	{
		CFileFilter* pFilter = m_filters[i];
		if (pFilter->HandlesFile(info) && pFilter->Evaluate(lpszParentDir, lpszName, info) == conditionToBreak)
			return valueToReturn;
	}

	return !valueToReturn;
	
}

} // namespace


#if defined _MSC_VER && _MSC_VER < 1300
	// STL warnings 
	#pragma warning (pop) 
#endif

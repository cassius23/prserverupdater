////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ZipCryptograph.h"
#include "ZipAesCryptograph.h"
#include "ZipCrc32Cryptograph.h"

CZipCryptograph* CZipCryptograph::CreateCryptograph(int iEncryptionMethod)
{
	if (iEncryptionMethod == encNone)
		return NULL;
#ifdef _ZIP_AES
	if (IsWinZipAesEncryption(iEncryptionMethod))
		return new CZipAesCryptograph(iEncryptionMethod);
	else
#endif
		return new CZipCrc32Cryptograph();
}


DWORD CZipCryptograph::GetEncryptedInfoSize(int iEncryptionMethod)
{
	if (iEncryptionMethod != encNone)
	{
		if (iEncryptionMethod == encStandard)
			return CZipCrc32Cryptograph::GetEncryptedInfoSizeBeforeData() + CZipCrc32Cryptograph::GetEncryptedInfoSizeAfterData();
#ifdef _ZIP_AES
		else if (IsWinZipAesEncryption(iEncryptionMethod))
			return CZipAesCryptograph::GetEncryptedInfoSizeBeforeData(iEncryptionMethod) + CZipAesCryptograph::GetEncryptedInfoSizeAfterData();
#endif
	}
	return 0;
}

DWORD CZipCryptograph::GetEncryptedInfoSizeBeforeData(int iEncryptionMethod)
{
	if (iEncryptionMethod != encNone)
	{
		if (iEncryptionMethod == encStandard)
			return CZipCrc32Cryptograph::GetEncryptedInfoSizeBeforeData();
#ifdef _ZIP_AES
		else if (IsWinZipAesEncryption(iEncryptionMethod))
			return CZipAesCryptograph::GetEncryptedInfoSizeBeforeData(iEncryptionMethod);	
#endif
	}
	return 0;
}


DWORD CZipCryptograph::GetEncryptedInfoSizeAfterData(int iEncryptionMethod)
{
	if (iEncryptionMethod != encNone)
	{
		if (iEncryptionMethod == encStandard)
			return CZipCrc32Cryptograph::GetEncryptedInfoSizeAfterData();
#ifdef _ZIP_AES
		else if (IsWinZipAesEncryption(iEncryptionMethod))
			return CZipAesCryptograph::GetEncryptedInfoSizeAfterData();	
#endif
	}
	return 0;
}

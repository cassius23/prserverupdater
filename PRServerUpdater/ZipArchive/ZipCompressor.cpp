////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ZipCompressor.h"
#include "BytesWriter.h"
#include "DeflateCompressor.h"
#ifdef _ZIP_BZIP2
	#include "Bzip2Compressor.h"
#endif

using namespace ZipArchiveLib;

CZipCompressor* CZipCompressor::CreateCompressor(WORD uMethod, CZipStorage* pStorage)
{
	if (uMethod == methodStore || uMethod == methodDeflate)
		return new CDeflateCompressor(pStorage);
#ifdef _ZIP_BZIP2
	else if (uMethod == methodBzip2)
		return new CBzip2Compressor(pStorage);
#endif

	return NULL;
}

void CZipCompressor::UpdateFileCrc(const void *pBuffer, DWORD uSize)
{
	m_pFile->m_uCrc32 = (DWORD)zarch_crc32(m_pFile->m_uCrc32, (zarch_Bytef*)pBuffer, uSize);
}

void CZipCompressor::UpdateCrc(const void *pBuffer, DWORD uSize)
{
	m_uCrc32 = (DWORD)zarch_crc32(m_uCrc32, (zarch_Bytef*)pBuffer, uSize);
}

void CZipCompressor::UpdateOptions(const COptionsMap& optionsMap)
{
	const COptions* pOptions = GetOptions();
	if (pOptions == NULL)
		return;
	const COptions* pNewOptions = optionsMap.Get(pOptions->GetType());
	if (pNewOptions != NULL)
		UpdateOptions(pNewOptions);
}

void CZipCompressor::InitBuffer()
{
	// This should be greater that 64k for deflate when creating offsets pairs is enabled
	// otherwise deflate will not be able to write one block in one go and will never report
	// a flushed block for low-compressable data
	const COptions* pOptions = GetOptions();
	DWORD bufferSize = 0;
	if (pOptions != NULL)
        bufferSize = pOptions->m_iBufferSize;
	if (bufferSize == 0)
		bufferSize = COptions::cDefaultBufferSize;
	m_pBuffer.Allocate(bufferSize);
}

#ifdef _ZIP_SEEK

bool CZipCompressor::PrepareForSeek(CZipFileHeader* pCurrentFile, COffsetsPair* pPair)
{
	if (pCurrentFile->IsEncrypted())
	{
		ZIPTRACE("%s(%i) : ZipArchive cannot seek in an encrypted data.\n");
		return false;
	}
	const COptions* pOptions = GetOptions();
	if (pOptions == NULL || pOptions->GetType() != typeDeflate)
	{
		ZIPTRACE("%s(%i) : The current compressor is unsupported for seeking.\n");
		return false;
	}	
	
	if (m_uUncomprLeft != pCurrentFile->m_uUncomprSize)
	{
		// some part of the file was already decompressed, reinitialize the decompressor
		FinishDecompression(true);
		// cryptograph should be null, the file is not encrypted
		InitDecompression(pCurrentFile, NULL);
	}

	if (m_uUncomprLeft < pPair->m_uUncompressedOffset || m_uComprLeft < pPair->m_uCompressedOffset)
	{
		ZIPTRACE("%s(%i) : Cannot seek beyond the compression stream.\n");
		return false;
	}

	m_uUncomprLeft -= pPair->m_uUncompressedOffset;
	m_uComprLeft -= pPair->m_uCompressedOffset;

	return true;
}
#endif

void CZipCompressor::COptionsMap::Set(const CZipCompressor::COptions* pOptions)
{
	if (pOptions == NULL)
		return;
	int iType = pOptions->GetType();
	Remove(iType);
	SetAt(iType, pOptions->Clone());
}

CZipCompressor::COptions* CZipCompressor::COptionsMap::Get(int iType) const
{
	COptions* pTemp = NULL;
	if (Lookup(iType, pTemp))
		return pTemp;
	else
		return NULL;
}

void CZipCompressor::COptionsMap::Remove(int iType)
{
	COptions* pTemp = Get(iType);
	if (pTemp != NULL)
	{
		delete pTemp;
		RemoveKey(iType);
	}	
}

CZipCompressor::COptionsMap::~COptionsMap()
{
	COptionsMap::iterator iter = GetStartPosition();
	while (IteratorValid(iter))
	{
		COptions* pOptions = NULL;
		int iDummyType;
		GetNextAssoc(iter, iDummyType, pOptions);
		delete pOptions;
	}
	RemoveAll();
}

#ifdef _ZIP_SEEK

void CZipCompressor::COffsetsArray::Clear()
{
	ZIP_ARRAY_SIZE_TYPE size = GetSize();
	for (ZIP_ARRAY_SIZE_TYPE i = 0; i < size; i++)
		delete GetAt(i);
	RemoveAll();
}

CZipCompressor::COffsetsArray::CStat CZipCompressor::COffsetsArray::GetStatistics() const
{
	CStat stat;
	stat.m_MinValues.m_uCompressedOffset = ZIP_SIZE_TYPE(-1);
	stat.m_MinValues.m_uUncompressedOffset = ZIP_SIZE_TYPE(-1);
	ZIP_ARRAY_SIZE_TYPE size = GetSize();
	ZIP_SIZE_TYPE uLastCompressedOffset = 0;
	ZIP_SIZE_TYPE uLastDataOffset = 0;
	for (ZIP_ARRAY_SIZE_TYPE i = 0; i < size; i++)
	{
		CZipCompressor::COffsetsPair* pPair = GetAt(i);
		ZIP_SIZE_TYPE uCompressedOffset = pPair->m_uCompressedOffset - uLastCompressedOffset;
		ZIP_SIZE_TYPE uDataOffset = pPair->m_uUncompressedOffset - uLastDataOffset;

		uLastCompressedOffset = pPair->m_uCompressedOffset;
		uLastDataOffset = pPair->m_uUncompressedOffset;

		if (uCompressedOffset < stat.m_MinValues.m_uCompressedOffset)
			stat.m_MinValues.m_uCompressedOffset = uCompressedOffset;
		if (uDataOffset < stat.m_MinValues.m_uUncompressedOffset)
			stat.m_MinValues.m_uUncompressedOffset = uDataOffset;

		if (uCompressedOffset > stat.m_MaxValues.m_uCompressedOffset)
			stat.m_MaxValues.m_uCompressedOffset = uCompressedOffset;
		if (uDataOffset > stat.m_MaxValues.m_uUncompressedOffset)
			stat.m_MaxValues.m_uUncompressedOffset = uDataOffset;

		stat.m_AvgValues.m_uCompressedOffset += (ZIP_SIZE_TYPE)uCompressedOffset;
		stat.m_AvgValues.m_uUncompressedOffset += (ZIP_SIZE_TYPE)uDataOffset;		
	}
	stat.m_AvgValues.m_uCompressedOffset /= (ZIP_SIZE_TYPE)size;
	stat.m_AvgValues.m_uUncompressedOffset /= (ZIP_SIZE_TYPE)size;

	return stat;
}

void CZipCompressor::COffsetsArray::Save(CZipAutoBuffer& buffer) const
{
	int iCount, iHeader;
	ZIP_ARRAY_SIZE_TYPE uSize = GetSize();
	ZIP_ARRAY_SIZE_TYPE i;
#ifdef _ZIP_ZIP64

	bool bLarge = false;
	for (i = 0; i < uSize; i++)
	{
		COffsetsPair* pPair = GetAt(i);
		if (pPair->m_uCompressedOffset >= UINT_MAX || pPair->m_uUncompressedOffset >= UINT_MAX)
		{
			bLarge = true;
			break;
		}
	}
	if (bLarge)
	{
		iCount = 8;
		iHeader = LargeSizes;
	}
	else
	{
#endif
		iCount = 4;
		iHeader = Default;
#ifdef _ZIP_ZIP64
	}
#endif

	DWORD uTotal = (DWORD)uSize * 2 * iCount + HeaderSize + CountSize;

	buffer.Allocate(uTotal);
	char* pBuffer = buffer;
	pBuffer[0] = (char)iHeader;
	pBuffer++;
	CBytesWriter::WriteBytes(pBuffer, (DWORD)uSize);
	pBuffer += CountSize;

	for (i = 0; i < uSize; i++)
	{
		COffsetsPair* pPair = GetAt(i);
		CBytesWriter::WriteBytes(pBuffer, pPair->m_uUncompressedOffset, iCount);
		pBuffer += iCount;
		CBytesWriter::WriteBytes(pBuffer, pPair->m_uCompressedOffset, iCount);
		pBuffer += iCount;
	}
}

void CZipCompressor::COffsetsArray::Load(const CZipAutoBuffer& buffer)
{
	Clear();
	if (buffer.GetSize() < (HeaderSize + CountSize))
		CZipException::Throw();

	const char* pBuffer = buffer;
	int iCount;
	if (pBuffer[0] == (char)LargeSizes)
#ifdef _ZIP_ZIP64
		iCount = 8;
#else
		CZipException::Throw(CZipException::noZip64);
#endif
	else
		iCount = 4;
	DWORD size;
	pBuffer++;
	CBytesWriter::ReadBytes(size, pBuffer);
	pBuffer += CountSize;
	if (buffer.GetSize() < size * 2 * iCount + HeaderSize + CountSize)
		CZipException::Throw();
	for (ZIP_ARRAY_SIZE_TYPE i = 0; i < (ZIP_ARRAY_SIZE_TYPE)size; i++)
	{
		CZipCompressor::COffsetsPair* pPair = new CZipCompressor::COffsetsPair();
		CBytesWriter::ReadBytes(pPair->m_uUncompressedOffset, pBuffer, iCount);
		pBuffer += iCount;
		CBytesWriter::ReadBytes(pPair->m_uCompressedOffset, pBuffer, iCount);
		pBuffer += iCount;
		Add(pPair);
	}
}

CZipCompressor::COffsetsPair* CZipCompressor::COffsetsArray::FindMax(ZIP_SIZE_TYPE offset, bool uncompressed)
{
	ZIP_ARRAY_SIZE_TYPE size = GetSize();

	COffsetsPair* pair;
	ZIP_ARRAY_SIZE_TYPE i = size;
	while (i > 0)
	{
		i--;
		pair = GetAt(i);
		if (uncompressed)
		{
			if (pair->m_uUncompressedOffset <= offset)
				return pair;
		}
		else if (pair->m_uCompressedOffset <= offset)
			return pair;				 
	}
	return NULL;
}

CZipCompressor::COffsetsPair* CZipCompressor::COffsetsArray::FindMin(ZIP_SIZE_TYPE offset, bool uncompressed)
{
	ZIP_ARRAY_SIZE_TYPE size = GetSize();
	COffsetsPair* pair;
	for (ZIP_ARRAY_SIZE_TYPE i = 0; i < size; i++)
	{
		pair = GetAt(i);
		if (uncompressed)
		{
			if (pair->m_uUncompressedOffset >= offset)
				return pair;
		}
		else if (pair->m_uCompressedOffset >= offset)
			return pair;				 
	}
	return NULL;
}

#endif

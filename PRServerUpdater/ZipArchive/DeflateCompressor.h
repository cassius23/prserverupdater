////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

/**
* \file DeflateCompressor.h
*	Includes the ZipArchiveLib::CDeflateCompressor class.
*
*/

#if !defined(ZIPARCHIVE_DEFLATECOMPRESSOR_DOT_H)
#define ZIPARCHIVE_DEFLATECOMPRESSOR_DOT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "ZipExport.h"
#include "BaseLibCompressor.h"
#include "ZipException.h"
#include "zlib/zlib.h"

namespace ZipArchiveLib
{

/**
	Compresses and decompresses data using the Zlib library.
*/
class ZIP_API CDeflateCompressor : public CBaseLibCompressor
{			
public:	
	/**
		Represents options of the CDeflateCompressor.

		\see
			<a href="kb">0610231446|options</a>
		\see
			CZipArchive::SetCompressionOptions
	*/
	struct ZIP_API COptions : CBaseLibCompressor::COptions
	{
		COptions()
		{
			m_bCheckLastBlock = true;
#ifdef _ZIP_SEEK
			m_iSyncRatio = 0;
			m_bCreateOffsetsArray = true;
#endif
		}

		int GetType() const
		{
			return typeDeflate;
		}

		CZipCompressor::COptions* Clone() const
		{
			return new COptions(*this);
		}

		/**
			Enables or disables checking, if the compressed data ends with an end-of-stream block.
			This should be enabled to protect against malformed data.
			\c true, if the checking of the last block should be enabled; \c false otherwise.
		*/
		bool m_bCheckLastBlock;	

#ifdef _ZIP_SEEK
		/**
			Determines how often synchronization blocks are created.
			- \c 0 - disables creation of synchronization blocks,
			- the larger the value, the rarer the synchronization blocks are created and the better the compression ratio is.

			\note
				This setting has no effect on stored or encrypted data.
				The synchronization blocks will not be created in these cases regardless of the value set.
			\note
				You can use CZipCompressor::COffsetsArray::GetStatistics to determine the best value.

			\see 
				<a href="kb">0711101739|creating</a>
			\see
				m_bCreateOffsetsArray
			\see
				CDeflateCompressor::GetOffsetsArray
		*/
		int m_iSyncRatio;

		/**
			Specifies whether the CZipCompressor::COffsetsArray is created while creating synchronization blocks.
			\c true, if the offsets array should be created; \c false otherwise.

			\note
				Normally, this value should be always set to \c true.
			
			\see 
				<a href="kb">0711101739|creating</a>
			\see
				CDeflateCompressor::GetOffsetsArray
			\see
				m_iSyncRatio
		*/
		bool m_bCreateOffsetsArray;
#endif
	};	

	/**
		Initializes a new instance of the CDeflateCompressor class.

		\param pStorage
			The current storage object.
	*/
	CDeflateCompressor(CZipStorage* pStorage);

	bool CanProcess(WORD uMethod) {return uMethod == methodStore || uMethod == methodDeflate;}

	void InitCompression(int iLevel, CZipFileHeader* pFile, CZipCryptograph* pCryptograph);
	void InitDecompression(CZipFileHeader* pFile, CZipCryptograph* pCryptograph);

	DWORD Decompress(void *pBuffer, DWORD uSize);
	void Compress(const void *pBuffer, DWORD uSize);
	
	void FinishCompression(bool bAfterException);
	void FinishDecompression(bool bAfterException);

#ifdef _ZIP_SEEK
	/**
		Retrieves the offsets array that gives the location of the synchronization blocks. This method should
		be called just after compressing a file. The array pointer will be invalid with the next compression 
		operation or when a file is decompressed with a method different than the deflate method or when an archive is closed.
		You can save the array to a buffer with the CZipCompressor::COffsetsArray::Save method.

		\return
			The offsets array created for the recently compressed file.
		
		\see 
			<a href="kb">0711101739</a>
		\see			
			CZipCompressor::COffsetsArray
		\see
			COptions::m_iSyncRatio
		\see
			COptions::m_bCreateOffsetsArray
	*/
	COffsetsArray* GetOffsetsArray() const
	{
		return m_pOffsetsArray;
	}
#endif

	const CZipCompressor::COptions* GetOptions() const
	{
		return &m_options;
	}

	~CDeflateCompressor()
	{
#ifdef _ZIP_SEEK
		ClearMap();		
#endif
	}
protected:
	void UpdateOptions(const CZipCompressor::COptions* pOptions)
	{
		m_options = *(COptions*)pOptions;
	}
	
	int ConvertInternalError(int iErr) const
	{
		switch (iErr)
		{
		case Z_NEED_DICT:
			return CZipException::needDict;
		case Z_STREAM_END:
			return CZipException::streamEnd;
		case Z_ERRNO:
			return CZipException::errNo;
		case Z_STREAM_ERROR:
			return CZipException::streamError;
		case Z_DATA_ERROR:
			return CZipException::dataError;
		case Z_MEM_ERROR:
			return CZipException::memError;
		case Z_BUF_ERROR:
			return CZipException::bufError;
		case Z_VERSION_ERROR:
			return CZipException::versionError;
		default:
			return CZipException::genericError;
		}
	}

	bool IsCodeErrorOK(int iErr) const
	{
		return iErr == Z_OK || iErr == Z_NEED_DICT;
	}

private:			
#ifdef _ZIP_SEEK
	void ClearMap();
	COffsetsArray* m_pOffsetsArray;
	int m_iPartsDone;
#endif
	COptions m_options;
	zarch_z_stream m_stream;
};

} // namespace

#endif


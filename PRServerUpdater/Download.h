#pragma once

#include <vector>
#include <curl/curl.h>

typedef unsigned char byte;

class Download
{
  public:
    static int ToFile(const char* url, const char* destination, const char* interfaceIP = NULL);
  private:
    struct ProgressData {
      double LastRunTime;
      CURL* Curl;
    };

    static int Progress(void*, double, double, double, double);
    static size_t WriteData(void*, size_t, size_t, void*);
    static size_t WriteFile(void*, size_t, size_t, void*);

    static double PROGRESS_UPDATE_INTERVAL;
};


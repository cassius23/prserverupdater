#include "Download.h"

#include <boost/filesystem.hpp>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>
#include <cryptopp/sha.h>
#include <stdlib.h>

#include "../PRServerUpdater-Core/ConsoleUtils.h"

namespace fs = boost::filesystem;

double Download::PROGRESS_UPDATE_INTERVAL = 0.1;

int Download::ToFile(const char* url, const char* destination, const char* interfaceIP)
{
  CURL *curl;
  CURLcode res = CURLE_OK;
  struct ProgressData prog;

  FILE *destinationFile;

  curl = curl_easy_init();
  if (curl) {
    prog.LastRunTime = 0;
    prog.Curl = curl;

    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, Progress);
    curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, &prog);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteFile);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "PRServerUpdater/1.0.0.0");
    if (interfaceIP != NULL)
      curl_easy_setopt(curl, CURLOPT_INTERFACE, interfaceIP);

#if defined(WIN32)
    fopen_s(&destinationFile, destination, "wb");
#elif defined(LINUX) || defined(LINUX64)
    destinationFile = fopen(destination, "wb");
#endif

    if (destinationFile != NULL) {
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, destinationFile);

      res = curl_easy_perform(curl);

      fclose(destinationFile);
      ConsoleUtils::FreeProgress();
    } else {
      std::cerr << std::endl << "Unable to create file: " << destination << std::endl;
      curl_easy_cleanup(curl);
      return 1;
    }

    if (res != CURLE_OK)
      std::cerr << std::endl << curl_easy_strerror(res) << std::endl;

    curl_easy_cleanup(curl);

    return (int)res;
  } else {
    return 1;
  }
}

size_t Download::WriteData(void* ptr, size_t size, size_t nmemb, void* stream)
{
  size_t realsize = size * nmemb;
  std::vector<byte>* memory = (std::vector<byte>*)stream;
  
  memory->insert(memory->end(), (byte*)ptr, (byte*)ptr + realsize);
  return realsize;
}

size_t Download::WriteFile(void* ptr, size_t size, size_t nmemb, void* stream)
{
  int written = fwrite(ptr, size, nmemb, (FILE*)stream);
  return written;
}

int Download::Progress(void* p, double dltotal, double dlnow, double ultotal, double ulnow)
{
  struct ProgressData* prog = (struct ProgressData*)p;
  CURL* curl = prog->Curl;
  double currentTime = 0;

  curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &currentTime);

  if((currentTime - prog->LastRunTime) >= PROGRESS_UPDATE_INTERVAL) {
    prog->LastRunTime = currentTime;

    if (dltotal > 0)
      ConsoleUtils::FileProgress(dlnow, dltotal, currentTime);
  } else if (dltotal > 0 && dlnow == dltotal) {
      ConsoleUtils::FileProgress(dlnow, dltotal, currentTime);
  }

  return 0;
}
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/regex.hpp>

#include "Directories.h"
#include "Version.h"

Version::Version()
  : Major(0), Minor(0), Build(0), Revision(0)
{
}

Version::Version(std::string const& input)
{
  const char* vinput = input.c_str();

  std::istringstream parser(input.length() == 0 ? "0.0.0.0" : vinput);

  unsigned int versionInfo[4];

  parser >> versionInfo[0];

  if (parser.fail()) {
    SetZero();
    return;
  } else {
    Major = versionInfo[0];
  }

  for (int i = 1; i < 4; i++) {
    parser.get(); //Skip period
    if (parser.eof())
      versionInfo[i] = 0;
    else {
      parser >> versionInfo[i];
      if (parser.fail()) {
        SetZero(4 - i);
        return;
      } else {
        if (i == 1)
          Minor = versionInfo[i];
        else if (i == 2)
          Build = versionInfo[i];
        else if (i == 3)
          Revision = versionInfo[i];
      }
    }
  }
}

Version::Version(unsigned int major)
  : Major(major), Minor(0), Build(0), Revision(0)
{
}

Version::Version(unsigned int major, unsigned int minor)
  : Major(major), Minor(minor), Build(0), Revision(0)
{
}

Version::Version(unsigned int major, unsigned int minor, unsigned int build)
  : Major(major), Minor(minor), Build(build), Revision(0)
{
}

Version::Version(unsigned int major, unsigned int minor, unsigned int build, unsigned int revision)
  : Major(major), Minor(minor), Build(build), Revision(revision)
{
}

std::string Version::CurrentVersion()
{
  return ReadVersion((Directories::GetCurrentModDirectory() / "mod.desc").string());
}

std::string Version::ReadVersion(std::string path)
{
  const char* vpath = path.c_str();
  std::ifstream in(vpath, std::ios::in);
  std::stringstream buffer;
  buffer << in.rdbuf();

  boost::property_tree::ptree modpt;
  boost::property_tree::read_xml(buffer, modpt);

  std::string currentVersion = modpt.get<std::string>("mod.version");
  boost::algorithm::trim(currentVersion);

  return currentVersion;
}

void Version::WriteVersion(std::string path, Version version)
{
  const char* vpath = path.c_str();
  std::ifstream in(vpath, std::ios::in);
  std::stringstream buffer;
  buffer << in.rdbuf();
  
  std::string modDesc = buffer.str();

  std::ostringstream versionReplacement;
  versionReplacement << "<version> " << version << " </version>";

  boost::regex regexVersion("<version>.+?</version>", boost::regex::icase);
  std::string result = boost::regex_replace(modDesc, regexVersion, versionReplacement.str());
  
  std::ofstream out(vpath, std::ios::out);
  out << result;
}

bool Version::IsZero() const
{
  return *this == Version();
}

void Version::SetZero()
{
  Major = 0;
  Minor = 0;
  Build = 0;
  Revision = 0;
}

void Version::SetZero(int elements)
{
  if (elements == 4)
    Major = 0;
  if (elements >= 3)
    Minor = 0;
  if (elements >= 2)
    Build = 0;
  if (elements >= 1)
    Revision = 0;
}

bool Version::operator<(Version const& rhs) const
{
  unsigned int versionInfo[4] = { Major, Minor, Build, Revision };
  unsigned int rhsVersionInfo[4] = { rhs.Major, rhs.Minor, rhs.Build, rhs.Revision };
  return std::lexicographical_compare(versionInfo, versionInfo + 4, rhsVersionInfo, rhsVersionInfo + 4);
}

bool Version::operator>(Version const& rhs) const
{
  return rhs < *this;
}

bool Version::operator==(Version const& rhs) const
{
  return Major == rhs.Major &&
    Minor == rhs.Minor &&
    Build == rhs.Build &&
    Revision == rhs.Revision;
}

bool Version::operator!=(Version const& rhs) const
{
  return !(*this == rhs);
}

bool Version::operator<=(Version const& rhs) const
{
  return *this < rhs || *this == rhs;
}

bool Version::operator>=(Version const& rhs) const
{
  return *this > rhs || *this == rhs;
}

std::ostream& operator<<(std::ostream& os, const Version& dt)
{
  os << dt.Major << "." << dt.Minor << "." << dt.Build << "." << dt.Revision;
  return os;
}
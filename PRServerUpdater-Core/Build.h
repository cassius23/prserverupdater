#pragma once

#include <ctime>
#include <cstdlib>
#include <map>
#include <string>
#include <vector>
#include "JsonTypes.h"
#include "ZipArchive/ZipArchive.h"

typedef unsigned char byte;

class ZipEntry
{
  public:
    ZipEntry() :
      Data(NULL), Size(((ULONGLONG)-1))
    {
    }

    ZipEntry(byte* data, ULONGLONG size) :
      Data(data), Size(size)
    {
    }
    
    ~ZipEntry()
    {
      if (Data != NULL && Size != ((ULONGLONG)-1)) {
        free(Data);
        Data = NULL;
        Size = ((ULONGLONG)-1);
      }
    }

    byte* Data;
    ULONGLONG Size;
};

struct PatchedZipFile
{
  std::string TempFileName;
  std::string FileName;
  std::string ShortFileName;
  CZipArchive* Zip;
  ULONGLONG BufferSize;
  bool Closed;
};

class Build
{
  public:
    typedef std::map<std::string, std::string> ImportantFilesType;

    static bool LoadData(Patch& patch, const char* path, const char* password = NULL);
    static int ApplyPatch(Patch patch, const char* path, ImportantFilesType& importantFiles, const char* password = NULL);
  private:
    typedef std::map<std::string, PatchedZipFile> ArchiveMapType;

    static bool ReadPatchLicense(Patch patch, CZipArchive& zip);
    static bool ApplyPatchFiles(std::vector<PatchItem> patchData, CZipArchive& zip, PatchTouchData touchData, ArchiveMapType& modifiedZipArchives, ImportantFilesType& importantFiles);
    static bool ApplyPatchFilesAdd(ZipEntry& source, std::string destination, std::string shortDestination);
    static bool ApplyPatchFilesModify(ZipEntry& source, std::string destination, std::string shortDestination, std::string beforeHash, std::string afterHash);
    static bool ApplyPatchFilesRemove(std::string destination, std::string shortDestination, bool isDirectory);
    static bool ApplyPatchZipAdd(ZipEntry& source, std::string destination, std::string entryName, std::string shortDestination, std::string shortDestinationArchive, time_t timestamp, std::map<std::string, PatchedZipFile>& modifiedZipArchives);
    static bool ApplyPatchZipModify(ZipEntry& source, std::string destination, std::string entryName, std::string shortDestination, std::string shortDestinationArchive, std::string beforeHash, std::string afterHash, time_t timestamp, ArchiveMapType& modifiedZipArchives);
    static bool ApplyPatchZipRemove(std::string destination, std::string entryName, std::string shortDestination, std::string shortDestinationArchive, ArchiveMapType& modifiedZipArchives);
    static ZipEntry PatchReadEntry(CZipArchive& zip, std::string source);
    static ZipEntry PatchReadEntry(std::string tempDestinationArchive, std::string destinationArchive, std::string shortDestinationArchive, std::string entryName, ArchiveMapType& modifiedZipArchives);
    static bool PatchAddEntry(std::string tempDestinationArchive, std::string destinationArchive, std::string shortDestinationArchive, std::string entryName, byte* data, size_t length, time_t timestamp, ArchiveMapType& modifiedZipArchives);
    static bool PatchRemoveEntry(std::string tempDestinationArchive, std::string destinationArchive, std::string shortDestinationArchive, std::string entryName, ArchiveMapType& modifiedZipArchives);
    static bool TouchArchive(CZipArchive& zip, time_t timestamp);
    static bool TouchArchive(std::string path, time_t timestamp);
    static void SafeMove(std::string sourceFileName, std::string targetFileName, std::string destinationBackupFileName);
    static void AttemptRestore(std::string destination, bool hasWritten);
    static void ClearAttributes(std::string target);
    static time_t from_iso_extended_string(const std::string& inDateString);

    static std::string _patchTempExtension;
    static std::string _patchOldExtension;
    static std::string _patchOriginalExtension;
    static ULONGLONG _maximumBufferSize;
};

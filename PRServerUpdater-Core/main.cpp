#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/regex.hpp>
#include <curl/curl.h>
#include "Build.h"
#include "ConsoleUtils.h"
#include "Directories.h"
#include "Download.h"
#include "EncryptedData.h"
#include "ErrorCodes.h"
#include "Exception.h"
#include "License.h"
#include "JsonTypes.h"
#include "Version.h"

void PrintHeader()
{
  std::cout << ConsoleUtils::White << "Project Reality: BF2 Server Updater - Core" << ConsoleUtils::Default << "\n";
  std::cout << "Compiled for ";
#if defined(WIN32)
  std::cout << ConsoleUtils::White << "win32" << ConsoleUtils::Default;
#elif defined(LINUX)
  std::cout << ConsoleUtils::White << "linux32" << ConsoleUtils::Default;
#elif defined(LINUX64)
  std::cout << ConsoleUtils::White << "linux64" << ConsoleUtils::Default;
#else
  std::cerr << "No build target defined!!!" << std::endl;
  exit(PRServerUpdater::ERROR_INVALID_BUILD_TARGET);
#endif
  std::cout << " at " << __TIME__ << " " << __DATE__ << "\n" << std::endl;
}

Version DownloadLatestVersion(bool encrypted)
{
  boost::posix_time::ptime current_date_microseconds = boost::posix_time::microsec_clock::local_time();
  boost::date_time::time_duration<boost::posix_time::time_duration, boost::posix_time::time_res_traits>::tick_type milliseconds = current_date_microseconds.time_of_day().total_milliseconds();

  std::vector<byte> latestVersionJsonBytes;
  int error = Download::ToMemory((boost::format("http://%1%.cdn.ancientdev.com/version.%2%?timestamp=%3%") % (encrypted ? "prbf2-test" : "prbf2") % (encrypted ? "predata" : "json") % (milliseconds)).str().c_str(), latestVersionJsonBytes);
  if (error > 0) {
    std::cerr << ConsoleUtils::Red << "Unable to get latest version info: " << error << ConsoleUtils::Default << std::endl;
    exit(PRServerUpdater::ERROR_DOWNLOAD_LATEST_VERSION);
  }
  
  std::string latestVersionJson;
  if (encrypted) {
    latestVersionJson = EncryptedData::Read(&latestVersionJsonBytes[0], latestVersionJsonBytes.size());
  } else {
    latestVersionJson = std::string(latestVersionJsonBytes.begin(), latestVersionJsonBytes.end());
  }

  std::istringstream iss(latestVersionJson);
  boost::property_tree::ptree pt;
  boost::property_tree::read_json(iss, pt);
  
  Version version = Version(pt.get<std::string>("Latest"));
  std::cout << ConsoleUtils::DarkCyan << version << ConsoleUtils::Default << std::endl;
  
  return version;
}

UpdateData GetVersionInfo(Version version, bool encrypted)
{
  UpdateData versionInfo;
  
  std::vector<byte> jsonBytes;
  int error = Download::ToMemory((boost::format("http://%1%.cdn.ancientdev.com/patch_%2%.%3%") % (encrypted ? "prbf2-test" : "prbf2") % version % (encrypted ? "predata" : "json")).str().c_str(), jsonBytes);
  if (error > 0) {
    std::cerr << ConsoleUtils::Red << "Error getting version info: " << error << ConsoleUtils::Default << std::endl;
    exit(PRServerUpdater::ERROR_DOWNLOAD_VERSION_INFO);
  }
  
  std::string json;
  if (encrypted) {
    json = EncryptedData::Read(&jsonBytes[0], jsonBytes.size());
  } else {
    json = std::string(jsonBytes.begin(), jsonBytes.end());
  }

  std::istringstream iss(json);
  boost::property_tree::ptree pt;
  boost::property_tree::read_json(iss, pt);
  
  versionInfo.FromVersion = Version(pt.get<std::string>("Requires"));
  versionInfo.ToVersion = version;
  versionInfo.Size = pt.get<long>("ServerSize");
  versionInfo.Hash = pt.get<std::string>("ServerHash");
  versionInfo.Password = pt.get_optional<std::string>("Password");
  versionInfo.Changelog = pt.get<std::string>("Changelog");

  BOOST_FOREACH(boost::property_tree::ptree::value_type &data, pt.get_child("ServerData")) {
    std::string uri = data.second.get_value<std::string>("");
    if (uri.compare("") == 0)
      throw Exception("Invalid Uri for ServerData");
    versionInfo.Uris.push_back(uri);
  }

  return versionInfo;
}

std::vector<UpdateData> DownloadVersionList(Version latestVersion, Version currentVersion, bool encrypted)
{
  std::vector<UpdateData> versions;
  Version version = latestVersion;

  while (true) {
    std::cout << "Downloading version info for: " << version << std::endl;
    
    UpdateData info = GetVersionInfo(version, encrypted);
    versions.push_back(info);

    if (info.FromVersion == currentVersion)
      break;
    else
      version = info.FromVersion;
  }
  
  std::reverse(versions.begin(), versions.end());
  return versions;
}

bool ApplyUpdate(UpdateData patchData, Build::ImportantFilesType& importantFiles)
{
  bool encrypted = false;
  if (patchData.Password.is_initialized())
    encrypted = true;
  
  std::cout << "Loading data..." << std::endl;
  Patch patch;
  if (!Build::LoadData(patch, patchData.DownloadPath.c_str(), encrypted ? patchData.Password.get().c_str() : NULL)) {
    std::cerr << ConsoleUtils::Red << "Error reading patch data" << ConsoleUtils::Default << std::endl;
    return false;
  }

  int error = Build::ApplyPatch(patch, patchData.DownloadPath.c_str(), importantFiles, encrypted ? patchData.Password.get().c_str() : NULL);
  if (error > 0) {
    if (error == 1) {
      std::cerr << ConsoleUtils::Red << "Error opening update file." << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::ERROR_OPEN_UPDATE);
    } else if (error == 2) {
      std::cerr << ConsoleUtils::Red << "Declined license agreement. Exiting update." << ConsoleUtils::Default << std::endl;
      std::cout << "Removing lock" << std::endl;
      boost::filesystem::remove((Directories::GetCurrentModDirectory() / "update.lock").string());
      exit(PRServerUpdater::ERROR_DECLINE_LICENSE);
    } else if (error == 3) {
      std::cerr << ConsoleUtils::Red << "Error applying patch." << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::ERROR_APPLY_UPDATE);
    } else if (error == 4) {
      std::cerr << ConsoleUtils::Red << "Error finalizing archives." << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::ERROR_FINALIZE_ARCHIVES);
    } else if (error == 5) {
      std::cerr << ConsoleUtils::Red << "Error touching archives." << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::ERROR_TOUCH_ARCHIVES);
    } else {
      std::cerr << ConsoleUtils::Red << "Error applying patch: " << error << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::ERROR_APPLY_UPDATE);
    }

    return false;
  }

  std::cout << "Updating version..." << std::endl;
  Version::WriteVersion((Directories::GetCurrentModDirectory() / "mod.desc").string(), patchData.ToVersion);

  return true;
}

void RunUpdater(bool beta, std::vector<std::string> patches)
{
  std::cout << "Validating server license... ";
  License licenseInfo = License::Validate();
  if (licenseInfo.Valid) {
    std::cout << ConsoleUtils::DarkCyan << "Valid"  << ConsoleUtils::Default << "\n";
    std::cout << "  IP: " << licenseInfo.IP << "\n";
    std::cout << "  Port: " << licenseInfo.Port << "\n";
    std::cout << "  User: " << licenseInfo.User << "\n";
  } else {
    std::cout << ConsoleUtils::Red << "Invalid License\n";
    std::cout << licenseInfo.ErrorCode << " " << licenseInfo.ErrorMsg << ConsoleUtils::Default << std::endl;
    exit(PRServerUpdater::ERROR_INVALID_LICENSE);
  }

  std::cout << std::endl;
  // check for update.lock, skip to apply if it exists and shit like that
  
  std::vector<UpdateData> versionList;
  Version latestVersion;
  if (patches.empty()) {
    std::cout << "Reading current version... ";
    std::string currentVersionText = Version::CurrentVersion();
    std::cout << ConsoleUtils::DarkCyan << currentVersionText << ConsoleUtils::Default << std::endl;
    Version currentVersion = Version(currentVersionText);
    if (currentVersion.IsZero()) {
      std::cerr << ConsoleUtils::Red << "Current version is not a valid version number." << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::ERROR_INVALID_CURRENT_VERSION);
    }
  
    std::cout << "Checking latest version... ";
    latestVersion = DownloadLatestVersion(beta);

    if (!currentVersion.IsZero() && latestVersion > currentVersion) {
      std::cout << std::endl << ConsoleUtils::Green << "New version available!" << ConsoleUtils::Default << std::endl;
    } else {
      std::cout << std::endl << ConsoleUtils::White << "No new version available." << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::SUCCESS);
    }

    std::cout << std::endl;

    try {
      versionList = DownloadVersionList(latestVersion, currentVersion, beta);
    } catch (std::exception const&) {
      std::cerr << ConsoleUtils::Red << "Unable to get version information\nNo update route exists!" << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::ERROR_VERSION_NO_ROUTE);
    }
    
    std::cout << std::endl;

    boost::filesystem::path downloadPath = Directories::GetDownloadDirectory(true);

    std::cout << std::endl;

    boost::random::random_device rng;
    for (std::vector<UpdateData>::iterator it = versionList.begin(); it != versionList.end(); ++it) {
      std::cout << "Downloading: " << it->ToVersion << std::endl;
      boost::random::uniform_int_distribution<> dist(0, it->Uris.size() - 1);

      std::string uri = it->Uris[dist(rng)];
    
      boost::filesystem::path fn = boost::filesystem::path(uri).filename().string();
      it->DownloadPath = (downloadPath / fn).string();

      int error = Download::ToFile(uri.c_str(), downloadPath.string().c_str(), it->Hash.c_str());
      if (error > 0) {
        std::cerr << "Error " << error << std::endl;
        exit(PRServerUpdater::ERROR_DOWNLOAD_UPDATE);
      }

      std::cout << std::endl;
    }
  } else if (!patches.empty()) {
    std::cout << "Manually updating..." << std::endl;
    // manually applied patch using the --applyPatches "filename,filename" argument
    BOOST_FOREACH(const std::vector<std::string>::value_type& patch, patches) {
      bool encrypted = false;
      boost::filesystem::path ext = boost::filesystem::path(patch).extension().string();
      if (ext == ".prepatch")
        encrypted = true;

      Version toVersion;
      boost::regex regexVersion(".*(?<requiresVersion>\\d+\\.\\d+\\.\\d+\\.\\d+)_to_(?<version>\\d+\\.\\d+\\.\\d+\\.\\d+).*", boost::regex::icase);
      boost::smatch what;
      std::string filename = boost::filesystem::path(patch).filename().string();
      if (boost::regex_match(filename, what, regexVersion)) {
        toVersion = Version(what["version"].str());
      }
      
      std::cout << "Getting version info for " << toVersion << "..." << std::endl;
      UpdateData data = GetVersionInfo(toVersion, encrypted);
      data.DownloadPath = patch;

      if (data.Password.is_initialized())
        encrypted = true;
      else
        encrypted = false;

      Patch patchData;
      std::cout << "Loading update data..." << std::endl;
      if (Build::LoadData(patchData, patch.c_str(), encrypted ? data.Password.get().c_str() : NULL)) {
        latestVersion = patchData.Version;
        versionList.push_back(data);
      }
    }
  }
  
  Build::ImportantFilesType importantFiles;
  
  for (std::vector<UpdateData>::iterator it = versionList.begin(); it != versionList.end(); ++it) {
    std::cout << "Applying: " << it->ToVersion << std::endl;
    std::cout << "Writing lock" << std::endl;

    std::ofstream lock((Directories::GetCurrentModDirectory() / "update.lock").string().c_str(), std::ios::out | std::ios::trunc);
    lock << it->DownloadPath;
    lock.close();
    
    if (ApplyUpdate(*it, importantFiles)) {
      std::cout << "Removing lock" << std::endl;
      boost::filesystem::remove((Directories::GetCurrentModDirectory() / "update.lock").string());
      std::cout << "Deleting update file" << std::endl;
      boost::filesystem::remove(it->DownloadPath);
    } else
      exit(PRServerUpdater::ERROR_APPLY_UPDATE);

    std::cout << std::endl;
  }

  if (importantFiles.size() > 0) {
    std::cout << ConsoleUtils::DarkCyan << "The following file(s) were updated that may have contained server specific information:\n\n";
    BOOST_FOREACH(const Build::ImportantFilesType::value_type& pair, importantFiles) {
      std::cout << "  " << pair.first << "\n";
    }
    std::cout << "\nThe original file(s) have been saved in the same directory with a " << ConsoleUtils::Cyan << ".original" << ConsoleUtils::DarkCyan << " file extension.\n\n";
    std::cout << "Please make sure any additional modifications to these files are based off the latest version.\n" << ConsoleUtils::Default << std::endl;
  }

  std::cout << ConsoleUtils::Green << "Successfully updated to " << latestVersion << "!" << ConsoleUtils::Default << std::endl;
}

void SetupProgramOptions(int argc, char* argv[], bool& beta, std::string& keyFile, std::string& cdKeyHash, std::vector<std::string>& patches)
{
  namespace po = boost::program_options;

  std::string patchesTemp;

  po::options_description desc("Options");
  desc.add_options()
    ("update,u",                                             "Run the updater")
    ("beta,b",                                               "Run under beta update branch")
    ("keyFile,k",   po::value<std::string>(&keyFile),        "Path to key file (Required when --beta is set)")
    ("cdKeyHash,c", po::value<std::string>(&cdKeyHash),      "CD Key Hash (Required when --beta is set)")
    ("applyPatches,p", po::value<std::string>(&patchesTemp), "Manually update using patches (comma separated)");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (!vm.count("update")) {
#if defined(WIN32)
    std::cerr << ConsoleUtils::Red << "Please run PRServerUpdater-win32.exe" << ConsoleUtils::Default << std::endl;
#elif defined(LINUX)
    std::cerr << ConsoleUtils::Red << "Please run ./PRServerUpdater-linux32" << ConsoleUtils::Default << std::endl;
#elif defined(LINUX64)
    std::cerr << ConsoleUtils::Red << "Please run ./PRServerUpdater-linux64" << ConsoleUtils::Default << std::endl;
#endif
    exit(PRServerUpdater::ERROR_ARGUMENTS);
  }

  if (vm.count("beta")) {
    if (!vm.count("keyFile") || !vm.count("cdKeyHash")) {
      std::cerr << ConsoleUtils::Red << "Error!\nMissing --keyFile and --cdKeyHash parameters." << ConsoleUtils::Default << std::endl;
      exit(PRServerUpdater::ERROR_ARGUMENTS);
    }
    beta = true;
  } else {
    beta = false;
  }

  if (vm.count("applyPatches"))
    boost::split(patches, patchesTemp, boost::is_any_of(","));
}

#if defined(WIN32)
PHANDLER_ROUTINE HandlerRoutine(DWORD dwCtrlType)
{
  exit(PRServerUpdater::ERROR_INTERRUPT);
}
#endif

int main(int argc, char* argv[])
{
  try {
    bool beta;
    std::string keyFile;
    std::string cdKeyHash;
    std::vector<std::string> patches;
    
    ConsoleUtils::InitializeColors();

#if defined(WIN32)
    SetConsoleCtrlHandler((PHANDLER_ROUTINE)&HandlerRoutine, true);
#endif

    PrintHeader();

    SetupProgramOptions(argc, argv, beta, keyFile, cdKeyHash, patches);

    if (beta) {
      if (!EncryptedData::LoadKeyFile(keyFile, cdKeyHash))
        std::cerr << ConsoleUtils::Red << "Unable to read key file!" << ConsoleUtils::Default << std::endl;
    }

    RunUpdater(beta, patches);
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "\nUnhandled exception!\n" << e.what() << ConsoleUtils::Default << std::endl;
    exit(PRServerUpdater::ERROR_UNKNOWN);
  }
  
  return PRServerUpdater::SUCCESS;
}
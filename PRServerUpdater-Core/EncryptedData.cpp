#include "EncryptedData.h"

#include "ZipArchive/ZipArchive.h"
#include <cryptopp/aes.h>
#include <cryptopp/ccm.h>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <fstream>

byte EncryptedData::_key[32] = {0};
byte EncryptedData::_iv[16] = {0};

std::string EncryptedData::Read(const byte* encryptedData, size_t size)
{
  std::string data;
  CryptoPP::CBC_Mode<CryptoPP::AES>::Decryption decryptor(_key, sizeof(_key), _iv);
  CryptoPP::ArraySource(encryptedData, size, true,
    new CryptoPP::StreamTransformationFilter(decryptor,
      new CryptoPP::StringSink(data),
      CryptoPP::BlockPaddingSchemeDef::PKCS_PADDING
    )
  );

  // i dunno why I have to do this, there's 3 junk characters at the start...
  // dunno if this will cause problems...
  data.erase(0, 3);
  
  return data;
}

bool EncryptedData::LoadKeyFile(std::string path, std::string cdKeyHash)
{
  CZipStringArray names;
  names.Add("key");
  names.Add("iv");

  bool keySet = false;
  bool ivSet = false;

  CZipArchive zip;
  if (zip.Open(path.c_str(), CZipArchive::zipOpenReadOnly)) {
    zip.SetPassword(cdKeyHash.c_str());

    CZipIndexesArray indexes;
    zip.GetIndexes(names, indexes);
    
    for (ZIP_ARRAY_SIZE_TYPE i = 0; i < indexes.GetCount(); i++) {
      ZIP_INDEX_TYPE index = indexes[i];
      CZipString name = names[i];
      
      if (index == ZIP_FILE_INDEX_NOT_FOUND) {
        break;
      } else {
        CZipMemFile mfOut;
        mfOut.SetLength(zip[index]->m_uUncomprSize);
        ((CZipAbstractFile*)&mfOut)->SeekToBegin();
        
        if (!zip.ExtractFile(index, mfOut))
          break;
        
        byte* b = mfOut.Detach();

        if (name.Compare("key") == 0) {
          for (int i = 0; i < 32; i++)
            _key[i] = b[i];
          keySet = true;
        } else if (name.Compare("iv") == 0) {
          for (int i = 0; i < 16; i++)
            _iv[i] = b[i];
          ivSet = true;
        }

        free(b);
      }
    }
    
    zip.Close();
  }

  if (!keySet || !ivSet)
    return false;

  return true;
}
#pragma once

#include <string>

#if defined(WIN32)
#include <windows.h>
#endif

typedef unsigned char byte;

class Utils
{
  public:
    static bool ValidateFile(std::string path, std::string sha1);
    static bool ValidateBytes(byte* bytes, size_t length, std::string sha1);
    static std::string FixPath(const std::string path);
    static std::string DescribeIosFailure(const std::ios& stream);
    
#if defined(WIN32)
    static std::string FormatSystemMessage(DWORD dwMsgId);
#endif

};


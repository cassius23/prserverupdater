#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>
#include <cstdio>
#include <iostream>

#include "ConsoleUtils.h"

#if defined(WIN32)
HANDLE ConsoleUtils::HConsolePosition = NULL;
HANDLE ConsoleUtils::HConsoleColor = NULL;
CONSOLE_SCREEN_BUFFER_INFO ConsoleUtils::ConsoleInfoPosition;
#elif defined(LINUX) || defined(LINUX64)
bool ConsoleUtils::Initialized = false;
#endif

unsigned int ConsoleUtils::DetailLength = 0;
ConsoleUtils::ConsoleColor ConsoleUtils::OriginalForeground = ConsoleUtils::Default;
ConsoleUtils::ConsoleColor ConsoleUtils::OriginalBackground = ConsoleUtils::Default;

void ConsoleUtils::FileProgress(double done, double total, double elapsed)
{
#if defined(WIN32)
  if (HConsolePosition == NULL) {
    HConsolePosition = GetStdHandle(STD_OUTPUT_HANDLE);
  } else {
    GetConsoleScreenBufferInfo(HConsolePosition, &ConsoleInfoPosition);
    ConsoleInfoPosition.dwCursorPosition.Y -= 2;
    SetConsoleCursorPosition(HConsolePosition, ConsoleInfoPosition.dwCursorPosition);
  }
#elif defined(LINUX) || defined(LINUX64)
  if (Initialized)
    std::cout << "\x1b[A" << "\x1b[A";
  else
    Initialized = true;
#endif

  ProgressBar(done / total * 100);
  std::cout << "\n";
  ProgressDetail(done, total, elapsed);
  std::cout << std::endl;
}

void ConsoleUtils::FreeProgress()
{
#if defined(WIN32)
  HConsolePosition = NULL;
#elif defined(LINUX) || defined(LINUX64)
  Initialized = false;
#endif
}

void ConsoleUtils::InitializeColors()
{
  OriginalBackground = Black;

#if defined(WIN32)
  OriginalForeground = DarkWhite;
  HConsoleColor = GetStdHandle(STD_OUTPUT_HANDLE);
  unsigned short attributes = ((unsigned short)(OriginalBackground << 4)) | (unsigned short)OriginalForeground;
  SetConsoleTextAttribute(HConsoleColor, attributes);
#elif defined(LINUX) || defined(LINUX64)
  OriginalForeground = White;
  printf("%c[%d;%d;%dm", 0x1b, 0, OriginalForeground, OriginalBackground + 10);
#endif
}

void ConsoleUtils::SetTextColor(ConsoleColor color)
{
  if (OriginalForeground == Default || OriginalBackground == Default)
    return;

  if (color == Default) {
    ResetTextColor();
    return;
  }

#if defined(WIN32)
  unsigned short attributes = ((unsigned short)(OriginalBackground << 4)) | (unsigned short)color;
  SetConsoleTextAttribute(HConsoleColor, attributes);
#elif defined(LINUX) || defined(LINUX64)
  bool isDarkColor = false;
  if (color == DarkBlue || color == DarkCyan || color == DarkGreen || color == DarkMagenta || color == DarkRed || color == DarkWhite || color == DarkYellow) {
    isDarkColor = true;
    color = (ConsoleColor)((int)color + 10);
  }
  
  printf("%c[%d;%d;%dm", 0x1b, isDarkColor ? 2 : 1, color, OriginalBackground + 10);
#endif
}

void ConsoleUtils::ResetTextColor()
{
#if defined(WIN32)
  unsigned short attributes = ((unsigned short)(OriginalBackground << 4)) | (unsigned short)OriginalForeground;
  SetConsoleTextAttribute(HConsoleColor, attributes);
#elif defined(LINUX) || defined(LINUX64)
  printf("%c[%d;%d;%dm", 0x1b, 0, OriginalForeground, OriginalBackground + 10);
#endif
}

void ConsoleUtils::ProgressBar(double percent)
{
  std::string bar;

  for (unsigned int i = 0; i < 50; i++) {
    if (i < ((unsigned int)percent / 2))
      bar.replace(i, 1, "=");
    else if (i == ((unsigned int)percent / 2))
      bar.replace(i, 1, ">");
    else
      bar.replace(i, 1, " ");
  }
  
  std::cout << boost::format("\r[%1%] %2$6.2f%%") % bar % percent;
}

void ConsoleUtils::ProgressDetail(double done, double total, double elapsed)
{
  boost::posix_time::time_duration duration = boost::posix_time::seconds((long)elapsed);
  std::string detail = boost::str(boost::format("Elapsed: %1% Downloaded: %2% Total: %3%") % duration % FormatFileSize(done) % FormatFileSize(total));

  std::cout << "\r" << std::string(DetailLength, ' ');

  if (detail.length() > DetailLength)
    DetailLength = detail.length();

  std::cout << "\r" << detail;
}

std::string ConsoleUtils::FormatFileSize(double size)
{
  int i = 0;
  const std::string units[] = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

  while (size > 1024) {
    size /= 1024;
    i++;
  }

  return boost::str(boost::format("%1$.2f %2%") % size % units[i]);
}

std::ostream& operator<<(std::ostream& os, const ConsoleUtils::ConsoleColor& color)
{
  os.flush();
  if (color == ConsoleUtils::Default)
    ConsoleUtils::ResetTextColor();
  else
    ConsoleUtils::SetTextColor(color);
  return os;
}
#include <fstream>
#include <sstream>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>
#include "Directories.h"
#include "Download.h"
#include "Exception.h"
#include "JsonTypes.h"
#include "License.h"

#if defined(LINUX) || defined(LINUX64)
#include <unistd.h>
#define Sleep(x) usleep(x * 1000)
#endif

License License::Validate(void)
{
  License license = License();

  try {
    license.ReadServerSettings((Directories::GetCurrentModDirectory() / "settings" / "serversettings.con").string());
  } catch (std::exception const& e) {
    license.Valid = false;
    license.ErrorCode = "0001";
    license.ErrorMsg = "Error reading serversettings.con\n" + boost::lexical_cast<std::string>(e.what());
    return license;
  }

  try {
    license.ReadExternalIP((Directories::GetCurrentModDirectory() / "externalip.txt").string());
  }
  catch (std::exception const& e) {
    license.Valid = false;
    license.ErrorCode = "0005";
    license.ErrorMsg = "Error reading externalip.txt\n" + boost::lexical_cast<std::string>(e.what());
    return license;
  }

  try {
    license.ReadLicenseKey((Directories::GetCurrentModDirectory() / "license.key").string());
  } catch (std::exception const& e) {
    license.Valid = false;
    license.ErrorCode = "0002";
    license.ErrorMsg = "Error reading license.key\n" + boost::lexical_cast<std::string>(e.what());
    return license;
  }

  try {
    if (license._externalIP.empty())
      license.GetLicenseDetails(license._serverSettingsIP, license._serverSettingsPort, license._licenseKey, license._serverSettingsInterface);
    else
      license.GetLicenseDetails(license._externalIP, license._serverSettingsPort, license._licenseKey, license._serverSettingsInterface);
  } catch (std::exception const& e) {
    license.Valid = false;
    license.ErrorCode = "0003";
    license.ErrorMsg = "Error reading from server\n" + boost::lexical_cast<std::string>(e.what());
    return license;
  }

  return license;
}

void License::ReadServerSettings(std::string path)
{
  boost::regex regexIP("^sv\\.serverIP \"(?<ip>\\d+?\\.\\d+?\\.\\d+?\\.\\d+?)\"$", boost::regex::icase);
  boost::regex regexInterface("^sv\\.interfaceIP \"(?<ip>\\d+?\\.\\d+?\\.\\d+?\\.\\d+?)\"$", boost::regex::icase);
  boost::regex regexPort("^sv\\.serverPort (?<port>\\d+?)$", boost::regex::icase);

  const char* cpath = path.c_str();
  std::ifstream in(cpath, std::ios::in);

  bool ipSet = false;
  bool interfaceSet = false;
  bool portSet = false;

  std::string line = "";
  while (std::getline(in, line)) {
    boost::cmatch what;
    if (boost::regex_search(line.c_str(), what, regexIP)) {
      _serverSettingsIP = what["ip"].str();
      ipSet = true;
    } else if (boost::regex_search(line.c_str(), what, regexInterface)) {
      _serverSettingsInterface = what["ip"].str();
      interfaceSet = true;
    } else if (boost::regex_search(line.c_str(), what, regexPort)) {
      try {
        _serverSettingsPort = boost::lexical_cast<unsigned short>(what["port"].str());
      } catch (std::exception const&) {
        throw Exception("Unable to parse sv.serverPort. Make sure port is between 0 and 65535.");
      }
      portSet = true;
    }
  }

  in.close();

  if (!ipSet)
    throw Exception("sv.serverIP is not set to a valid IP Address (xxx.xxx.xxx.xxx)");
  else if (!portSet)
    throw Exception("sv.serverPort is not set to a valid port number (0-65535)");
  else if (!interfaceSet)
    _serverSettingsInterface = "";
}

void License::ReadExternalIP(std::string path)
{
  boost::regex regexKey("^\\d+?\\.\\d+?\\.\\d+?\\.\\d+?$");

  const char* cpath = path.c_str();
  std::ifstream in(cpath, std::ios::in);

  std::string line = "";
  if (std::getline(in, line)) {
    boost::cmatch what;
    if (boost::regex_search(line.c_str(), what, regexKey)) {
      _externalIP = line;
    }
  }

  in.close();
}

void License::ReadLicenseKey(std::string path)
{
  boost::regex regexKey("^[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}$");
  
  const char* lpath = path.c_str();
  std::ifstream in(lpath, std::ios::in);
  
  bool keySet = false;

  std::string line = "";
  if (std::getline(in, line)) {
    boost::cmatch what;
    if (boost::regex_search(line.c_str(), what, regexKey)) {
      _licenseKey = line;
      keySet = true;
    }
  }

  in.close();

  if (!keySet)
    throw Exception("Key not defined or is in a bad format (XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX all uppercase)");
}

void License::GetLicenseDetails(std::string ip, unsigned short port, std::string key, std::string interfaceIP)
{
  std::vector<byte> jsonBytes;
  
  std::string url = (boost::format("http://www.realitymod.com/forum/lcp_validate.php?action=server&game=prbf2&key=%1%&ip=%2%&port=%3%") % key % ip % port).str();

  Sleep(1000);
  int error = Download::ToMemory(url.c_str(), jsonBytes, interfaceIP.compare("") == 0 ? NULL : interfaceIP.c_str());
  if (error > 0) {
    Valid = false;
    ErrorCode = "0004";
    ErrorMsg = "Error reading from server: " + boost::lexical_cast<std::string>(error);
    return;
  }

  std::string json = std::string(jsonBytes.begin(), jsonBytes.end());

  std::istringstream iss(json);
  boost::property_tree::ptree pt;
  boost::property_tree::read_json(iss, pt);

  if (pt.get<bool>("Valid")) {
    Valid = true;
    IP = ip;
    Port = port;
    User = pt.get<std::string>("User");
  } else {
    Valid = false;
    ErrorCode = pt.get<std::string>("ErrorCode");
    ErrorMsg = pt.get<std::string>("ErrorMsg");
  }
}
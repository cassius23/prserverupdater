#include "Build.h"

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include <boost/thread/thread.hpp>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>
#include <cryptopp/sha.h>
#include <fstream>
#include <string>
#include "BinaryPatch.h"
#include "ConsoleUtils.h"
#include "Directories.h"
#include "Utils.h"

namespace fs = boost::filesystem;

#define sleep(a) (boost::this_thread::sleep(boost::posix_time::millisec(a)))
#if !defined(WIN32)
#define FILE_ATTRIBUTE_NORMAL 0x00000080 
#endif

std::string Build::_patchTempExtension = ".patchtemp";
std::string Build::_patchOldExtension = ".patchold";
std::string Build::_patchOriginalExtension = ".original";
ULONGLONG Build::_maximumBufferSize = 67108864; // 64 MB

bool Build::LoadData(Patch& patch, const char* path, const char* password)
{
  CZipStringArray names;
  names.Add("info.json");

  bool infoBytesSet = false;
  byte* infoBytes = NULL;
  int infoBytesLength = 0;

  CZipArchive zip;
  if (zip.Open(path, CZipArchive::zipOpenReadOnly)) {
    if (password != NULL)
      zip.SetPassword(password);

    CZipIndexesArray indexes;
    zip.GetIndexes(names, indexes);

    for (ZIP_ARRAY_SIZE_TYPE i = 0; i < indexes.GetCount(); i++) {
      ZIP_INDEX_TYPE index = indexes[i];
      CZipString name = names[i];

      if (index == ZIP_FILE_INDEX_NOT_FOUND) {
        break;
      } else {
        CZipMemFile mfOut;
        mfOut.SetLength(zip[index]->m_uUncomprSize);
        ((CZipAbstractFile*)&mfOut)->SeekToBegin();

        if (!zip.ExtractFile(index, mfOut))
          break;
        
        infoBytesLength = (int)mfOut.GetLength();
        infoBytes = mfOut.Detach();
        infoBytesSet = true;
      }
    }
  }
  zip.Close();

  if (!infoBytesSet)
    return false;

  std::string json = std::string((char*)infoBytes, infoBytesLength);

  free(infoBytes);

  std::istringstream iss(json);
  boost::property_tree::ptree pt;
  boost::property_tree::read_json(iss, pt);
  
  patch.Version = Version(pt.get<std::string>("Version"));
  patch.RequiresVersion = Version(pt.get<std::string>("RequiresVersion"));
  patch.License = pt.get_optional<std::string>("License");

  if (pt.get_child_optional("PrePatch").is_initialized()) {
    BOOST_FOREACH(boost::property_tree::ptree::value_type &data, pt.get_child("PrePatch")) {
      std::string utility = data.second.get_value<std::string>("");
      patch.PrePatch.push_back(PatchUtilityMap[utility]);
    }
  }

  if (pt.get_child_optional("PostPatch").is_initialized()) {
    BOOST_FOREACH(boost::property_tree::ptree::value_type &data, pt.get_child("PostPatch")) {
      std::string utility = data.second.get_value<std::string>("");
      patch.PostPatch.push_back(PatchUtilityMap[utility]);
    }
  }

  if (pt.get_child_optional("TouchData").is_initialized()) {
    PatchTouchData touchData;
    std::string timestamp = pt.get<std::string>("TouchData.Timestamp");
    touchData.Timestamp = Build::from_iso_extended_string(timestamp);
    BOOST_FOREACH(boost::property_tree::ptree::value_type &data, pt.get_child("TouchData.Archives")) {
      std::string archive = data.second.get_value<std::string>("");
      touchData.Archives.push_back(archive);
    }
    patch.TouchData = touchData;
  }

  BOOST_FOREACH(boost::property_tree::ptree::value_type &data, pt.get_child("PatchData")) {
    PatchItem patchItem;
    patchItem.Method = PatchItemMap[data.second.get<std::string>("Method")];
    patchItem.Source = data.second.get_optional<std::string>("Source");
    patchItem.Destination = data.second.get<std::string>("Destination");
    patchItem.Entry = data.second.get_optional<std::string>("Entry");
    patchItem.BeforeHash = data.second.get_optional<std::string>("BeforeHash");
    patchItem.AfterHash = data.second.get_optional<std::string>("AfterHash");
    patchItem.IsReplacementExecutable = data.second.get_optional<bool>("IsReplacementExecutable");
    patchItem.IsImportantFile = data.second.get_optional<bool>("IsImportantFile");
    patchItem.IsDirectory = data.second.get_optional<bool>("IsDirectory");
    patch.PatchData.push_back(patchItem);
  }
  
  return true;
}

int Build::ApplyPatch(Patch patch, const char* path, ImportantFilesType& importantFiles, const char* password)
{
  CZipArchive zip;

  if (zip.Open(path, CZipArchive::zipOpenReadOnly)) {
    if (password != NULL)
      zip.SetPassword(password);

    if (!ReadPatchLicense(patch, zip)) {
      zip.Close();
      return 2;
    }
    
    std::cout << "Applying patch..." << std::endl;

    ArchiveMapType modifiedZipArchives;

    if (!ApplyPatchFiles(patch.PatchData, zip, patch.TouchData, modifiedZipArchives, importantFiles)) {
      zip.Close();
      return 3;
    }

    std::vector<std::string> archivesToTouch = patch.TouchData.Archives;

    try {
      BOOST_FOREACH(const ArchiveMapType::value_type& pair, modifiedZipArchives) {
        std::cout << ConsoleUtils::White << "Finalizing archive: " << ConsoleUtils::Default << pair.first << std::endl;
        if (!pair.second.Closed) {
          bool res = TouchArchive(*pair.second.Zip, patch.TouchData.Timestamp);
          pair.second.Zip->Close();
          delete pair.second.Zip;
          if (!res) {
            std::cerr << ConsoleUtils::Red << "Error finalizing archives: Touch error" << ConsoleUtils::Default << std::endl;
            zip.Close();
            return 4;
          }
        }
        SafeMove(pair.second.TempFileName, pair.second.FileName, pair.second.FileName + _patchOldExtension);
        fs::remove(pair.second.FileName + _patchOldExtension);

        std::vector<std::string>::iterator it = std::find(archivesToTouch.begin(), archivesToTouch.end(), pair.first);
        if (it != archivesToTouch.end())
          archivesToTouch.erase(it);
      }
    } catch(CZipException& e) {
      std::cerr << ConsoleUtils::Red << "Error finalizing archives: " << e.m_iCause << " - " << e.GetErrorDescription() << ConsoleUtils::Default << std::endl;
      zip.Close();
      return 4;
    } catch (boost::filesystem::filesystem_error& e) {
      std::cerr << ConsoleUtils::Red << "Error finalizing archives: " << e.what() << ConsoleUtils::Default << std::endl;
      zip.Close();
      return 4;
    } catch (std::exception const& e) {
      std::cerr << ConsoleUtils::Red << "Error finalizing archives: " << e.what() << ConsoleUtils::Default << std::endl;
      zip.Close();
      return 4;
    }

    try {
      BOOST_FOREACH(const std::string& archive, archivesToTouch) {
        fs::path archivePath = Directories::GetCurrentModDirectory() / archive;
        if (!fs::exists(archivePath))
          continue;

        std::cout << ConsoleUtils::White << "Touching archive: " << ConsoleUtils::Default << archive << std::endl;
        if (!TouchArchive(archivePath.string(), patch.TouchData.Timestamp))
          return false;
      }
    } catch(CZipException& e) {
      std::cerr << ConsoleUtils::Red << "Error touching archives: " << e.m_iCause << " - " << e.GetErrorDescription() << ConsoleUtils::Default << std::endl;
      zip.Close();
      return 5;
    } catch (boost::filesystem::filesystem_error& e) {
      std::cerr << ConsoleUtils::Red << "Error touching archives: " << e.what() << ConsoleUtils::Default << std::endl;
      zip.Close();
      return 5;
    } catch (std::exception const& e) {
      std::cerr << ConsoleUtils::Red << "Error touching archives: " << e.what() << ConsoleUtils::Default << std::endl;
      zip.Close();
      return 5;
    }
  } else {
    zip.Close();
    return 1;
  }

  zip.Close();
  
  return 0;
}

bool Build::ReadPatchLicense(Patch patch, CZipArchive& zip)
{
  std::vector<PatchUtility>::iterator it = std::find(patch.PrePatch.begin(), patch.PrePatch.end(), AcceptNewLicense);
  if (it != patch.PrePatch.end()) {
    std::cout << "\n" << ConsoleUtils::Yellow << "There is a new server license for this version.\n";
    std::cout << "The license may be viewed in full here:\n";
    std::cout << "http://www.realitymod.com/forum/licensecp.php?do=license_agreement" << ConsoleUtils::Default << "\n\n";

    std::string line;
    while (true) {
      std::cout << "Do you accept this license agreement [y/n]: ";
      std::flush(std::cout);
      std::getline(std::cin, line);

      if (boost::iequals(line, "n"))
        return false;
      else if (boost::iequals(line, "y")) {
        std::cout << std::endl;
        return true;
      }
    }
  }

  return true;
}

bool Build::ApplyPatchFiles(std::vector<PatchItem> patchData, CZipArchive& zip, PatchTouchData touchData, ArchiveMapType& modifiedZipArchives, std::map<std::string, std::string>& importantFiles)
{
  BOOST_FOREACH(PatchItem &item, patchData) {
    std::string destination = Utils::FixPath(item.Destination);
    std::string shortDestination = destination;
    std::string shortDestinationArchive = destination;

    if (boost::starts_with(destination, "@ModDir")) {
      shortDestination = destination.substr(8); // "@ModDir\\".Length
      boost::replace_all(destination, "@ModDir", fs::absolute(Directories::GetCurrentModDirectory()).string());
    } else if (boost::starts_with(destination, "@RootDir")) {
      shortDestination = destination.substr(9); // "@RootDir\\".Length
      boost::replace_all(destination, "@RootDir", fs::absolute(Directories::GetBF2RootDirectory()).string());
    }
    
    if (item.IsImportantFile.is_initialized()) {
      if (fs::exists(destination)) {
        ImportantFilesType::iterator it = importantFiles.find(shortDestination);
        if (it == importantFiles.end()) {
          fs::copy_file(destination, destination + _patchOriginalExtension, fs::copy_option::overwrite_if_exists);
          importantFiles.insert(std::pair<std::string, std::string>(shortDestination, shortDestination + _patchOriginalExtension));
        }
      }
    }

    switch (item.Method) {
      case FileAdd: {
        ZipEntry source = PatchReadEntry(zip, Utils::FixPath(item.Source.get()));
        if (!ApplyPatchFilesAdd(source, destination, shortDestination))
          return false;
        break;
      }
      case FileModify: {
        ZipEntry source = PatchReadEntry(zip, Utils::FixPath(item.Source.get()));
        if (!ApplyPatchFilesModify(source, destination, shortDestination, item.BeforeHash.get(), item.AfterHash.get()))
          return false;
        break;
      }
      case FileRemove: {
        if (!ApplyPatchFilesRemove(destination, shortDestination, item.IsDirectory.get()))
          return false;
        break;
      }
      case ZipAdd: {
        shortDestinationArchive = shortDestination;
        shortDestination = (fs::path(shortDestination) / fs::path(Utils::FixPath(item.Entry.get()))).string();
        ZipEntry source = PatchReadEntry(zip, Utils::FixPath(item.Source.get()));
        if (!ApplyPatchZipAdd(source, destination, Utils::FixPath(item.Entry.get()), shortDestination, shortDestinationArchive, touchData.Timestamp, modifiedZipArchives))
          return false;
        break;
      }
      case ZipModify: {
        shortDestinationArchive = shortDestination;
        shortDestination = (fs::path(shortDestination) / fs::path(Utils::FixPath(item.Entry.get()))).string();
        ZipEntry source = PatchReadEntry(zip, Utils::FixPath(item.Source.get()));
        if (!ApplyPatchZipModify(source, destination, Utils::FixPath(item.Entry.get()), shortDestination, shortDestinationArchive, item.BeforeHash.get(), item.AfterHash.get(), touchData.Timestamp, modifiedZipArchives))
          return false;
        break;
      }
      case ZipRemove: {
        shortDestinationArchive = shortDestination;
        shortDestination = (fs::path(shortDestination) / fs::path(Utils::FixPath(item.Entry.get()))).string();
        if (!ApplyPatchZipRemove(destination, Utils::FixPath(item.Entry.get()), shortDestination, shortDestinationArchive, modifiedZipArchives))
          return false;
        break;
      }
    }
  }

  return true;
}

bool Build::ApplyPatchFilesAdd(ZipEntry& source, std::string destination, std::string shortDestination)
{
  if (source.Size == ((ULONGLONG)-1)) {
    std::cerr << ConsoleUtils::Red << "Error reading source data: " << shortDestination << ConsoleUtils::Default << std::endl;
    return false;
  }
  
  std::cout << ConsoleUtils::DarkGreen << "FileAdd: " << ConsoleUtils::Default << shortDestination << std::endl;

  bool hasWritten = false;

  try {
    fs::path destinationPath(destination);

    if (!fs::is_directory(destinationPath.parent_path()))
      fs::create_directories(destinationPath.parent_path());

    if (fs::exists(destination + _patchTempExtension))
      fs::remove(destination + _patchTempExtension);

    if (fs::exists(destination + _patchOldExtension))
      fs::remove(destination + _patchOldExtension);
    
    hasWritten = true;
    std::ofstream ofs((destination + _patchTempExtension).c_str(), std::ios::binary | std::ios::trunc);
    if (ofs.good()) {
      ofs.write((char*)source.Data, source.Size);
      ofs.close();
    } else {
      std::cerr << ConsoleUtils::Red << Utils::DescribeIosFailure(ofs) << ConsoleUtils::Default << std::endl;
      std::cerr << ConsoleUtils::Red << "Unable to write output file" << ConsoleUtils::Default << std::endl;
      return false;
    }
    SafeMove(destination + _patchTempExtension, destination, destination + _patchOldExtension);

    ClearAttributes((destination + _patchOldExtension).c_str());
    fs::remove(destination + _patchOldExtension);
  } catch (boost::filesystem::filesystem_error& e) {
    std::cerr << ConsoleUtils::Red << "Error adding file: " << e.what() << ConsoleUtils::Default << std::endl;
    AttemptRestore(destination, hasWritten);
    return false;
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Error adding file: " << e.what() << ConsoleUtils::Default << std::endl;
    AttemptRestore(destination, hasWritten);
    return false;
  }

  return true;
}

bool Build::ApplyPatchFilesModify(ZipEntry& source, std::string destination, std::string shortDestination, std::string beforeHash, std::string afterHash)
{
  if (source.Size == ((ULONGLONG)-1)) {
    std::cerr << ConsoleUtils::Red << "Error reading source data: " << shortDestination << ConsoleUtils::Default << std::endl;
    return false;
  }

  if (!fs::exists(destination))
    return true;
  
  std::cout << ConsoleUtils::DarkYellow << "FileModify: " << ConsoleUtils::Default << shortDestination << std::endl;

  bool hasWritten = false;

  try {
    bool isUpdateable = true;

    if (!Utils::ValidateFile(destination, beforeHash))
      isUpdateable = false;

    if (isUpdateable) {
      fs::path patch = (fs::temp_directory_path() / fs::unique_path("PRDiffTempPatch-%%%%-%%%%-%%%%-%%%%"));
      std::ofstream ofs(patch.string().c_str(), std::ios::binary | std::ios::trunc);
      if (ofs.good()) {
        ofs.write((char*)source.Data, source.Size);
        ofs.close();
      } else {
        std::cerr << ConsoleUtils::Red << Utils::DescribeIosFailure(ofs) << ConsoleUtils::Default << std::endl;
        std::cerr << ConsoleUtils::Red << "Unable to write patch file" << ConsoleUtils::Default << std::endl;
        return false;
      }

      BinaryPatch::Apply(destination.c_str(), patch.string().c_str(), (destination + _patchTempExtension).c_str());
      fs::remove(patch);

      if (fs::exists(destination + _patchTempExtension)) {
        hasWritten = true;

        if (!Utils::ValidateFile(destination + _patchTempExtension, afterHash)) {
          std::cerr << ConsoleUtils::Red << "The result does not match the target file" << ConsoleUtils::Default << std::endl;
          return false;
        }

        SafeMove(destination + _patchTempExtension, destination, destination + _patchOldExtension);
        
        ClearAttributes((destination + _patchOldExtension).c_str());
        fs::remove(destination + _patchOldExtension);
      } else {
        std::cerr << ConsoleUtils::Red << "Error patching file" << ConsoleUtils::Default << std::endl;
        return false;
      }
    } else if (!Utils::ValidateFile(destination, afterHash)) {
      std::cerr << ConsoleUtils::Red << "The existing file does not match the target file" << ConsoleUtils::Default << std::endl;
      return false;
    }
  } catch (boost::filesystem::filesystem_error& e) {
    std::cerr << ConsoleUtils::Red << "Error modifying file: " << e.what() << ConsoleUtils::Default << std::endl;
    AttemptRestore(destination, hasWritten);
    return false;
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Error modifying file: " << e.what() << ConsoleUtils::Default << std::endl;
    AttemptRestore(destination, hasWritten);
    return false;
  }

  return true;
}

bool Build::ApplyPatchFilesRemove(std::string destination, std::string shortDestination, bool isDirectory)
{
  try {
    fs::path destinationPath(destination);

    if (isDirectory) {
      if (fs::is_directory(destinationPath)) {
        std::cout << ConsoleUtils::DarkRed << "FileRemove: " << ConsoleUtils::Default << shortDestination << std::endl;
        fs::remove_all(destinationPath);
      }
    } else if (fs::exists(destinationPath)) {
      std::cout << ConsoleUtils::DarkRed << "FileRemove: " << ConsoleUtils::Default << shortDestination << std::endl;
      fs::remove(destinationPath);
    }
  } catch (boost::filesystem::filesystem_error& e) {
    std::cerr << ConsoleUtils::Red << "Error removing " << (isDirectory ? "directory: " : "file: ") << e.what() << ConsoleUtils::Default << std::endl;
    return false;
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Error removing " << (isDirectory ? "directory: " : "file: ") << e.what() << ConsoleUtils::Default << std::endl;
    return false;
  }

  return true;
}

bool Build::ApplyPatchZipAdd(ZipEntry& source, std::string destination, std::string entryName, std::string shortDestination, std::string shortDestinationArchive, time_t timestamp, ArchiveMapType& modifiedZipArchives)
{
  if (source.Size == ((ULONGLONG)-1)) {
    std::cerr << ConsoleUtils::Red << "Error reading source data: " << shortDestination << ConsoleUtils::Default << std::endl;
    return false;
  }
  
  std::cout << ConsoleUtils::DarkGreen << "ZipAdd: " << ConsoleUtils::Default << shortDestination << std::endl;

  try {
    fs::path destinationPath(destination);

    if (!fs::is_directory(destinationPath.parent_path()))
      fs::create_directories(destinationPath.parent_path());

    if (fs::exists(destination + _patchTempExtension) && modifiedZipArchives.find(shortDestinationArchive) == modifiedZipArchives.end())
      fs::remove(destination + _patchTempExtension);

    if (!fs::exists(destination + _patchTempExtension) || modifiedZipArchives.find(shortDestinationArchive) == modifiedZipArchives.end())
      if (fs::exists(destination))
        fs::copy_file(destination, destination + _patchTempExtension, fs::copy_option::overwrite_if_exists);

    if (fs::exists(destination + _patchOldExtension))
      fs::remove(destination + _patchOldExtension);

    if (!PatchAddEntry(destination + _patchTempExtension, destination, shortDestinationArchive, entryName, source.Data, (size_t)source.Size, timestamp, modifiedZipArchives))
      return false;
  } catch (boost::filesystem::filesystem_error& e) {
    std::cerr << ConsoleUtils::Red << "Error adding to zip archive: " << e.what() << ConsoleUtils::Default << std::endl;
    return false;
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Error adding to zip archive: " << e.what() << ConsoleUtils::Default << std::endl;
    return false;
  }

  return true;
}

bool Build::ApplyPatchZipModify(ZipEntry& source, std::string destination, std::string entryName, std::string shortDestination, std::string shortDestinationArchive, std::string beforeHash, std::string afterHash, time_t timestamp, ArchiveMapType& modifiedZipArchives)
{
  if (source.Size == ((ULONGLONG)-1)) {
    std::cerr << ConsoleUtils::Red << "Error reading source data: " << shortDestination << ConsoleUtils::Default << std::endl;
    return false;
  }
  
  std::cout << ConsoleUtils::DarkYellow << "ZipModify: " << ConsoleUtils::Default << shortDestination << std::endl;
        
  byte* outputData = {0};
  size_t outputSize = -1;

  try {
    fs::path destinationPath(destination);

    if (!fs::is_directory(destinationPath.parent_path()))
      fs::create_directories(destinationPath.parent_path());

    if (fs::exists(destination + _patchTempExtension) && modifiedZipArchives.find(shortDestinationArchive) == modifiedZipArchives.end())
      fs::remove(destination + _patchTempExtension);

    if (!fs::exists(destination + _patchTempExtension) || modifiedZipArchives.find(shortDestinationArchive) == modifiedZipArchives.end())
      if (fs::exists(destination))
        fs::copy_file(destination, destination + _patchTempExtension, fs::copy_option::overwrite_if_exists);

    if (fs::exists(destination + _patchOldExtension))
      fs::remove(destination + _patchOldExtension);
    
    ZipEntry inputEntry = PatchReadEntry(destination + _patchTempExtension, destination, shortDestinationArchive, entryName, modifiedZipArchives);
    if (inputEntry.Size == ((ULONGLONG)-1))
      return true;
    
    fs::path input = (fs::temp_directory_path() / fs::unique_path("PRDiffTempInput-%%%%-%%%%-%%%%-%%%%"));
    std::ofstream ofs(input.string().c_str(), std::ios::binary | std::ios::trunc);
    if (ofs.good()) {
      ofs.write((char*)inputEntry.Data, inputEntry.Size);
      ofs.close();
    } else {
      std::cerr << ConsoleUtils::Red << Utils::DescribeIosFailure(ofs) << ConsoleUtils::Default << std::endl;
      std::cerr << ConsoleUtils::Red << "Unable to write input file" << ConsoleUtils::Default << std::endl;
      return false;
    }

    bool isUpdateable = true;

    if (!Utils::ValidateFile(input.string(), beforeHash)) {
      isUpdateable = false;
    }

    if (isUpdateable) {
      fs::path patch = (fs::temp_directory_path() / fs::unique_path("PRDiffTempPatch-%%%%-%%%%-%%%%-%%%%"));
      std::ofstream ofs(patch.string().c_str(), std::ios::binary | std::ios::trunc);
      if (ofs.good()) {
        ofs.write((char*)source.Data, source.Size);
        ofs.close();
      } else {
        std::cerr << ConsoleUtils::Red << Utils::DescribeIosFailure(ofs) << ConsoleUtils::Default << std::endl;
        std::cerr << ConsoleUtils::Red << "Unable to write patch file" << ConsoleUtils::Default << std::endl;
        return false;
      }
      
      fs::path output = (fs::temp_directory_path() / fs::unique_path("PRDiffTempOutput-%%%%-%%%%-%%%%-%%%%"));

      BinaryPatch::Apply(input.string().c_str(), patch.string().c_str(), output.string().c_str());
      fs::remove(patch);

      if (fs::exists(output)) {
        if (!Utils::ValidateFile(output.string(), afterHash)) {
          std::cerr << ConsoleUtils::Red << "The result does not match the target file" << ConsoleUtils::Default << std::endl;
          return false;
        }

        std::ifstream ifs(output.string().c_str(), std::ios::binary);
        if (ifs.good()) {
          ifs.seekg(0, std::ios::end);
          outputSize = (size_t)ifs.tellg();
          ifs.seekg(0, std::ios::beg);
          outputData = new byte[outputSize];
          ifs.read((char*)outputData, outputSize);
          ifs.close();
        } else {
          std::cerr << ConsoleUtils::Red << Utils::DescribeIosFailure(ifs) << ConsoleUtils::Default << std::endl;
          std::cerr << ConsoleUtils::Red << "Unable to read output file" << ConsoleUtils::Default << std::endl;
          return false;
        }

        if (!PatchAddEntry(destination + _patchTempExtension, destination, shortDestinationArchive, entryName, outputData, outputSize, timestamp, modifiedZipArchives)) {
          delete outputData;
          std::cerr << ConsoleUtils::Red << "Error adding entry to archive" << ConsoleUtils::Default << std::endl;
          return false;
        }
        
        delete outputData;
      } else {
        std::cerr << ConsoleUtils::Red << "Error patching file" << ConsoleUtils::Default << std::endl;
        return false;
      }
    } else if (!Utils::ValidateFile(input.string(), afterHash)) {
      std::cerr << ConsoleUtils::Red << "The existing file does not match the target file" << ConsoleUtils::Default << std::endl;
      fs::remove(input);
      return false;
    }
  } catch (boost::filesystem::filesystem_error& e) {
    std::cerr << ConsoleUtils::Red << "Error modifying entry in zip archive: " << e.what() << ConsoleUtils::Default << std::endl;

    if (outputSize != ((size_t)-1))
      delete outputData;

    return false;
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Error modifying entry in zip archive: " << e.what() << ConsoleUtils::Default << std::endl;

    if (outputSize != ((size_t)-1))
      delete outputData;

    return false;
  }

  return true;
}

bool Build::ApplyPatchZipRemove(std::string destination, std::string entryName, std::string shortDestination, std::string shortDestinationArchive, ArchiveMapType& modifiedZipArchives)
{
  if (!fs::exists(destination))
    return true;
  
  std::cout << ConsoleUtils::DarkRed << "ZipRemove: " << ConsoleUtils::Default << shortDestination << std::endl;

  try {
    if (fs::exists(destination + _patchTempExtension) && modifiedZipArchives.find(shortDestinationArchive) == modifiedZipArchives.end())
      fs::remove(destination + _patchTempExtension);

    if (!fs::exists(destination + _patchTempExtension) || modifiedZipArchives.find(shortDestinationArchive) == modifiedZipArchives.end())
      fs::copy_file(destination, destination + _patchTempExtension, fs::copy_option::overwrite_if_exists);

    if (fs::exists(destination + _patchOldExtension))
      fs::remove(destination + _patchOldExtension);

    if (!PatchRemoveEntry(destination + _patchTempExtension, destination, shortDestinationArchive, entryName, modifiedZipArchives))
      return false;
  } catch (boost::filesystem::filesystem_error& e) {
    std::cerr << ConsoleUtils::Red << "Error removing entry from archive: " << e.what() << ConsoleUtils::Default << std::endl;
    return false;
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Error removing entry from archive: " << e.what() << ConsoleUtils::Default << std::endl;
    return false;
  }

  return true;
}

ZipEntry Build::PatchReadEntry(CZipArchive& zip, std::string entryName)
{
  try {
    ZIP_INDEX_TYPE index = zip.FindFile(entryName.c_str(), CZipArchive::ffNoCaseSens);
    if (index == ZIP_FILE_INDEX_NOT_FOUND) {
      return ZipEntry(NULL, -1);
    } else {
      CZipMemFile mfOut;
      mfOut.SetLength(zip[index]->m_uUncomprSize);
      ((CZipAbstractFile*)&mfOut)->SeekToBegin();
      
      if (!zip.ExtractFile(index, mfOut))
        return ZipEntry(NULL, -1);
      
      return ZipEntry(mfOut.Detach(), mfOut.GetLength());
    }
  } catch(CZipException& e) {
    std::cerr << ConsoleUtils::Red << "Unable to read from archive: " << e.m_iCause << " - " << e.GetErrorDescription() << ConsoleUtils::Default << std::endl;
    return ZipEntry(NULL, -1);
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Unable to read from archive: " << e.what() << ConsoleUtils::Default << std::endl;
    return ZipEntry(NULL, -1);
  }
}

ZipEntry Build::PatchReadEntry(std::string tempDestinationArchive, std::string destinationArchive, std::string shortDestinationArchive, std::string entryName, ArchiveMapType& modifiedZipArchives)
{
  CZipArchive* zip;

  try {
    ArchiveMapType::iterator iterator = modifiedZipArchives.find(shortDestinationArchive);

    if (iterator != modifiedZipArchives.end() && !iterator->second.Closed) {
      zip = iterator->second.Zip;
    } else {
      zip = new CZipArchive();

      PatchedZipFile zipFile;
      zipFile.TempFileName = tempDestinationArchive;
      zipFile.FileName = destinationArchive;
      zipFile.Zip = zip;
      zipFile.BufferSize = 0;
      zipFile.Closed = true;
      modifiedZipArchives.insert(std::pair<std::string, PatchedZipFile>(shortDestinationArchive, zipFile));
      iterator = modifiedZipArchives.find(shortDestinationArchive);
      if (iterator == modifiedZipArchives.end()) {
        std::cerr << ConsoleUtils::Red << "Error storing zip archive information" << ConsoleUtils::Default << std::endl;
        return ZipEntry(NULL, -1);
      }

      zip->SetAdvanced(4095, 4095, 4095);
      zip->SetCommitMode(CZipArchive::cmManual);

      if (!zip->Open(tempDestinationArchive.c_str(), fs::exists(tempDestinationArchive) ? CZipArchive::zipOpen : CZipArchive::zipCreate)) {
        std::cerr << ConsoleUtils::Red << "Error opening zip archive" << ConsoleUtils::Default << std::endl;
        return ZipEntry(NULL, -1);
      }
      
      iterator->second.Closed = false;

      if (!zip->SetCompressionMethod(CZipCompressor::methodDeflate)) {
        std::cerr << ConsoleUtils::Red << "Error setting compression method" << ConsoleUtils::Default << std::endl;
        return ZipEntry(NULL, -1);
      }
    }

    return PatchReadEntry(*zip, entryName);
  } catch(CZipException& e) {
    std::cerr << ConsoleUtils::Red << "Unable to read from archive: " << e.m_iCause << " - " << e.GetErrorDescription() << ConsoleUtils::Default << std::endl;
    return ZipEntry(NULL, -1);
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Unable to read from archive: " << e.what() << ConsoleUtils::Default << std::endl;
    return ZipEntry(NULL, -1);
  }
}

bool Build::PatchAddEntry(std::string tempDestinationArchive, std::string destinationArchive, std::string shortDestinationArchive, std::string entryName, byte* data, size_t length, time_t timestamp, ArchiveMapType& modifiedZipArchives)
{
  CZipArchive* zip;

  ArchiveMapType::iterator iterator = modifiedZipArchives.find(shortDestinationArchive);

  if (iterator != modifiedZipArchives.end() && !iterator->second.Closed) {
    zip = iterator->second.Zip;
  } else {
    zip = new CZipArchive();
      
    try {
      PatchedZipFile zipFile;
      zipFile.TempFileName = tempDestinationArchive;
      zipFile.FileName = destinationArchive;
      zipFile.Zip = zip;
      zipFile.BufferSize = 0;
      zipFile.Closed = true;
      modifiedZipArchives.insert(std::pair<std::string, PatchedZipFile>(shortDestinationArchive, zipFile));
      iterator = modifiedZipArchives.find(shortDestinationArchive);
      if (iterator == modifiedZipArchives.end()) {
        std::cerr << ConsoleUtils::Red << "Error storing zip archive information" << ConsoleUtils::Default << std::endl;
        return false;
      }

      zip->SetAdvanced(4095, 4095, 4095);
      zip->SetCommitMode(CZipArchive::cmManual);

      if (!zip->Open(tempDestinationArchive.c_str(), fs::exists(tempDestinationArchive) ? CZipArchive::zipOpen : CZipArchive::zipCreate)) {
        std::cerr << ConsoleUtils::Red << "Error opening zip archive" << ConsoleUtils::Default << std::endl;
        return false;
      }
      
      iterator->second.Closed = false;

      if (!zip->SetCompressionMethod(CZipCompressor::methodDeflate)) {
        std::cerr << ConsoleUtils::Red << "Error setting compression method" << ConsoleUtils::Default << std::endl;
        return false;
      }
    } catch(CZipException& e) {
      std::cerr << ConsoleUtils::Red << "Unable to add file to archive: " << e.m_iCause << " - " << e.GetErrorDescription() << ConsoleUtils::Default << std::endl;
      zip->Close(CZipArchive::afAfterException);
      return false;
    } catch (std::exception const& e) {
      std::cerr << ConsoleUtils::Red << "Unable to add file to archive: " << e.what() << ConsoleUtils::Default << std::endl;
      return false;
    }
  }

  try {
    ZIP_INDEX_TYPE index = zip->FindFile(entryName.c_str(), CZipArchive::ffNoCaseSens);
    if (index != ZIP_FILE_INDEX_NOT_FOUND) {
      if (!zip->RemoveFile(index)) {
        std::cerr << ConsoleUtils::Red << "Error deleting existing entry" << ConsoleUtils::Default << std::endl;
        return false;
      }
    }
    
    CZipFileHeader file;
    if (!file.SetFileName(entryName.c_str())) {
      std::cerr << ConsoleUtils::Red << "Error setting filename" << ConsoleUtils::Default << std::endl;
      return false;
    }
    
    if (!file.SetSystemAttr(FILE_ATTRIBUTE_NORMAL)) {
      std::cerr << ConsoleUtils::Red << "Error setting file attributes" << ConsoleUtils::Default << std::endl;
      return false;
    }
    
    file.SetCreationTime(timestamp);
    file.SetLastAccessTime(timestamp);
    file.SetModificationTime(timestamp, true);
    
    if (!zip->OpenNewFile(file, 6)) {
      std::cerr << ConsoleUtils::Red << "Error opening zip entry" << ConsoleUtils::Default << std::endl;
      return false;
    }
    
    if (!zip->WriteNewFile(data, length)) {
      std::cerr << "Error writing new file" << ConsoleUtils::Default << std::endl;
      return false;
    }
    
    if (!zip->CloseNewFile()) {
      std::cerr << ConsoleUtils::Red << "Error closing new file" << ConsoleUtils::Default << std::endl;
      return false;
    }
    
    iterator->second.BufferSize += length;

    if (iterator->second.BufferSize > _maximumBufferSize) {
      TouchArchive(*zip, timestamp);
      zip->Close();
      delete zip;
      iterator->second.Closed = true;
    }

    return true;
  } catch(CZipException& e) {
    std::cerr << ConsoleUtils::Red << "Unable to add file to archive: " << e.m_iCause << " - " << e.GetErrorDescription() << ConsoleUtils::Default << std::endl;
    zip->Close(CZipArchive::afAfterException);
    return false;
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Unable to add file to archive: " << e.what() << ConsoleUtils::Default << std::endl;
    return false;
  }
}

bool Build::PatchRemoveEntry(std::string tempDestinationArchive, std::string destinationArchive, std::string shortDestinationArchive, std::string entryName, ArchiveMapType& modifiedZipArchives)
{
  CZipArchive* zip;

  ArchiveMapType::iterator iterator = modifiedZipArchives.find(shortDestinationArchive);

  if (iterator != modifiedZipArchives.end() && !iterator->second.Closed) {
    zip = iterator->second.Zip;
  } else {
    zip = new CZipArchive();
      
    try {
      PatchedZipFile zipFile;
      zipFile.TempFileName = tempDestinationArchive;
      zipFile.FileName = destinationArchive;
      zipFile.Zip = zip;
      zipFile.BufferSize = 0;
      zipFile.Closed = true;
      modifiedZipArchives.insert(std::pair<std::string, PatchedZipFile>(shortDestinationArchive, zipFile));
      iterator = modifiedZipArchives.find(shortDestinationArchive);
      if (iterator == modifiedZipArchives.end()) {
        std::cerr << ConsoleUtils::Red << "Error storing zip archive information" << ConsoleUtils::Default << std::endl;
        return false;
      }

      zip->SetAdvanced(4095, 4095, 4095);
      zip->SetCommitMode(CZipArchive::cmManual);

      if (!zip->Open(tempDestinationArchive.c_str(), fs::exists(tempDestinationArchive) ? CZipArchive::zipOpen : CZipArchive::zipCreate)) {
        std::cerr << ConsoleUtils::Red << "Error opening zip archive" << ConsoleUtils::Default << std::endl;
        return false;
      }
      
      iterator->second.Closed = false;

      if (!zip->SetCompressionMethod(CZipCompressor::methodDeflate)) {
        std::cerr << ConsoleUtils::Red << "Error setting compression method" << ConsoleUtils::Default << std::endl;
        return false;
      }
    } catch(CZipException& e) {
      std::cerr << ConsoleUtils::Red << "Unable to remove file from archive: " << e.m_iCause << " - " << e.GetErrorDescription() << ConsoleUtils::Default << std::endl;
      zip->Close(CZipArchive::afAfterException);
      return false;
    } catch (std::exception const& e) {
      std::cerr << ConsoleUtils::Red << "Unable to remove file from archive: " << e.what() << ConsoleUtils::Default << std::endl;
      return false;
    }
  }

  try {
    ZIP_INDEX_TYPE index = zip->FindFile(entryName.c_str(), CZipArchive::ffNoCaseSens);
    if (index != ZIP_FILE_INDEX_NOT_FOUND) {
      if (!zip->RemoveFile(index)) {
        std::cerr << ConsoleUtils::Red << "Error deleting existing entry" << ConsoleUtils::Default << std::endl;
        return false;
      }
    }
  } catch(CZipException& e) {
    std::cerr << ConsoleUtils::Red << "Unable to remove file from archive: " << e.m_iCause << " - " << e.GetErrorDescription() << ConsoleUtils::Default << std::endl;
    zip->Close(CZipArchive::afAfterException);
    return false;
  } catch (std::exception const& e) {
    std::cerr << ConsoleUtils::Red << "Unable to remove file from archive: " << e.what() << ConsoleUtils::Default << std::endl;
    return false;
  }

  return true;
}

bool Build::TouchArchive(CZipArchive& zip, time_t timestamp)
{
  zip.SetFullFileTimes();
  zip.EnableFindFast();
  
  ZIP_INDEX_TYPE i = zip.GetCount();
  while (i > 0) {
    i--;

    CZipFileHeader* info = zip.GetFileInfo(i);

    if (!zip.ReadLocalHeader(i)) {
      std::cerr << ConsoleUtils::Red << "Error reading local header" << ConsoleUtils::Default << std::endl;
      return false;
    }
    
    info->SetCreationTime(timestamp);
    info->SetLastAccessTime(timestamp);
    info->SetModificationTime(timestamp, true);

    if (!zip.OverwriteLocalHeader(i)) {
      std::cerr << ConsoleUtils::Red << "Error overwriting local header" << ConsoleUtils::Default << std::endl;
      return false;
    }

    if (!zip.RemoveCentralDirectoryFromArchive()) {
      std::cerr << ConsoleUtils::Red << "Error removing central directory" << ConsoleUtils::Default << std::endl;
      return false;
    }
  }

  return true;
}

bool Build::TouchArchive(std::string path, time_t timestamp)
{
  if (!fs::exists(path))
    return true;

  CZipArchive zip;

  if (!zip.Open(path.c_str(), CZipArchive::zipOpen)) {
    std::cerr << ConsoleUtils::Red << "Error opening zip archive" << ConsoleUtils::Default << std::endl;
    return false;
  }
  
  if (!zip.SetCompressionMethod(CZipCompressor::methodDeflate)) {
    std::cerr << ConsoleUtils::Red << "Error setting compression method" << ConsoleUtils::Default << std::endl;
    return false;
  }

  bool res = TouchArchive(zip, timestamp);

  zip.Close();

  return res;
}

void Build::SafeMove(std::string sourceFileName, std::string targetFileName, std::string destinationBackupFileName)
{
  if (fs::exists(targetFileName)) {
    ClearAttributes(sourceFileName);
    ClearAttributes(targetFileName);
    fs::copy_file(targetFileName, destinationBackupFileName, fs::copy_option::overwrite_if_exists);
    ClearAttributes(destinationBackupFileName);
    fs::copy_file(sourceFileName, targetFileName, fs::copy_option::overwrite_if_exists);
    fs::remove(sourceFileName);
  } else {
    ClearAttributes(sourceFileName);
    fs::rename(sourceFileName, targetFileName);
    ClearAttributes(targetFileName);
  }
}

void Build::AttemptRestore(std::string destination, bool hasWritten)
{
  if (hasWritten && fs::exists(destination + _patchOldExtension)) {
    std::cout << "Attempting to restore previous file..." << std::endl;

    try {
      if (fs::exists(destination))
        fs::remove(destination);

      if (fs::exists(destination + _patchTempExtension))
        fs::remove(destination + _patchTempExtension);
    } catch (std::exception const&) {
      std::cerr << ConsoleUtils::Red << "Error restoring file!" << ConsoleUtils::Default << std::endl;
    }
  }
}

void Build::ClearAttributes(std::string target)
{
#if defined(WIN32)
  SetFileAttributes(target.c_str(), FILE_ATTRIBUTE_NORMAL);
#endif
}

time_t Build::from_iso_extended_string(const std::string& inDateString)
{
  if (inDateString.empty())
    return time(0);

  boost::posix_time::time_input_facet* input_facet = new boost::posix_time::time_input_facet();
  input_facet->set_iso_extended_format();
  input_facet->format("%Y-%m-%dT%H:%M:%S%F%Q");
  
  std::stringstream ss;
  ss.imbue(std::locale(ss.getloc(), input_facet));
  ss.str(inDateString);
  boost::posix_time::ptime timeFromString;
  ss >> timeFromString;

  boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
  boost::posix_time::time_duration diff(timeFromString - epoch);

  return diff.total_seconds();
}
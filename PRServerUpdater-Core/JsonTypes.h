#pragma once

#include <ctime>
#include <map>
#include <string>
#include <vector>
#include <boost/assign/list_of.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "Version.h"

struct UpdateData
{
  Version FromVersion;
  Version ToVersion;
  long Size;
  std::string Hash;
  boost::optional<std::string> Password;
  std::string Changelog;
  std::vector<std::string> Uris;
  std::string DownloadPath;
};

enum PatchUtility
{
  AcceptNewLicense,
  ReplaceExecutables,
  DeleteShaderCache
};

enum PatchItemType
{
  ZipAdd,
  ZipModify,
  ZipRemove,
  FileAdd,
  FileModify,
  FileRemove
};

static std::map<std::string, PatchUtility> PatchUtilityMap = boost::assign::map_list_of
  ("AcceptNewLicense", AcceptNewLicense)
  ("ReplaceExecutables", ReplaceExecutables)
  ("DeleteShaderCache", DeleteShaderCache);

static std::map<std::string, PatchItemType> PatchItemMap = boost::assign::map_list_of
  ("ZipAdd", ZipAdd)
  ("ZipModify", ZipModify)
  ("ZipRemove", ZipRemove)
  ("FileAdd", FileAdd)
  ("FileModify", FileModify)
  ("FileRemove", FileRemove);

struct PatchTouchData
{
  time_t Timestamp;
  std::vector<std::string> Archives;
};

struct PatchItem
{
  PatchItemType Method;
  boost::optional<std::string> Source;
  std::string Destination;
  boost::optional<std::string> Entry;
  boost::optional<std::string> BeforeHash;
  boost::optional<std::string> AfterHash;
  boost::optional<bool> IsReplacementExecutable;
  boost::optional<bool> IsImportantFile;
  boost::optional<bool> IsDirectory;
};

struct Patch
{
  ::Version Version;
  ::Version RequiresVersion;
  std::vector<PatchUtility> PrePatch;
  std::vector<PatchUtility> PostPatch;
  boost::optional<std::string> License;
  PatchTouchData TouchData;
  std::vector<PatchItem> PatchData;
};
#include "BinaryPatch.h"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include "ZipArchive/bzip2/bzlib.h"

#if defined(WIN32)
#pragma warning(disable:4996)
#endif

#if defined(LINUX) || defined(LINUX64)
#include <err.h>
#include <string.h>
#include <unistd.h>

#define fseek(x, y, z) fseeko(x, y, z)
#elif defined(WIN32)
#include <io.h>

template<class T1, class T2>
void err(int i, const char* str, T1 arg1, T2 arg2) {
  char lastErrorTxt[1024];
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,NULL,GetLastError(),0,lastErrorTxt,1024,NULL);
  fprintf(stderr, "%s",lastErrorTxt);
  fprintf(stderr, str, arg1, arg2);
  exit(i);
}
template<class T>
void err(int i, const char* str, T arg) {
  char lastErrorTxt[1024];
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,NULL,GetLastError(),0,lastErrorTxt,1024,NULL);
  fprintf(stderr, "%s",lastErrorTxt);
  fprintf(stderr, str, arg);
  exit(i);
}
void err(int i, const char* str) {
  char lastErrorTxt[1024];
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,NULL,GetLastError(),0,lastErrorTxt,1024,NULL);
  fprintf(stderr, "%s",lastErrorTxt);
  if (str!=NULL) {
    fprintf(stderr, "%s",str);
  }
  exit(i);
}
template<class T>
void errx(int i, const char* str, T arg) {
  fprintf(stderr, str, arg);
  exit(i);
}
void errx(int i, const char* str) {
  fprintf(stderr, "%s",str);
  exit(i);
}
#endif

static off_t offtin(u_char *buf)
{
  off_t y;

  y=buf[7]&0x7F;
  y=y*256;y+=buf[6];
  y=y*256;y+=buf[5];
  y=y*256;y+=buf[4];
  y=y*256;y+=buf[3];
  y=y*256;y+=buf[2];
  y=y*256;y+=buf[1];
  y=y*256;y+=buf[0];

  if(buf[7]&0x80) y=-y;

  return y;
}

void BinaryPatch::Apply(const char* input, const char* patch, const char* output)
{
  FILE *f, *cpf, *dpf, *epf;
  BZFILE *cpfbz2, *dpfbz2, *epfbz2;
  int cbz2err, dbz2err, ebz2err;
  int fd;
  ssize_t oldsize,newsize;
  ssize_t bzctrllen,bzdatalen;
  u_char header[32],buf[8];
  u_char *old, *_new;
  off_t oldpos,newpos;
  off_t ctrl[3];
  off_t lenread;
  off_t i;

  /* Open patch file */
  if ((f = fopen(patch, "rb")) == NULL)
    err(1, "fopen(%s)", patch);

  /*
  File format:
    0	8	"PRDIFF10"
    8	8	X
    16	8	Y
    24	8	sizeof(newfile)
    32	X	bzip2(control block)
    32+X	Y	bzip2(diff block)
    32+X+Y	???	bzip2(extra block)
  with control block a set of triples (x,y,z) meaning "add x bytes
  from oldfile to x bytes from the diff block; copy y bytes from the
  extra block; seek forwards in oldfile by z bytes".
  */

  /* Read header */
  if (fread(header, 1, 32, f) < 32) {
    if (feof(f))
      errx(1, "Corrupt patch\n");
    err(1, "fread(%s)", patch);
  }

  /* Check for appropriate magic */
  if (memcmp(header, "PRDIFF10", 8) != 0)
    errx(1, "Corrupt patch\n");

  /* Read lengths from header */
  bzctrllen=offtin(header+8);
  bzdatalen=offtin(header+16);
  newsize=offtin(header+24);
  if((bzctrllen<0) || (bzdatalen<0) || (newsize<0))
    errx(1,"Corrupt patch\n");

  /* Close patch file and re-open it via libbzip2 at the right places */
  if (fclose(f))
    err(1, "fclose(%s)", patch);
#if defined(WIN32)
  if ((cpf = fopen(patch, "rb")) == NULL)
#elif defined(LINUX) || defined(LINUX64)
  if ((cpf = fopen(patch, "r")) == NULL)
#endif
    err(1, "fopen(%s)", patch);
  if (fseek(cpf, 32, SEEK_SET))
    err(1, "fseeko(%s, %lld)", patch, (long long)32);
  if ((cpfbz2 = BZ2_bzReadOpen(&cbz2err, cpf, 0, 0, NULL, 0)) == NULL)
    errx(1, "BZ2_bzReadOpen, bz2err = %d", cbz2err);
#if defined(WIN32)
  if ((dpf = fopen(patch, "rb")) == NULL)
#elif defined(LINUX) || defined(LINUX64)
  if ((dpf = fopen(patch, "r")) == NULL)
#endif
    err(1, "fopen(%s)", patch);
  if (fseek(dpf, 32 + bzctrllen, SEEK_SET))
    err(1, "fseeko(%s, %lld)", patch, (long long)(32 + bzctrllen));
  if ((dpfbz2 = BZ2_bzReadOpen(&dbz2err, dpf, 0, 0, NULL, 0)) == NULL)
    errx(1, "BZ2_bzReadOpen, bz2err = %d", dbz2err);
#if defined(WIN32)
  if ((epf = fopen(patch, "rb")) == NULL)
#elif defined(LINUX) || defined(LINUX64)
  if ((epf = fopen(patch, "r")) == NULL)
#endif
    err(1, "fopen(%s)", patch);
  if (fseek(epf, 32 + bzctrllen + bzdatalen, SEEK_SET))
    err(1, "fseeko(%s, %lld)", patch, (long long)(32 + bzctrllen + bzdatalen));
  if ((epfbz2 = BZ2_bzReadOpen(&ebz2err, epf, 0, 0, NULL, 0)) == NULL)
    errx(1, "BZ2_bzReadOpen, bz2err = %d", ebz2err);
  
#if defined(WIN32)
  if(((fd=open(input,O_RDONLY|O_BINARY|O_NOINHERIT,0))<0) ||
    ((oldsize=lseek(fd,0,SEEK_END))==-1) ||
    ((old=(u_char*)malloc(oldsize+1))==NULL) ||
    (lseek(fd,0,SEEK_SET)!=0))
        err(1,"%s",input);
  int r=oldsize;
  while (r>0 && (i=read(fd,old+oldsize-r,r))>0) r-=i;
  if (r>0 || close(fd)==-1) err(1,"%s",input);
#elif defined(LINUX) || defined(LINUX64)
  if(((fd=open(input,O_RDONLY,0))<0) ||
    ((oldsize=lseek(fd,0,SEEK_END))==-1) ||
    ((old=(u_char*)malloc(oldsize+1))==NULL) ||
    (lseek(fd,0,SEEK_SET)!=0) ||
    (read(fd,old,oldsize)!=oldsize) ||
    (close(fd)==-1)) err(1,"%s",input);
#endif

  if((_new=(u_char*)malloc(newsize+1))==NULL)
    err(1,"Error");

  oldpos=0;newpos=0;
  while(newpos<newsize) {
    /* Read control data */
    for(i=0;i<=2;i++) {
      lenread = BZ2_bzRead(&cbz2err, cpfbz2, buf, 8);
      if ((lenread < 8) || ((cbz2err != BZ_OK) &&
          (cbz2err != BZ_STREAM_END)))
        errx(1, "Corrupt patch\n");
      ctrl[i]=offtin(buf);
    };

    /* Sanity-check */
    if(newpos+ctrl[0]>newsize)
      errx(1,"Corrupt patch\n");

    /* Read diff string */
    lenread = BZ2_bzRead(&dbz2err, dpfbz2, _new + newpos, ctrl[0]);
    if ((lenread < ctrl[0]) ||
        ((dbz2err != BZ_OK) && (dbz2err != BZ_STREAM_END)))
      errx(1, "Corrupt patch\n");

    /* Add old data to diff string */
    for(i=0;i<ctrl[0];i++)
      if((oldpos+i>=0) && (oldpos+i<oldsize))
        _new[newpos+i]+=old[oldpos+i];

    /* Adjust pointers */
    newpos+=ctrl[0];
    oldpos+=ctrl[0];

    /* Sanity-check */
    if(newpos+ctrl[1]>newsize)
      errx(1,"Corrupt patch\n");

    /* Read extra string */
    lenread = BZ2_bzRead(&ebz2err, epfbz2, _new + newpos, ctrl[1]);
    if ((lenread < ctrl[1]) ||
        ((ebz2err != BZ_OK) && (ebz2err != BZ_STREAM_END)))
      errx(1, "Corrupt patch\n");

    /* Adjust pointers */
      newpos+=ctrl[1];
      oldpos+=ctrl[2];
    };

  /* Clean up the bzip2 reads */
  BZ2_bzReadClose(&cbz2err, cpfbz2);
  BZ2_bzReadClose(&dbz2err, dpfbz2);
  BZ2_bzReadClose(&ebz2err, epfbz2);
  if (fclose(cpf) || fclose(dpf) || fclose(epf))
    err(1, "fclose(%s)", patch);
  
#if defined(WIN32)
  if(((fd=open(output,O_CREAT|O_TRUNC|O_WRONLY|O_BINARY,0666))<0) ||
#elif defined(LINUX) || defined(LINUX64)
  if(((fd=open(output,O_CREAT|O_TRUNC|O_WRONLY,0666))<0) ||
#endif

    (write(fd,_new,newsize)!=newsize) || (close(fd)==-1))
    err(1,"%s",output);

  free(_new);
  free(old);
}
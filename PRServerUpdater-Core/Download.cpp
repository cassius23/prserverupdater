#include "Download.h"

#include <boost/filesystem.hpp>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>
#include <cryptopp/sha.h>
#include <stdlib.h>

#include "ConsoleUtils.h"

namespace fs = boost::filesystem;

double Download::PROGRESS_UPDATE_INTERVAL = 0.1;

int Download::ToMemory(const char* url, std::vector<byte>& destination, const char* interfaceIP)
{
  CURL *curl;
  CURLcode res = CURLE_OK;

  curl = curl_easy_init();
  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteData);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&destination);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "PRServerNetRequest");
    //if (interfaceIP != NULL)
    //  curl_easy_setopt(curl, CURLOPT_INTERFACE, interfaceIP);

    res = curl_easy_perform(curl);

    if(res != CURLE_OK)
      std::cerr << std::endl << ConsoleUtils::Red << curl_easy_strerror(res) << ConsoleUtils::Default << std::endl;

    curl_easy_cleanup(curl);

    return (int)res;
  } else {
    return 1;
  }
}

int Download::ToFile(const char* url, const char* destination, const char* sha1, const char* interfaceIP)
{
  CURL *curl;
  CURLcode res = CURLE_OK;
  struct ProgressData prog;
  
  fs::path filename = fs::path(url).filename().string();
  fs::path destinationPath = fs::path(destination) / filename;

  if (sha1 != NULL) {
    if (fs::exists(destinationPath)) {
      if (ValidateFile(destinationPath.string().c_str(), sha1)) {
        std::cout << ConsoleUtils::DarkGreen << "File already exists that matches the required file hash. Skipping download..." << ConsoleUtils::Default << std::endl;
        return 0;
      }
    }
  }

  FILE *destinationFile;

  curl = curl_easy_init();
  if (curl) {
    prog.LastRunTime = 0;
    prog.Curl = curl;

    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, Progress);
    curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, &prog);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteFile);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "PRServerNetRequest");
    //if (interfaceIP != NULL)
    //  curl_easy_setopt(curl, CURLOPT_INTERFACE, interfaceIP);

#if defined(WIN32)
    fopen_s(&destinationFile, destinationPath.string().c_str(), "wb");
#elif defined(LINUX) || defined(LINUX64)
    destinationFile = fopen(destinationPath.string().c_str(), "wb");
#endif

    if (destinationFile != NULL) {
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, destinationFile);

      std::cout << "Downloading update to: " << destinationPath << "\n" << std::endl;

      res = curl_easy_perform(curl);

      fclose(destinationFile);
      ConsoleUtils::FreeProgress();
    } else {
      std::cerr << std::endl << ConsoleUtils::Red << "Unable to create file: " << destinationPath << ConsoleUtils::Default << std::endl;
      curl_easy_cleanup(curl);
      return 1;
    }

    if (res != CURLE_OK) {
      std::cerr << std::endl << ConsoleUtils::Red << curl_easy_strerror(res) << ConsoleUtils::Default << std::endl;
    } else {
      if (sha1 != NULL) {
        std::cout << std::endl << "Verifying file... ";
        if (!ValidateFile(destinationPath.string().c_str(), sha1)) {
          std::cerr << std::endl << ConsoleUtils::Red << "File downloaded does not match the required file hash. File is corrupt!" << ConsoleUtils::Default << std::endl;
          return 1;
        } else {
          std::cout << ConsoleUtils::DarkCyan << "Success!" << ConsoleUtils::Default << std::endl;
        }
      }
    }

    curl_easy_cleanup(curl);

    return (int)res;
  } else {
    return 1;
  }
}

bool Download::ValidateFile(const char* path, const char* sha1)
{
  std::string result;
  CryptoPP::SHA1 hash;
  CryptoPP::FileSource(path, true, new CryptoPP::HashFilter(hash, new CryptoPP::HexEncoder(new CryptoPP::StringSink(result), false)));
  return result.compare(sha1) == 0;
}

size_t Download::WriteData(void* ptr, size_t size, size_t nmemb, void* stream)
{
  size_t realsize = size * nmemb;
  std::vector<byte>* memory = (std::vector<byte>*)stream;
  
  memory->insert(memory->end(), (byte*)ptr, (byte*)ptr + realsize);
  return realsize;
}

size_t Download::WriteFile(void* ptr, size_t size, size_t nmemb, void* stream)
{
  int written = fwrite(ptr, size, nmemb, (FILE*)stream);
  return written;
}

int Download::Progress(void* p, double dltotal, double dlnow, double ultotal, double ulnow)
{
  struct ProgressData* prog = (struct ProgressData*)p;
  CURL* curl = prog->Curl;
  double currentTime = 0;

  curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &currentTime);

  if((currentTime - prog->LastRunTime) >= PROGRESS_UPDATE_INTERVAL) {
    prog->LastRunTime = currentTime;

    if (dltotal > 0)
      ConsoleUtils::FileProgress(dlnow, dltotal, currentTime);
  } else if (dltotal > 0 && dlnow == dltotal) {
      ConsoleUtils::FileProgress(dlnow, dltotal, currentTime);
  }

  return 0;
}
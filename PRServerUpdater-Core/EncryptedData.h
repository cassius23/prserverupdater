#pragma once

#include <string>

typedef unsigned char byte;

class EncryptedData
{
  public:
    static std::string Read(const byte* encryptedData, size_t size);
    static bool LoadKeyFile(std::string path, std::string cdKeyHash);
  private:
    static byte _key[32];
    static byte _iv[16];
};

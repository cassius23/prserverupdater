#pragma once

#if defined(WIN32)
typedef signed int ssize_t;
#endif

class BinaryPatch
{
  public:
    static void Apply(const char* input, const char* patch, const char* output);
};

#pragma once

#include <string>

class License
{
  public:
    static License Validate(void);

    bool Valid;
    std::string IP;
    unsigned short Port;
    std::string User;
    std::string ErrorCode;
    std::string ErrorMsg;

  private:
    void ReadServerSettings(std::string path);
    void ReadExternalIP(std::string path);
    void ReadLicenseKey(std::string path);
    void GetLicenseDetails(std::string ip, unsigned short port, std::string key, std::string interfaceIP = "");

    std::string _serverSettingsIP;
    unsigned short _serverSettingsPort;
    std::string _serverSettingsInterface;
    std::string _externalIP;
    std::string _licenseKey;
};


#include "Utils.h"

#include <boost/algorithm/string/trim.hpp>
#include <cerrno>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>
#include <cryptopp/sha.h>

#if defined(WIN32)
#pragma warning(disable:4996)
#endif

bool Utils::ValidateFile(std::string path, std::string sha1)
{
  std::string result;
  CryptoPP::SHA1 hash;
  CryptoPP::FileSource(path.c_str(), true, new CryptoPP::HashFilter(hash, new CryptoPP::HexEncoder(new CryptoPP::StringSink(result), false)));
  return result.compare(sha1) == 0;
}

bool Utils::ValidateBytes(byte* bytes, size_t length, std::string sha1)
{
  std::string result;
  CryptoPP::SHA1 hash;
  CryptoPP::ArraySource(bytes, length, true, new CryptoPP::HashFilter(hash, new CryptoPP::HexEncoder(new CryptoPP::StringSink(result), false)));
  return result.compare(sha1) == 0;
}

std::string Utils::FixPath(const std::string path)
{
#if defined(WIN32)
  return path;
#elif defined(LINUX) || defined(LINUX64)
  std::string newPath(path);
  std::replace(newPath.begin(), newPath.end(), '\\', '/');
  return newPath;
#endif
}

std::string Utils::DescribeIosFailure(const std::ios& stream)
{
  std::string result;

  if (stream.eof()) {
    result = "Unexpected end of file.";
  }
  
#if defined(WIN32)
  // GetLastError() gives more details than errno.
  else if (GetLastError() != 0) {
    result = FormatSystemMessage(GetLastError());
  }
#endif

  else if (errno) {
#if defined(LINUX) || defined(LINUX64)
    // We use strerror_r because it's threadsafe.
    // GNU's strerror_r returns a string and may ignore buffer completely.
    char buffer[255];
    result = std::string(strerror_r(errno, buffer, sizeof(buffer)));
#else
    result = std::string(strerror(errno));
#endif
  }

  else {
    result = "Unknown file error.";
  }

  boost::trim_right(result);  // from Boost String Algorithms library
  return result;
}

#if defined(WIN32)
std::string Utils::FormatSystemMessage(DWORD dwMsgId)
{
  char buf[4096];
  FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, 0, dwMsgId, 0, buf, sizeof buf, 0 );

  int len = strlen(buf);

  if ( len > 1 && buf[len-2] == '\r' && buf[len-1] == '\n' )
    buf[len-2] = 0;

  return buf;
}
#endif
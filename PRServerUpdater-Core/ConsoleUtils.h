#pragma once

#if defined(WIN32)
#include <conio.h>
#include <windows.h>
#endif

class ConsoleUtils
{
  public:
    enum ConsoleColor
    {
      Default = -1,
  #if defined(WIN32)
      Black = 0,
      DarkBlue = 1,
      DarkGreen = 2,
      DarkCyan = 3,
      DarkRed = 4,
      DarkMagenta = 5,
      DarkYellow = 6,
      DarkWhite = 7,
      Blue = 9,
      Green = 10,
      Cyan = 11,
      Red = 12,
      Magenta = 13,
      Yellow = 14,
      White = 15
  #elif defined(LINUX) || defined(LINUX64)
      Black = 30,
      DarkRed = 21,
      Red = 31,
      DarkGreen = 22,
      Green = 32,
      DarkYellow = 23,
      Yellow = 33,
      DarkBlue = 24,
      Blue = 34,
      DarkMagenta = 25,
      Magenta = 35,
      DarkCyan = 26,
      Cyan = 36,
      DarkWhite = 27,
      White = 37
  #endif
    };
    
    friend std::ostream& operator<<(std::ostream& os, const ConsoleColor& color);

    static void FileProgress(double, double, double);
    static void FreeProgress();
    static void InitializeColors();
    static void SetTextColor(ConsoleColor color);
  private:
    static void ResetTextColor();
    static void ProgressBar(double);
    static void ProgressDetail(double, double, double);
    static std::string FormatFileSize(double);
    
    static ConsoleColor OriginalForeground;
    static ConsoleColor OriginalBackground;

#if defined(WIN32)
    static HANDLE HConsolePosition;
    static HANDLE HConsoleColor;
    static CONSOLE_SCREEN_BUFFER_INFO ConsoleInfoPosition;
#elif defined(LINUX) || defined(LINUX64)
    static bool Initialized;
#endif

    static unsigned int DetailLength;
};

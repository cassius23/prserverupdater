#pragma once

#include <exception>

class Exception : public std::exception
{
  public:
    explicit Exception(const char* message)
      : _message(message)
    { }

    explicit Exception(const std::string& message)
      : _message(message)
    { }

    virtual ~Exception() throw()
    { }

    virtual const char* what() const throw()
    {
      return _message.c_str();
    }
  private:
    std::string _message;
};
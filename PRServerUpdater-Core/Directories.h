#pragma once

#include <boost/filesystem.hpp>

class Directories
{
  public:
    static boost::filesystem::path GetCurrentModDirectory();
    static boost::filesystem::path GetBF2RootDirectory();
    static boost::filesystem::path GetDownloadDirectory(bool displayOutput = false);
};


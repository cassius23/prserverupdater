#include "Directories.h"

#include <iostream>
#include "ConsoleUtils.h"
#include "Exception.h"

#if defined(WIN32)
#pragma warning(disable:4996)
#endif

boost::filesystem::path Directories::GetCurrentModDirectory()
{
  boost::filesystem::path path = boost::filesystem::path("..");

  if (!boost::filesystem::exists(path / "mod.desc"))
    throw Exception("Unable to find mod directory");

  return path;
}

boost::filesystem::path Directories::GetBF2RootDirectory()
{
  boost::filesystem::path path = GetCurrentModDirectory() / ".." / "..";

  if (!boost::filesystem::is_directory(path / "mods"))
    throw Exception("Unable to find root BF2 Server directory");

  return path;
}

boost::filesystem::path Directories::GetDownloadDirectory(bool displayOutput)
{
  const char* downloadDir = getenv("PR_DOWNLOAD_DIR");
  if (downloadDir == NULL) {
    if (displayOutput)
      std::cerr << ConsoleUtils::DarkRed << "Environment variable \"PR_DOWNLOAD_DIR\" does not exist." << ConsoleUtils::Default << std::endl;
    
    try {
      boost::filesystem::path path = boost::filesystem::temp_directory_path();
      if (displayOutput)
        std::cout << "Download Directory: " << ConsoleUtils::DarkCyan << path << ConsoleUtils::Default << std::endl;
      return path;
    } catch (boost::filesystem::filesystem_error e) {
      if (displayOutput)
        std::cerr << ConsoleUtils::DarkRed << "Unable to get temp directory\n" << e.what() << ConsoleUtils::Default << std::endl;
      
      boost::filesystem::path path = boost::filesystem::current_path();
      if (displayOutput)
        std::cout << "Download Directory: " << ConsoleUtils::DarkCyan << path << ConsoleUtils::Default << std::endl;
      return path;
    }
  } else {
    std::string path = std::string(downloadDir);
    if (displayOutput)
      std::cout << "Download Directory: " << ConsoleUtils::DarkCyan << path << ConsoleUtils::Default << std::endl;
    return boost::filesystem::path(path);
  }
}
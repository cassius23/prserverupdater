#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

class Version
{
  friend std::ostream& operator<<(std::ostream& os, const Version& dt);

  public:
    Version();
    Version(std::string const& input);
        
    Version(unsigned int major);
    Version(unsigned int major, unsigned int minor);
    Version(unsigned int major, unsigned int minor, unsigned int build);
    Version(unsigned int major, unsigned int minor, unsigned int build, unsigned int revision);
        
    static std::string CurrentVersion();
    static std::string ReadVersion(std::string path);
    static void WriteVersion(std::string path, Version version);

    bool IsZero() const;
    void SetZero();
    
    bool operator<(Version const& rhs) const;
    bool operator>(Version const& rhs) const;
    bool operator==(Version const& rhs) const;
    bool operator!=(Version const& rhs) const;
    bool operator<=(Version const& rhs) const;
    bool operator>=(Version const& rhs) const;

    unsigned int Major;
    unsigned int Minor;
    unsigned int Build;
    unsigned int Revision;
  private:
    void SetZero(int elements);
};

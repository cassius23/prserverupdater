////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

/**
* \file ZipMutex.h
*	Includes the ZipArchiveLib::CZipMutex class.
*
*/

#if !defined(ZIPARCHIVE_ZIPMUTEX_DOT_H)
#define ZIPARCHIVE_ZIPMUTEX_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif

#include "_features.h"

#ifdef _ZIP_SYSTEM_LINUX
	#include "ZipMutex_lnx.h"
#else
	#include "ZipMutex_win.h"
#endif


#endif


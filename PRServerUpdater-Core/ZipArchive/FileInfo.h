////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

/**
* \file FileInfo.h
*	Includes the ZipArchiveLib::CFileInfo class.
*
*/

#if !defined(ZIPARCHIVE_FILEINFO_DOT_H)
#define ZIPARCHIVE_FILEINFO_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif

#include "stdafx.h"
#include "ZipExport.h"
#include "ZipPlatform.h"

namespace ZipArchiveLib
{
	/**
		A structure holding a file or a directory information.
	*/
	struct ZIP_API CFileInfo
	{
	public:
		/**
			Initializes a new instance of the CFileInfo class.
		*/
		CFileInfo()
		{
			m_uSize = 0;
			m_uAttributes = 0;
			m_tCreationTime = m_tModificationTime = m_tLastAccessTime = (time_t)0;
		}
		ZIP_FILE_USIZE m_uSize;		///< The file size.
		DWORD m_uAttributes;		///< The file system attributes.
		time_t m_tCreationTime;		///< The file creation time.
		time_t m_tModificationTime;	///< The file modification time.
		time_t m_tLastAccessTime;	///< The file last access time.

		/**
			Returns the value indicating whether the current CFileInfo
			object represents a directory or a regular file.

			\return 
				\c true, if the current CFileInfo object represents 
				a directory; \c false, if it represents a regular file.
		*/
		bool IsDirectory() const
		{
			return ZipPlatform::IsDirectory(m_uAttributes);
		}
	};
}
#endif

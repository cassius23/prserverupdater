////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#ifdef _ZIP_BZIP2

#include "Bzip2Compressor.h"

namespace ZipArchiveLib
{

CBzip2Compressor::CBzip2Compressor(CZipStorage* pStorage)
	:CBaseLibCompressor(pStorage)
{		
	m_stream.bzalloc = (void *(*)(void*, int, int))_zipalloc;
	m_stream.bzfree = _zipfree;
}

void CBzip2Compressor::InitCompression(int iLevel, CZipFileHeader* pFile, CZipCryptograph* pCryptograph)
{
	CZipCompressor::InitCompression(iLevel, pFile, pCryptograph);
	SetOpaque(&m_stream.opaque, &m_options);
	if (iLevel == levelDefault)
		iLevel = 6;
	else if (iLevel == 0)
	{
		// it should not happen, please notify the author
		ASSERT(FALSE);
		iLevel = 1;
	}
	// use default work factor (30)
	CheckForError(BZ2_bzCompressInit(&m_stream, iLevel, 0, 0));

	m_stream.avail_in = 0;
	m_stream.avail_out = (UINT)m_pBuffer.GetSize();
	m_stream.next_out = (char*)m_pBuffer;
}

void CBzip2Compressor::Compress(const void *pBuffer, DWORD uSize)
{ 
	m_stream.next_in = (char*)pBuffer;
	m_stream.avail_in = (UINT)uSize;
	UpdateFileCrc(pBuffer, uSize);	
	
	while (m_stream.avail_in > 0)
	{
		if (m_stream.avail_out == 0)
		{
			FlushWriteBuffer();
			m_stream.avail_out = (UINT)m_pBuffer.GetSize();
			m_stream.next_out = (char*)m_pBuffer;
		}
		
		ZIP_SIZE_TYPE uTotal = GetStreamTotalOut();
		CheckForError(BZ2_bzCompress(&m_stream,  BZ_RUN));			
		m_uComprLeft += GetStreamTotalOut() - uTotal;
	}
}

void CBzip2Compressor::FinishCompression(bool bAfterException)
{
	m_stream.avail_in = 0;
	if (!bAfterException)
	{
		int err;
		for(;;)
		{
			if (m_stream.avail_out == 0)
			{
				FlushWriteBuffer();
				m_stream.avail_out = m_pBuffer.GetSize();
				m_stream.next_out = (char*)m_pBuffer;
			}
			ZIP_SIZE_TYPE uTotal = GetStreamTotalOut();
			err = BZ2_bzCompress(&m_stream,  BZ_FINISH);			
			m_uComprLeft += GetStreamTotalOut() - uTotal;
			if (err == BZ_STREAM_END)
				break;
			CheckForError(err);
		}

		if (m_uComprLeft > 0)
			FlushWriteBuffer();
		
		CheckForError(BZ2_bzCompressEnd(&m_stream));
		
		// it may be increased by the encrypted header size in CZipFileHeader::PrepareData
		m_pFile->m_uComprSize += GetStreamTotalOut();
		m_pFile->m_uUncomprSize = GetStreamTotalIn();
	}
	EmptyPtrList();
	ReleaseBuffer();
}

void CBzip2Compressor::InitDecompression(CZipFileHeader* pFile, CZipCryptograph* pCryptograph)
{
	CBaseLibCompressor::InitDecompression(pFile, pCryptograph);
	SetOpaque(&m_stream.opaque, &m_options);
	CheckForError(BZ2_bzDecompressInit(&m_stream, 0, m_options.m_bUseSlowDecompression ? 1 : 0));	
	m_stream.avail_in = 0;
}

DWORD CBzip2Compressor::Decompress(void *pBuffer, DWORD uSize)
{
	if (m_bDecompressionDone)
		return 0;

	m_stream.next_out = (char*)pBuffer;
	m_stream.avail_out = uSize > m_uUncomprLeft 
		? (DWORD)m_uUncomprLeft : uSize;
			
	DWORD uRead = 0;

	// may happen when the file is 0 sized
	bool bForce = m_stream.avail_out == 0 && m_uComprLeft > 0;
	while (m_stream.avail_out > 0 || (bForce && m_uComprLeft > 0))
	{
		if (m_stream.avail_in == 0 && m_uComprLeft > 0)
		{
			DWORD uToRead = FillBuffer();
			
			m_stream.next_in = (char*)m_pBuffer;
			m_stream.avail_in = uToRead;
		}
		
		
		ZIP_SIZE_TYPE uTotal = GetStreamTotalOut();
		char* pOldBuf =  m_stream.next_out;
		int ret = BZ2_bzDecompress(&m_stream);
		// will not exceed DWORD
		DWORD uToCopy = (DWORD)(GetStreamTotalOut() - uTotal);
		if (uToCopy)
		{
			UpdateCrc(pOldBuf, uToCopy);
		
			m_uUncomprLeft -= uToCopy;
			uRead += uToCopy;
		}
        
		if (ret == BZ_STREAM_END)
		{
			m_bDecompressionDone = true;
			return uRead;
		}
		else
			CheckForError(ret);

		// truncated data
		if (!uToCopy && m_stream.avail_in == 0 && m_uComprLeft == 0)
			break;
	}

	if (!uRead && uSize != 0)
		ThrowError(CZipException::badZipFile);
	
	return uRead;
}

void CBzip2Compressor::FinishDecompression(bool bAfterException)
{	
	if (!bAfterException)
		BZ2_bzDecompressEnd(&m_stream);
	EmptyPtrList();
	ReleaseBuffer();
}

} // namespace


#ifdef BZ_NO_STDIO
extern "C"
{
	void bz_internal_error ( int )
	{
		CZipException::Throw(CZipException::bzInternalError);
	}
}
#endif 

#endif

////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ZipFileHeader.h"
#include "ZipAutoBuffer.h"
#include "ZipArchive.h"
#include "ZipPlatform.h"
#include "ZipCompatibility.h"
#include <time.h>

#include "ZipCrc32Cryptograph.h"
#include "BytesWriter.h"
#include "zlib/zlib.h"

#define FILEHEADERSIZE	46
#define LOCALFILEHEADERSIZE	30
#ifdef _ZIP_UNICODE_CUSTOM
  #define ZIP_EXTRA_ZARCH_NAME_VER 1
#endif
#ifdef _ZIP_AES
  #define ZIP_EXTRA_WINZIP_DATA_SIZE 7
  #define ZIP_EXTRA_WINZIP_VENDOR 0x4541 // AE
#endif
#ifdef _ZIP_UNICODE
  #define FLAG_EFS 0x800
  #define ZIP_EXTRA_UNICODE_CP CP_UTF8
  // use a common version for both Unicode extra headers
  #define ZIP_EXTRA_UNICODE_VERSION 1
#endif
#define ZIP_EXTRA_NTFS_TAG 0x0001

using namespace ZipArchiveLib;

char CZipFileHeader::m_gszSignature[] = {0x50, 0x4b, 0x01, 0x02};
char CZipFileHeader::m_gszLocalSignature[] = {0x50, 0x4b, 0x03, 0x04};
CZipFileHeader::CZipFileHeader()
{
  Initialize(NULL);
  SetSystemCompatibility(ZipPlatform::GetSystemID());
}

CZipFileHeader::CZipFileHeader(CZipCentralDir* pCentralDir)
{
  Initialize(pCentralDir);
}

CZipFileHeader::~CZipFileHeader()
{
  
}

void CZipFileHeader::Initialize(CZipCentralDir* pCentralDir)
{
  m_uExternalAttr = 0;//ZipPlatform::GetDefaultAttributes();
  m_uModDate = m_uModTime = 0;
  m_tModificationTime = m_tCreationTime = m_tLastAccessTime = (time_t)0;
  m_uMethod = CZipCompressor::methodDeflate;
  m_uVersionMadeBy = 0;
  m_uCrc32 = 0;
  // initialize to 0, because on 64 bit platform unsigned long is 8 byte and we are copying only 4 bytes in Read()
  m_uComprSize = m_uUncomprSize = m_uOffset = 0;
  m_uLocalFileNameSize = 0;
  m_uLocalComprSize = m_uLocalUncomprSize = 0;
  m_uLocalHeaderSize = 0;
  m_uVolumeStart = 0;
  m_uEncryptionMethod = CZipCryptograph::encNone;
  m_bIgnoreCrc32 = false;
  m_uFlag = 0;
  m_pCentralDir = pCentralDir;
  m_state = sfNone;
}

bool CZipFileHeader::Read(bool bReadSignature)
{
  m_state = sfNone;

  CZipStorage *pStorage = m_pCentralDir->GetStorage();
  WORD uFileNameSize, uCommentSize, uExtraFieldSize;
  CZipAutoBuffer buf(FILEHEADERSIZE);
  if (bReadSignature)
  {
    pStorage->Read(buf, FILEHEADERSIZE, true);	
    if (!VerifySignature(buf))
      return false;
  }
  else
    pStorage->Read((char*)buf + 4, FILEHEADERSIZE - 4, true);	

  WORD uVersionMadeBy;
  CBytesWriter::ReadBytes(uVersionMadeBy,	buf + 4);
  CBytesWriter::ReadBytes(m_uVersionNeeded,	buf + 6);
  CBytesWriter::ReadBytes(m_uFlag,			buf + 8);
  CBytesWriter::ReadBytes(m_uMethod,			buf + 10);
  CBytesWriter::ReadBytes(m_uModTime,			buf + 12);
  CBytesWriter::ReadBytes(m_uModDate,			buf + 14);
  CBytesWriter::ReadBytes(m_uCrc32,			buf + 16);
  CBytesWriter::ReadBytes(m_uComprSize,		buf + 20, 4);
  CBytesWriter::ReadBytes(m_uUncomprSize,		buf + 24, 4);
  CBytesWriter::ReadBytes(uFileNameSize,		buf + 28);
  CBytesWriter::ReadBytes(uExtraFieldSize,	buf + 30);
  CBytesWriter::ReadBytes(uCommentSize,		buf + 32);
  CBytesWriter::ReadBytes(m_uVolumeStart,		buf + 34, 2);
  CBytesWriter::ReadBytes(m_uInternalAttr,	buf + 36);
  CBytesWriter::ReadBytes(m_uExternalAttr,	buf + 38);
  CBytesWriter::ReadBytes(m_uOffset,			buf + 42, 4);
  buf.Release();

  m_uVersionMadeBy = uVersionMadeBy & 0xFF;
  SetSystemCompatibility((uVersionMadeBy & 0xFF00) >> 8);

  // we may need to modify this later
  m_uEncryptionMethod = (BYTE)((m_uFlag & (WORD) 1) != 0 ? CZipCryptograph::encStandard : CZipCryptograph::encNone);
  
  ZIP_VOLUME_TYPE uCurDsk = pStorage->GetCurrentVolume();	
  m_fileName.m_buffer.Allocate(uFileNameSize); // don't add NULL at the end
  pStorage->Read(m_fileName.m_buffer, uFileNameSize, true);
  
#ifdef _ZIP_UNICODE_CUSTOM
  const CZipStringStoreSettings& stringStoreSettings = m_pCentralDir->GetStringStoreSettings();

  if (stringStoreSettings.IsStandardNameCodePage())
    m_stringSettings.SetDefaultNameCodePage(GetSystemCompatibility());
  else
    m_stringSettings.m_uNameCodePage = stringStoreSettings.m_uNameCodePage;

  if (!stringStoreSettings.IsStandardCommentCodePage())
    m_stringSettings.m_uCommentCodePage = stringStoreSettings.m_uCommentCodePage;
#endif

  if (!m_aCentralExtraData.Read(pStorage, uExtraFieldSize))
    return false;

  CZipExtraData* pExtra = m_aCentralExtraData.Lookup(ZIP_EXTRA_NTFS);
  if (pExtra != NULL && pExtra->m_data.GetSize() >= 32)
  {
    WORD temp;
    CBytesWriter::ReadBytes(temp, pExtra->m_data + 4);			
    if (temp == ZIP_EXTRA_NTFS_TAG)
    {
      CBytesWriter::ReadBytes(temp, pExtra->m_data + 6);			
      if (temp == 24)
      {
        m_tModificationTime = ReadFileTime(pExtra->m_data + 8);
        m_tCreationTime = ReadFileTime(pExtra->m_data + 16);
        m_tLastAccessTime = ReadFileTime(pExtra->m_data + 24);
      }
    }
  }

#ifdef _ZIP_UNICODE
  if ((m_uFlag & (WORD)FLAG_EFS) != 0)
  {
    m_state.Set(sfStringsUnicode);
  }

  pExtra = m_aCentralExtraData.Lookup(ZIP_EXTRA_UNICODE_PATH);
  if (pExtra != NULL)
  {
    WORD uExtraDataSize = (WORD)pExtra->m_data.GetSize();		
    if (uExtraDataSize >= 5 && pExtra->m_data[0] == ZIP_EXTRA_UNICODE_VERSION)
    {
      DWORD checksum;
      CBytesWriter::ReadBytes(checksum, pExtra->m_data + 1);			
      if (zarch_crc32(0, (zarch_Bytef*)(char*)m_fileName.m_buffer, uFileNameSize) == checksum)
      {
        int size = uExtraDataSize - 5;
        if (size > 0)
        {
          m_fileName.m_buffer.Allocate(size);
          memcpy(m_fileName.m_buffer, pExtra->m_data + 5, size);
          // convert right away, so as to use OEM encoding in regular headers
          if (!m_state.IsSetAny(sfStringsUnicode))
          {
            m_fileName.AllocateString();
            ZipCompatibility::ConvertBufferToString(m_fileName.GetString(), m_fileName.m_buffer, ZIP_EXTRA_UNICODE_CP);
            m_fileName.ClearBuffer();
            ZipCompatibility::NormalizePathSeparators(m_fileName.GetString());
          }
        }
      }			
      // write the new data anyway if crc32 is mismatched
      m_state.Set(sfFileNameExtra);
    }		
  }
#endif

#ifdef _ZIP_UNICODE_CUSTOM
  if (m_state == sfNone)
  {
    pExtra = m_aCentralExtraData.Lookup(ZIP_EXTRA_ZARCH_NAME);
    if (pExtra != NULL)
    {	
      m_state = sfCustomUnicode;

      WORD uExtraDataSize = (WORD)pExtra->m_data.GetSize();
      int offset = 1;
      if (offset > uExtraDataSize)
        return false;
      if (pExtra->m_data[0] <= ZIP_EXTRA_ZARCH_NAME_VER) // else don't parse it
      {
        offset++;
        if (offset > uExtraDataSize)
          return false;
        BYTE flag = pExtra->m_data[1];
        bool bReadCommentCp = (flag & 4) != 0;
        if ((flag & 1) != 0)
        {
          // code page present
          if (offset + 4 > uExtraDataSize)
            return false;
          // avoid warnings
          DWORD temp;
          CBytesWriter::ReadBytes(temp, pExtra->m_data + offset);
          m_stringSettings.m_uNameCodePage = temp;
          offset += 4;
        }

        if ((flag & 3) == 3)
        {
          m_stringSettings.m_bStoreNameInExtraData = true;
          int iFileNameSize = pExtra->m_data.GetSize() - 2 - 4;
          if (bReadCommentCp)
            iFileNameSize -= 4;
          // code page present
          if (offset + iFileNameSize > uExtraDataSize || iFileNameSize <= 0)
            return false;
          // just in case
          m_fileName.ClearString();
          m_fileName.m_buffer.Allocate(iFileNameSize);
          memcpy(m_fileName.m_buffer, pExtra->m_data + offset, iFileNameSize);
          offset += iFileNameSize;
        }
        else
          m_stringSettings.m_bStoreNameInExtraData = false;

        if (bReadCommentCp)
        {
          // code page present
          if (offset + 4 > uExtraDataSize)
            return false;
          DWORD temp;
          CBytesWriter::ReadBytes(temp, pExtra->m_data + offset);
          m_stringSettings.m_uCommentCodePage = temp;
          // offset += 4;
        }
      }			
    }
    else if ((m_pCentralDir->GetUnicodeMode() & CZipArchive::umCustom) != 0)
    {
      m_state = sfCustomUnicode;
    }
  }
#endif

#ifdef _ZIP_ZIP64	
  //if (m_uVersionNeeded >= ZIP_ZIP64_ARCHIVE_VERSION) // some programs do not write the correct version
  {		
    pExtra = m_aCentralExtraData.Lookup(ZIP_EXTRA_PKZIP);
    if (pExtra != NULL)
    {		
      WORD offset = 0;
      WORD uExtraDataSize = (WORD)pExtra->m_data.GetSize();
      if (m_uUncomprSize == 0xFFFFFFFF)
      {			
        if (offset + 8 > uExtraDataSize)
          return false;
        CBytesWriter::ReadBytes(m_uUncomprSize,	pExtra->m_data + offset, 8);
        offset += 8;
      }
      if (m_uComprSize == 0xFFFFFFFF)
      {
        if (offset + 8 > uExtraDataSize)
          return false;
        CBytesWriter::ReadBytes(m_uComprSize, pExtra->m_data + offset, 8);
        offset += 8;
      }
      if (m_uOffset == 0xFFFFFFFF)
      {
        if (offset + 8 > uExtraDataSize)
          return false;
        CBytesWriter::ReadBytes(m_uOffset,	pExtra->m_data + offset, 8);
        offset += 8;
      }
      if (m_uVolumeStart == 0xFFFF)
      {
        if (offset + 4 > uExtraDataSize)
          return false;
        CBytesWriter::ReadBytes(m_uVolumeStart, pExtra->m_data + offset, 4);
      }		
    }
  }
#endif

#ifdef _ZIP_AES
  if (m_uMethod == CZipCompressor::methodWinZipAes && IsEncrypted())
  {
    pExtra = m_aCentralExtraData.Lookup(ZIP_EXTRA_WINZIP_AES);
    if (pExtra != NULL)
    {
      WORD uVersion;
      if (ReadWinZipAesExtra(pExtra->m_data, uVersion, m_uEncryptionMethod, m_uMethod))
        m_bIgnoreCrc32 = uVersion == 0x0002;
    }
  }
#endif	

  if (uCommentSize)
  {
    m_comment.m_buffer.Allocate(uCommentSize);
    pStorage->Read(m_comment.m_buffer, uCommentSize, true);
  }
#ifdef _ZIP_UNICODE
  pExtra = m_aCentralExtraData.Lookup(ZIP_EXTRA_UNICODE_COMMENT);
  if (pExtra != NULL)
  {		
    WORD uExtraDataSize = (WORD)pExtra->m_data.GetSize();		
    if (uExtraDataSize >= 5 && pExtra->m_data[0] == ZIP_EXTRA_UNICODE_VERSION)
    {
      DWORD checksum;
      CBytesWriter::ReadBytes(checksum, pExtra->m_data + 1);			
      if (zarch_crc32(0, (zarch_Bytef*)(char*)m_comment.m_buffer, uCommentSize) == checksum)
      {
        int size = uExtraDataSize - 5;
        if (size > 0)
        {
          m_comment.m_buffer.Allocate(size);
          memcpy(m_comment.m_buffer, pExtra->m_data + 5, size);					
          // convert right away, so as to use ASCII encoding in regular headers
          if (!m_state.IsSetAny(sfStringsUnicode))
          {
            m_comment.AllocateString();
            ZipCompatibility::ConvertBufferToString(m_comment.GetString(), m_comment.m_buffer, ZIP_EXTRA_UNICODE_CP);
            m_comment.ClearBuffer();
          }
        }
      }
      // write the new data anyway if crc32 is mismatched
      m_state.Set(sfCommentExtra);
    }		
  }
#endif

  m_aCentralExtraData.RemoveInternalHeaders();

  return pStorage->GetCurrentVolume() == uCurDsk || pStorage->IsBinarySplit(); // check that the whole header is in one volume
}

#ifdef _ZIP_AES

bool CZipFileHeader::ReadWinZipAesExtra(CZipAutoBuffer& data, WORD& uVersion, BYTE& uStrength, WORD& uCompressionMethod)
{
  if (data.GetSize() < ZIP_EXTRA_WINZIP_DATA_SIZE)
    return false;
  CBytesWriter::ReadBytes(uVersion, data);
  if (uVersion != 0x0001 && uVersion != 0x0002)
    return false;
  WORD uVendor;
  CBytesWriter::ReadBytes(uVendor, data + 2);
  if (uVendor != ZIP_EXTRA_WINZIP_VENDOR)
    return false;
  uStrength = (BYTE)data[4];	
  if (uStrength < 1 || uStrength > 3)
    return false;
  CBytesWriter::ReadBytes(uCompressionMethod, data + 5);
  return true;
}
void CZipFileHeader::WriteWinZipAesExtra(bool local)
{	
  CZipExtraField* field = local ? &m_aLocalExtraData : &m_aCentralExtraData;
  CZipExtraData* pExtra = field->CreateNew(ZIP_EXTRA_WINZIP_AES);
  pExtra->m_data.Allocate(ZIP_EXTRA_WINZIP_DATA_SIZE);
  WORD uTemp = m_bIgnoreCrc32 ? 0x0002 : 0x0001;
  // the version
  char* data = (char*)pExtra->m_data;
  CBytesWriter::WriteBytes(data, uTemp);
  uTemp = ZIP_EXTRA_WINZIP_VENDOR;
  CBytesWriter::WriteBytes(data + 2, uTemp);
  data[4] = (char)m_uEncryptionMethod;
  CBytesWriter::WriteBytes(data + 5, m_uMethod);
}
#endif

time_t CZipFileHeader::GetModificationTime()const
{
  if (m_tModificationTime != (time_t) 0)
  {
    return m_tModificationTime;
  }
  struct tm atm;
  atm.tm_sec = (m_uModTime & ~0xFFE0) << 1;
  atm.tm_min = (m_uModTime & ~0xF800) >> 5;
  atm.tm_hour = m_uModTime >> 11;

  atm.tm_mday = m_uModDate & ~0xFFE0;
  atm.tm_mon = ((m_uModDate & ~0xFE00) >> 5) - 1;
  atm.tm_year = (m_uModDate >> 9) + 80;
  atm.tm_isdst = -1;
  return mktime(&atm);
}

void CZipFileHeader::SetModificationTime(const time_t& ttime, bool bFullResolution)
{
  if (bFullResolution)
  {
    m_tModificationTime = ttime;
  }	
  else
  {
    m_tModificationTime = m_tCreationTime = m_tLastAccessTime = (time_t)0;
  }
#if _MSC_VER >= 1400
  tm gts;
  tm* gt = &gts;
  localtime_s(gt, &ttime);
#else
  tm* gt = localtime(&ttime);
#endif
  WORD year, month, day, hour, min, sec;
  if (gt == NULL)
  {
    year = 0;
    month = day = 1;
    hour = min = sec = 0;
  }
  else
  {
    year = (WORD)(gt->tm_year + 1900);		
    if (year <= 1980)
      year = 0;
    else
      year -= 1980;
    month = (WORD)gt->tm_mon + 1;
    day = (WORD)gt->tm_mday;
    hour = (WORD)gt->tm_hour;
    min = (WORD)gt->tm_min;
    sec = (WORD)gt->tm_sec;
  }
      
    m_uModDate = (WORD) (day + ( month << 5) + (year << 9));
    m_uModTime = (WORD) ((sec >> 1) + (min << 5) + (hour << 11));
}

void CZipFileHeader::WriteFileTime(const time_t& ttime, char* buffer)
{
  FILETIME ft;
  ZipPlatform::ConvertTimeToFileTime(ttime, ft);
  CBytesWriter::WriteBytes(buffer, ft.dwLowDateTime);
  CBytesWriter::WriteBytes(buffer + 4, ft.dwHighDateTime);	
}

time_t CZipFileHeader::ReadFileTime(const char* buffer)
{
  FILETIME ft = {0};
  CBytesWriter::ReadBytes(ft.dwLowDateTime, buffer);
  CBytesWriter::ReadBytes(ft.dwHighDateTime, buffer + 4);	
  time_t ret;
  return ZipPlatform::ConvertFileTimeToTime(ft, ret) ? ret : (time_t)0;
}


// write the header to the central dir
DWORD CZipFileHeader::Write(CZipStorage *pStorage)
{
  m_aCentralExtraData.RemoveInternalHeaders();
  if (m_tModificationTime != (time_t)0 || m_tCreationTime != (time_t)0 || m_tLastAccessTime != (time_t)0)
  {
    CZipExtraData* pExtra = m_aCentralExtraData.CreateNew(ZIP_EXTRA_NTFS);
    CZipAutoBuffer& buf = pExtra->m_data;
    buf.Allocate(32, true);
    // first 4 bytes are reserved
    WORD temp = ZIP_EXTRA_NTFS_TAG; // tag for the attribute
    CBytesWriter::WriteBytes((char*)(buf + 4), temp);
    temp = 3 * 8; // the size of the attribute
    CBytesWriter::WriteBytes((char*)(buf + 6), temp);

    WriteFileTime(m_tModificationTime, (char*)(buf + 8));
    WriteFileTime(m_tCreationTime, (char*)(buf + 16));
    WriteFileTime(m_tLastAccessTime, (char*)(buf + 24));
  }
#ifdef _ZIP_ZIP64
  
  if (NeedsZip64())
  {
    CZipAutoBuffer buf;
    
    CZipExtraData* pExtra = m_aCentralExtraData.CreateNew(ZIP_EXTRA_PKZIP);
    buf.Allocate( 28 );
    int size = 0;
    if (m_uUncomprSize >= UINT_MAX)
    {
      CBytesWriter::WriteBytes(buf, m_uUncomprSize, 8);
      size += 8;
    }
    if (m_uComprSize >= UINT_MAX)
    {
      CBytesWriter::WriteBytes((char*)(buf + size), m_uComprSize, 8);
      size += 8;
    }
    if (m_uOffset >= UINT_MAX)
    {
      CBytesWriter::WriteBytes((char*)(buf + size), m_uOffset, 8);
      size += 8;
    }
    if (m_uVolumeStart >= USHRT_MAX)
    {
      CBytesWriter::WriteBytes((char*)(buf + size), m_uVolumeStart, 4);
      size += 4;
    }
    // now we know, how many bytes we need to allocate
    pExtra->m_data.Allocate(size);
    memcpy(pExtra->m_data, buf, size);
    if (m_uVersionNeeded < ZIP_ZIP64_ARCHIVE_VERSION)
      m_uVersionNeeded = ZIP_ZIP64_ARCHIVE_VERSION;
  }
#endif
  
#ifdef _ZIP_AES
  WORD uMethod;
  if (IsWinZipAesEncryption())
  {
    WriteWinZipAesExtra(false);
    uMethod = CZipCompressor::methodWinZipAes;
  }
  else
    uMethod = m_uMethod;
#else
  WORD uMethod = m_uMethod;
#endif

  PrepareStringBuffers();
#ifdef _ZIP_UNICODE
  // write before preparing the buffers, to access the original string for conversion
  WriteUnicodeExtra(false, true);
  WriteUnicodeExtra(false, false);
#endif	

  if (!CheckLengths(false))
    CZipException::Throw(CZipException::tooLongData);

#ifdef _ZIP_UNICODE_CUSTOM
  if (m_state == sfCustomUnicode)
  {
    if (m_stringSettings.m_bStoreNameInExtraData)
    {
      if (!m_fileName.HasString() && m_fileName.HasBuffer())
        GetFileName(false); // don't clear the buffer, it will be needed in a moment
      ASSERT(m_fileName.HasString());
      ASSERT(m_fileName.HasBuffer());
      if (m_fileName.GetString().GetLength() == 0)
        m_stringSettings.m_bStoreNameInExtraData = false;
    }

    if (!m_stringSettings.IsStandard(GetSystemCompatibility()))
    {
      CZipExtraData* pExtra = m_aCentralExtraData.CreateNew(ZIP_EXTRA_ZARCH_NAME);
      bool bWriteCommentCp = !m_stringSettings.IsStandardCommentCodePage(GetSystemCompatibility());

      BYTE flag = 0;				
      int offset = 2;
      char* data = NULL;
      if (m_stringSettings.m_bStoreNameInExtraData)
      {
        CZipAutoBuffer buffer;
        // m_fileName.m_buffer contains CP_ACP page, we don't check if the current page is CP_ACP - too large dependency on PrepareStringBuffers
        ZipCompatibility::ConvertStringToBuffer(m_fileName.GetString(), buffer, m_stringSettings.m_uNameCodePage);
        int size = 2 + 4 + buffer.GetSize();
        if (bWriteCommentCp)
          size += 4;
        pExtra->m_data.Allocate(size);
        data = (char*)pExtra->m_data;
        CBytesWriter::WriteBytes(data + offset, (DWORD)m_stringSettings.m_uNameCodePage);
        offset += 4;
        memcpy(data + offset, buffer, buffer.GetSize());
        offset += buffer.GetSize();
        flag = 3;
      }
      else if (!m_stringSettings.IsStandardNameCodePage(GetSystemCompatibility()))
      {
        int size = 2 + 4;
        if (bWriteCommentCp)
          size += 4;
        pExtra->m_data.Allocate(size);
        data = (char*)pExtra->m_data;
        CBytesWriter::WriteBytes(data + offset, (DWORD)m_stringSettings.m_uNameCodePage);
        offset += 4;
        flag = 1;
      }

      if (bWriteCommentCp)
      {
        if (!pExtra->m_data.IsAllocated())
        {
          pExtra->m_data.Allocate(2 + 4);
          data = (char*)pExtra->m_data;
        }
        ASSERT(data);
        CBytesWriter::WriteBytes(data + offset, (DWORD)m_stringSettings.m_uCommentCodePage);
        flag |= 4;
      }
      if (data != NULL) // just in case
      {
        data[0] = ZIP_EXTRA_ZARCH_NAME_VER;
        data[1] = flag;
      }
    }
  }
#endif

  WORD uFileNameSize = (WORD)m_fileName.GetBufferSize(), uCommentSize = m_comment.GetBufferSize(),
    uExtraFieldSize = (WORD)m_aCentralExtraData.GetTotalSize();
  DWORD uSize = FILEHEADERSIZE + uFileNameSize + uCommentSize + uExtraFieldSize;
  CZipAutoBuffer buf(uSize);
  char* dest = (char*)buf;
  memcpy(dest, m_gszSignature, 4);
  WORD uVersionMadeBy = (int)m_uVersionMadeBy & 0xFF;
  uVersionMadeBy |= (WORD)(m_iSystemCompatibility << 8);
  CBytesWriter::WriteBytes(dest + 4,  uVersionMadeBy);
  CBytesWriter::WriteBytes(dest + 6,  m_uVersionNeeded);
  CBytesWriter::WriteBytes(dest + 8,  m_uFlag);
  CBytesWriter::WriteBytes(dest + 10, uMethod);
  CBytesWriter::WriteBytes(dest + 12, m_uModTime);
  CBytesWriter::WriteBytes(dest + 14, m_uModDate);
  WriteCrc32(dest + 16);
  CBytesWriter::WriteBytes(dest + 20, CBytesWriter::WriteSafeU32(m_uComprSize));
  CBytesWriter::WriteBytes(dest + 24, CBytesWriter::WriteSafeU32(m_uUncomprSize));
  CBytesWriter::WriteBytes(dest + 28, uFileNameSize);
  CBytesWriter::WriteBytes(dest + 30, uExtraFieldSize);
  CBytesWriter::WriteBytes(dest + 32, uCommentSize);
  CBytesWriter::WriteBytes(dest + 34, CBytesWriter::WriteSafeU16(m_uVolumeStart));
  CBytesWriter::WriteBytes(dest + 36, m_uInternalAttr);
  CBytesWriter::WriteBytes(dest + 38, m_uExternalAttr);
  CBytesWriter::WriteBytes(dest + 42, CBytesWriter::WriteSafeU32(m_uOffset));

  memcpy(dest + 46, m_fileName.m_buffer, uFileNameSize);

  if (uExtraFieldSize)
    m_aCentralExtraData.Write(dest + 46 + uFileNameSize);

  if (uCommentSize)
    memcpy(dest + 46 + uFileNameSize + uExtraFieldSize, m_comment.m_buffer, uCommentSize);
  
  pStorage->Write(dest, uSize, true);

  // remove to avoid miscalculations in GetSize()
  m_aCentralExtraData.RemoveInternalHeaders();	
  ClearFileName();
  return uSize;
}

bool CZipFileHeader::ReadLocal(CZipCentralDir* pCentralDir)
{	
  CZipStorage* pStorage = pCentralDir->GetStorage();
  pStorage->ChangeVolume(m_uVolumeStart);
  bool isBinary = pStorage->IsBinarySplit();
  if (isBinary)
    pStorage->SeekInBinary(m_uOffset, true);
  else
    pStorage->Seek(m_uOffset);

  char buf[LOCALFILEHEADERSIZE];
  
  pStorage->Read(buf, LOCALFILEHEADERSIZE, true);
  if (memcmp(buf, m_gszLocalSignature, 4) != 0)
    return false;
  
  bool bIsDataDescr = (((WORD)*(buf + 6)) & 8) != 0;
  
  WORD uTemp; 
  CBytesWriter::ReadBytes(uTemp, buf + 6);
  // do not compare the whole flag - the bits reserved by PKWARE may differ 
  // in local and central headers
  if (pCentralDir->IsConsistencyCheckOn(CZipArchive::checkLocalFlag)
    && (uTemp & 0xf) != (m_uFlag & 0xf))
    return false;
  
  // method
  WORD uMethod;
  CBytesWriter::ReadBytes(uMethod, buf + 8);

  // this may be different in the local header (it may contain disk name for example)
  CBytesWriter::ReadBytes(m_uLocalFileNameSize, buf + 26); 
  WORD uExtraFieldSize;
  CBytesWriter::ReadBytes(uExtraFieldSize, buf + 28);
  ZIP_VOLUME_TYPE uCurDsk = pStorage->GetCurrentVolume();
  // skip reading the local file name
  
  if (isBinary)
    pStorage->SeekInBinary(m_uLocalFileNameSize);
  else
    pStorage->m_pFile->Seek(m_uLocalFileNameSize, CZipAbstractFile::current);

  m_uLocalHeaderSize = LOCALFILEHEADERSIZE + m_uLocalFileNameSize + uExtraFieldSize;
  if (!m_aLocalExtraData.Read(pStorage, uExtraFieldSize))
    return false;

#ifdef _ZIP_ZIP64
  bool isZip64Extra = false;
  //if (m_uVersionNeeded >= ZIP_ZIP64_ARCHIVE_VERSION) // some programs do not write the correct version
  {
    CZipExtraData* pExtra = m_aLocalExtraData.Lookup(ZIP_EXTRA_PKZIP);
    if (pExtra != NULL)	
    {		
      if (pExtra->m_data.GetSize() < 16)
        return false;
      CBytesWriter::ReadBytes(m_uLocalUncomprSize, pExtra->m_data, 8);
      CBytesWriter::ReadBytes(m_uLocalComprSize, pExtra->m_data + 8, 8);
      // do not remove: it is always kept
      //m_aLocalExtraData.RemoveAt(index);
      isZip64Extra = true;
    }
  }
  
  if (!isZip64Extra)
  {
#endif
    CBytesWriter::ReadBytes(m_uLocalComprSize, buf + 18, 4);
    CBytesWriter::ReadBytes(m_uLocalUncomprSize, buf + 22, 4);
#ifdef _ZIP_ZIP64
  }
#endif

#ifdef _ZIP_UNICODE
  m_aLocalExtraData.Remove(ZIP_EXTRA_UNICODE_PATH);
  // just in case
  m_aLocalExtraData.Remove(ZIP_EXTRA_UNICODE_COMMENT);
#endif

#ifdef _ZIP_AES
  if (uMethod == CZipCompressor::methodWinZipAes && IsEncrypted())
  {
    int index = 0;
    CZipExtraData* pExtra = m_aLocalExtraData.Lookup(ZIP_EXTRA_WINZIP_AES, index);
    if (pExtra != NULL)
    {
      WORD uVersion;
      BYTE uEncryptionMethod;
      bool ok = ReadWinZipAesExtra(pExtra->m_data, uVersion, uEncryptionMethod, uMethod)
        && (uVersion == 0x0002) == m_bIgnoreCrc32 && uEncryptionMethod == m_uEncryptionMethod;				
      m_aLocalExtraData.RemoveAt(index);
      if (!ok)
        return false;
    }
  }
#else
  if (uMethod == CZipCompressor::methodWinZipAes && IsEncrypted())
    CZipException::Throw(CZipException::noAES);
#endif

  if (pCentralDir->IsConsistencyCheckOn(CZipArchive::checkLocalMethod)
    && uMethod != m_uMethod )
    return false;

  if (!bIsDataDescr && pCentralDir->IsConsistencyCheckOn(CZipArchive::checkLocalCRC | CZipArchive::checkLocalSizes))
  {
    // read all at once - probably faster than checking and reading separately
    DWORD uCrc32;
    CBytesWriter::ReadBytes(uCrc32, buf + 14);
    if (pCentralDir->IsConsistencyCheckOn(CZipArchive::checkLocalCRC)
      && uCrc32 != m_uCrc32)
        return false;

    if (pCentralDir->IsConsistencyCheckOn(CZipArchive::checkLocalSizes)
      // do not check, if local compressed size is 0 - this usually means, that some archiver 
      // could not update the compressed size after compression
      && ( (m_uLocalComprSize != 0 && m_uLocalComprSize != m_uComprSize) || m_uLocalUncomprSize != m_uUncomprSize))
        return false;
  }	
  return pStorage->GetCurrentVolume() == uCurDsk || isBinary; // check that the whole header is in one volume
}

void CZipFileHeader::ConvertFileName(CZipAutoBuffer& buffer) const
{	
  if (!m_fileName.HasString())
    return;
  CZipString temp = m_fileName.GetString();
  ZipCompatibility::SlashBackslashChg(temp, false);
  UINT codePage;
#ifdef _ZIP_UNICODE_CUSTOM
  if (m_state.IsSetAny(sfCustomUnicode))
  {
    if (m_stringSettings.m_bStoreNameInExtraData)
    {
      codePage = GetDefaultFileNameCodePage();
      
    }
    else
    {
      codePage = m_stringSettings.m_uNameCodePage;
    }
  }
  else
#endif
#ifdef _ZIP_UNICODE
  if (m_state.IsSetAny(sfStringsUnicode))
  {
    codePage = ZIP_EXTRA_UNICODE_CP;
  }
  else
#endif
  {
    codePage = GetDefaultFileNameCodePage();
  }

  ZipCompatibility::ConvertStringToBuffer(temp, buffer, codePage);
}

void CZipFileHeader::ConvertFileName(CZipString& szFileName) const
{	
  if (!m_fileName.HasBuffer())
    return;
  UINT codePage;
#ifdef _ZIP_UNICODE_CUSTOM
  if (m_state.IsSetAny(sfCustomUnicode))
  {
    codePage = m_stringSettings.m_uNameCodePage;
  }
  else
#endif
#ifdef _ZIP_UNICODE
  if (m_state.IsSetAny(sfStringsUnicode))
  {
    codePage = ZIP_EXTRA_UNICODE_CP;
  }
  else
#endif
  {
    codePage = GetDefaultFileNameCodePage();
  }
  ZipCompatibility::ConvertBufferToString(szFileName, m_fileName.m_buffer, codePage);
  // some archives may have an invalid path separator stored
  ZipCompatibility::NormalizePathSeparators(szFileName);
}

void CZipFileHeader::ConvertComment(CZipAutoBuffer& buffer) const
{	
  if (!m_comment.HasString())
    return;
  UINT codePage;
#ifdef _ZIP_UNICODE_CUSTOM
  if (m_state.IsSetAny(sfCustomUnicode))
  {
    codePage = m_stringSettings.m_uCommentCodePage;
  }
  else
#endif
#ifdef _ZIP_UNICODE
  if (m_state.IsSetAny(sfStringsUnicode))
  {
    codePage = ZIP_EXTRA_UNICODE_CP;
  }
  else
#endif
  {
    codePage = GetDefaultCommentCodePage();
  }

  ZipCompatibility::ConvertStringToBuffer(m_comment.GetString(), buffer, codePage);
}

void CZipFileHeader::ConvertComment(CZipString& szComment) const
{	
  if (!m_comment.HasBuffer())
    return;
  UINT codePage;
#ifdef _ZIP_UNICODE_CUSTOM
  if (m_state.IsSetAny(sfCustomUnicode))
  {
    codePage = m_stringSettings.m_uCommentCodePage;
  }
  else
#endif
#ifdef _ZIP_UNICODE
  if (m_state.IsSetAny(sfStringsUnicode))
  {
    codePage = ZIP_EXTRA_UNICODE_CP;
  }
  else
#endif
  {
    codePage = GetDefaultCommentCodePage();
  }
  ZipCompatibility::ConvertBufferToString(szComment, m_comment.m_buffer, codePage);
}

// write the local header
void CZipFileHeader::WriteLocal(CZipStorage *pStorage)
{	
  m_aLocalExtraData.RemoveInternalLocalHeaders();
  if (IsDataDescriptor())
  {
    m_uLocalComprSize = 0;
    // write, if we know it - WinZip 9.0 in segmented mode with AES encryption will 
    // complain otherwise (this seems like a bug, because the data descriptor is present and
    // local descriptor should be discarded)
    if (!IsWinZipAesEncryption())
      m_uLocalUncomprSize = 0;
  }
  else
  {
#ifdef _ZIP_ZIP64
    // if we know the sizes now, we can act appropriately	
    if (m_aLocalExtraData.HasHeader(ZIP_EXTRA_PKZIP))
    {		
      if (m_uLocalComprSize >= UINT_MAX)
        m_uLocalComprSize = UINT_MAX;
      if (m_uLocalUncomprSize >= UINT_MAX)
        m_uLocalUncomprSize = UINT_MAX;			
    }
#endif
  }

  WORD uMethod = m_uMethod;
#ifdef _ZIP_AES
  if (IsWinZipAesEncryption())
  {
    WriteWinZipAesExtra(true);
    uMethod = CZipCompressor::methodWinZipAes;
  }
#endif

  PrepareStringBuffers();
#ifdef _ZIP_UNICODE
  WriteUnicodeExtra(true, true);
  if (m_state.IsSetAny(sfStringsUnicode))
    m_uFlag |= FLAG_EFS;
  else
    m_uFlag &= ~FLAG_EFS;
#endif	
  // this check was already performed, if a file was replaced
  if (!CheckLengths(true))
    m_pCentralDir->ThrowError(CZipException::tooLongData);
  m_uLocalFileNameSize = (WORD)m_fileName.GetBufferSize();
  DWORD uExtraFieldSize = m_aLocalExtraData.GetTotalSize();
  m_uLocalHeaderSize = LOCALFILEHEADERSIZE + uExtraFieldSize + m_uLocalFileNameSize;
  CZipAutoBuffer buf(m_uLocalHeaderSize);
  char* dest = (char*) buf;
  memcpy(dest, m_gszLocalSignature, 4);

  CBytesWriter::WriteBytes(dest + 4,  m_uVersionNeeded);
  CBytesWriter::WriteBytes(dest + 6,  m_uFlag);
  CBytesWriter::WriteBytes(dest + 8,  uMethod);
  CBytesWriter::WriteBytes(dest + 10, m_uModTime);
  CBytesWriter::WriteBytes(dest + 12, m_uModDate);
  WriteSmallDataDescriptor(dest + 14);
  CBytesWriter::WriteBytes(dest + 26, m_uLocalFileNameSize);
  CBytesWriter::WriteBytes(dest + 28, (WORD)uExtraFieldSize);
  memcpy(dest + 30, m_fileName.m_buffer, m_uLocalFileNameSize);

  if (uExtraFieldSize)
    m_aLocalExtraData.Write(dest + 30 + m_uLocalFileNameSize);
  
  // possible volume change before writing to the file in the segmented archive
  // so write the local header first 
  pStorage->Write(dest, m_uLocalHeaderSize, true);

  m_uVolumeStart = pStorage->IsBinarySplit() ? 0 : pStorage->GetCurrentVolume();	
  m_uOffset = pStorage->GetPosition() - m_uLocalHeaderSize;
  
  m_aLocalExtraData.RemoveInternalLocalHeaders();
  ClearFileName();
}

#ifdef _ZIP_UNICODE

DWORD CZipFileHeader::PredictUnicodeExtra(bool fileName) const
{	
  const StringWithBuffer* data;
  if (fileName)
  {
    if (!m_state.IsSetAny(sfFileNameExtra))
      return 0;
    data = &m_fileName;		
  }
  else
  {
    if (!m_state.IsSetAny(sfCommentExtra))
      return 0;
    data = &m_comment;		
  }

  DWORD ret;

  if (m_state.IsSetAny(sfStringsUnicode) && data->HasBuffer())
    ret = data->GetBufferSize();
  else
  {
    CZipAutoBuffer buffer;
    CZipString temp;
    if (data->HasString())
      temp = data->GetString();
    else if (fileName)
      ConvertFileName(temp);
    else
      ConvertComment(temp);
    ZipCompatibility::ConvertStringToBuffer(temp, buffer, ZIP_EXTRA_UNICODE_CP);
    ret = buffer.GetSize();
  }
  return ret + 1 + 4 + 4; // version, crc, headerID + size
}

void CZipFileHeader::WriteUnicodeExtra(bool local, bool fileName)
{
  CZipExtraField* field = local ? &m_aLocalExtraData : &m_aCentralExtraData;
  CZipExtraData* pExtra;
  CZipAutoBuffer buffer;
  CZipAutoBuffer* original;
  if (fileName)
  {
    if (!m_state.IsSetAny(sfFileNameExtra))
      return;
    pExtra = field->CreateNew(ZIP_EXTRA_UNICODE_PATH);
    original = &m_fileName.m_buffer;
    if (m_state.IsSetAny(sfStringsUnicode) && m_fileName.HasBuffer())
    {
      // the string is stored with the same code page
      buffer = m_fileName.m_buffer;
    }
    else
    {
      GetFileName(false);
      CZipString temp = m_fileName.GetString();
      ZipCompatibility::SlashBackslashChg(temp, false);
      ZipCompatibility::ConvertStringToBuffer(temp, buffer, ZIP_EXTRA_UNICODE_CP);
    }
  }
  else
  {
    if (!m_state.IsSetAny(sfCommentExtra))
      return;
    pExtra = field->CreateNew(ZIP_EXTRA_UNICODE_COMMENT);
    original = &m_comment.m_buffer;
    if (m_state.IsSetAny(sfStringsUnicode) && m_comment.HasBuffer())
    {
      // the string is stored with the same code page
      buffer = m_comment.m_buffer;
    }
    else
    {
      GetComment(false);
      ZipCompatibility::ConvertStringToBuffer(m_comment.GetString(), buffer, ZIP_EXTRA_UNICODE_CP);
    }		
  }
  DWORD checksum = (DWORD)zarch_crc32(0, (zarch_Bytef*)(char*)*original, original->GetSize());
  pExtra->m_data.Allocate(buffer.GetSize() + 1 + 4);
  char* data = pExtra->m_data;
  data[0] = ZIP_EXTRA_UNICODE_VERSION;	
  data++;
  CBytesWriter::WriteBytes(data, checksum);
  data +=4;
  memcpy(data, buffer, buffer.GetSize());
}
#endif

WORD CZipFileHeader::GetDataDescriptorSize(bool bConsiderSignature) const
{
  if (IsDataDescriptor())
  {
    
#ifdef _ZIP_ZIP64			
    WORD size = (WORD)(m_uComprSize >= UINT_MAX || m_uUncomprSize >= UINT_MAX ? 20 : 12);
#else
    WORD size = 12;
#endif
    return (WORD)(bConsiderSignature ? size + 4 : size);
  }
  else
    return 0;
}	

bool CZipFileHeader::NeedsDataDescriptor() const
{
#ifdef _ZIP_ZIP64
  // we could not write local extra zip64 header and that's why we use the data descriptor (local sizes are 0)
  return m_uEncryptionMethod == CZipCryptograph::encStandard || (m_uComprSize >= UINT_MAX && m_uLocalComprSize == 0) || (m_uUncomprSize >= UINT_MAX && m_uLocalUncomprSize == 0);
#else
  return m_uEncryptionMethod == CZipCryptograph::encStandard;
#endif
}

void CZipFileHeader::PrepareData(int iLevel, bool bSegm)
{
  // could be == 1, but the way below it works for PredictMaximumFileSizeInArchive when used on an existing segmented archive
  m_uInternalAttr = 0;

  // version made by
#ifdef _ZIP_BZIP2
  m_uVersionMadeBy = (unsigned char)ZIP_BZIP2_ARCHIVE_VERSION; 
#else

#ifdef _ZIP_ZIP64
  m_uVersionMadeBy = (unsigned char)ZIP_ZIP64_ARCHIVE_VERSION;
#else
  m_uVersionMadeBy = (unsigned char)0x14;
#endif

#endif

  m_uCrc32 = 0;
  m_uComprSize = 0;
  m_uUncomprSize = 0;

  m_uFlag  = 0;
  if (m_uMethod == CZipCompressor::methodDeflate)
  {
    switch (iLevel)
    {
    case 1:
      m_uFlag  |= 6;
      break;
    case 2:
      m_uFlag  |= 4;
      break;
    case 8:
    case 9:
      m_uFlag  |= 2;
      break;
    }
  }

  UpdateFlag(bSegm);

  AdjustLocalComprSize();

  m_uVersionNeeded = 0;
#ifdef _ZIP_BZIP2
  if (m_uMethod == CZipCompressor::methodBzip2)
    m_uVersionNeeded = ZIP_BZIP2_ARCHIVE_VERSION;
#endif
#ifdef _ZIP_ZIP64
  UpdateLocalZip64(false);
#endif
  if (m_uVersionNeeded == 0)
    m_uVersionNeeded = IsDirectory() ? 0xa : 0x14; // 1.0 or 2.0		
}

#ifdef _ZIP_ZIP64
void CZipFileHeader::UpdateLocalZip64(bool bAdjustLocalComprSize)
{	
  m_aLocalExtraData.Remove(ZIP_EXTRA_PKZIP);
  if (m_uLocalComprSize >= UINT_MAX || m_uLocalUncomprSize >= UINT_MAX)
  {		
    CZipExtraData* pExtra = m_aLocalExtraData.CreateNew(ZIP_EXTRA_PKZIP);
    pExtra->m_data.Allocate(16);
    
    CBytesWriter::WriteBytes(pExtra->m_data, m_uLocalUncomprSize, 8);
    ZIP_SIZE_TYPE uLocalComprSize = m_uLocalComprSize;
    if (bAdjustLocalComprSize)
      AdjustLocalComprSize(uLocalComprSize);
    CBytesWriter::WriteBytes((char*)(pExtra->m_data + 8), uLocalComprSize, 8);
    // Zip64 version is lower than BZip2, so do not overwrite that value
    if (m_uVersionNeeded < ZIP_ZIP64_ARCHIVE_VERSION) 
      m_uVersionNeeded = ZIP_ZIP64_ARCHIVE_VERSION;
  }
}
#endif

void CZipFileHeader::GetCrcAndSizes(char * pBuffer)const
{
  WriteCrc32(pBuffer);
  CBytesWriter::WriteBytes(pBuffer + 4, m_uComprSize, 4);
  CBytesWriter::WriteBytes(pBuffer + 8, m_uUncomprSize, 4);
}

bool CZipFileHeader::CheckDataDescriptor(CZipStorage* pStorage) const
{
  if (!IsDataDescriptor())
    return true;

#ifdef _ZIP_ZIP64
  const int sizeOfSize = m_uComprSize >= UINT_MAX || m_uUncomprSize >= UINT_MAX ? 8 : 4;
#else
  const int sizeOfSize = 4;
#endif

  const int size = 4 + 2 * sizeOfSize; // crc and two sizes

  CZipAutoBuffer buf(size + 4);
  pStorage->Read(buf, size, false);
  char* pBuf;

  // when an archive is segmented, files that are divided between volume have bit 3 of flag set
  // which tell about the presence of the data descriptor after the compressed data
  // This signature may be in a segmented archive that is one volume only
  // (it is detected as a not segmented archive)
  if (memcmp(buf, CZipStorage::m_gszExtHeaderSignat, 4) == 0) // there is a signature
  {
    pStorage->Read((char*)buf + size, 4, false);
    pBuf = (char*)buf + 4;
  }
  else 
    pBuf = buf;

  DWORD uCrc32 = 0;
  ZIP_SIZE_TYPE uCompressed = 0, uUncompressed = 0;

  CBytesWriter::ReadBytes(uCrc32,			pBuf);
  CBytesWriter::ReadBytes(uCompressed,	pBuf + 4, sizeOfSize);
  CBytesWriter::ReadBytes(uUncompressed,	pBuf + 4 + sizeOfSize, sizeOfSize);
  return uCrc32 == m_uCrc32 && uCompressed == m_uComprSize && uUncompressed == m_uUncomprSize;
}

DWORD CZipFileHeader::GetSize()const
{	
  DWORD uSize = FILEHEADERSIZE + PredictFileNameSize() + PredictCommentSize();
  uSize += m_aCentralExtraData.GetTotalSize();
#ifdef _ZIP_UNICODE
  uSize += PredictUnicodeExtra(true) + PredictUnicodeExtra(false);
#endif
#ifdef _ZIP_ZIP64		
  if (NeedsZip64())
  {
    // add extra data length - the zip64 extra data for the central header is always removed
    uSize += 4; // headerID and size
    if (m_uUncomprSize >= UINT_MAX)
      uSize += 8;
    if (m_uComprSize >= UINT_MAX)
      uSize += 8;
    if (m_uOffset >= UINT_MAX)
      uSize += 8;
    if (m_uVolumeStart >= USHRT_MAX)
      uSize += 4;
  }
#endif
#ifdef _ZIP_AES
  if (IsWinZipAesEncryption())
    uSize += ZIP_EXTRA_WINZIP_DATA_SIZE + 4;
#endif

#ifdef _ZIP_UNICODE_CUSTOM
  if (m_state.IsSetAny(sfCustomUnicode))
  {
    if (m_stringSettings.m_bStoreNameInExtraData)
    {
      CZipString temp;
      if (m_fileName.HasString())
        temp = m_fileName.GetString();
      else
        ConvertFileName(temp);			
      if (temp.GetLength() > 0)
      {
        uSize += 4 + 2 + 4; // headerID, size + version, flag + filename code page
        CZipAutoBuffer buffer;				
        ZipCompatibility::ConvertStringToBuffer(temp, buffer, m_stringSettings.m_uNameCodePage);
        uSize += buffer.GetSize();
        if (!m_stringSettings.IsStandardCommentCodePage(GetSystemCompatibility()))
          uSize += 4;
      }
    }
  }
#endif
  return uSize;
}

DWORD CZipFileHeader::GetLocalSize(bool bReal)const
{
  if (bReal)
    return m_uLocalHeaderSize;

  DWORD uSize = LOCALFILEHEADERSIZE + m_aLocalExtraData.GetTotalSize() + PredictFileNameSize();
#ifdef _ZIP_UNICODE
  uSize += PredictUnicodeExtra(true);
#endif
#ifdef _ZIP_AES
  // we add it here, because it is removed after reading in local header
  if (IsWinZipAesEncryption())
    uSize += ZIP_EXTRA_WINZIP_DATA_SIZE + 4;
#endif
  return uSize;
}

bool CZipFileHeader::SetComment(LPCTSTR lpszComment)
{
  if (m_pCentralDir)
  {
    // update the lpszFileName to make sure the renaming is necessary
    GetComment();
    CZipString newComment(lpszComment);
    if (!UpdateCommentFlags(&newComment))
    {			
      if (m_comment.GetString().Collate(newComment) == 0)
        return true;
    }
    // just in case
    m_comment.ClearBuffer();

    bool ret;
    CZipString oldComment= m_comment.GetString();
    m_comment.SetString(lpszComment);
    ret = m_pCentralDir->OnFileCentralChange();
    if (!ret)
      m_comment.SetString(oldComment);

    return ret;
  }
  else
  {
    m_comment.ClearBuffer();
    m_comment.SetString(lpszComment);
    return true;
  }
}

const CZipString& CZipFileHeader::GetComment(bool bClearBuffer)
{
  if (m_comment.HasString())
  {
    return m_comment.GetString();
  }
  m_comment.AllocateString();
  ConvertComment(m_comment.GetString());
  if (bClearBuffer)
    m_comment.ClearBuffer();
  return m_comment.GetString();
}

int CZipFileHeader::GetCompressionLevel() const
{
  if (m_uMethod == CZipCompressor::methodStore)
    return CZipCompressor::levelStore;
  else if ((m_uFlag & (WORD) 6) != 0)
    return 1;
  else if ((m_uFlag & (WORD) 4) != 0)
    return 2;
  else if ((m_uFlag & (WORD) 2) != 0)
    return CZipCompressor::levelBest;
  else
    return CZipCompressor::levelDefault;
}

bool CZipFileHeader::SetFileName(LPCTSTR lpszFileName, bool bInCentralOnly)
{
  CZipString newFileName(lpszFileName);
  if (!IsDirectory() || newFileName.GetLength() != 1 || !CZipPathComponent::IsSeparator(newFileName[0]))
    // do not remove from directories where only path separator is present
    CZipPathComponent::RemoveSeparatorsLeft(newFileName);
  if (m_pCentralDir)
  {
    // update the lpszFileName to make sure the renaming is necessary
    GetFileName();

    if (!UpdateFileNameFlags(&newFileName, true))
    {						
      if (IsDirectory())
        CZipPathComponent::AppendSeparator(newFileName);
      else
        CZipPathComponent::RemoveSeparators(newFileName);

      if (m_fileName.GetString().Collate(newFileName) == 0)
        return true;
    }
    // just in case
    m_fileName.ClearBuffer();

    bool ret;
    CZipString oldFileName = m_fileName.GetString();
    m_fileName.SetString(lpszFileName);
    ret = m_pCentralDir->OnFileNameChange(this, bInCentralOnly);
    if (!bInCentralOnly)
    {
      if (ret)
        SetModified();
      else
        m_fileName.SetString(oldFileName);
    }

    return ret;
  }
  else
  {
    m_fileName.ClearBuffer();		
    m_fileName.SetString(newFileName);
    return true;
  }
}

const CZipString& CZipFileHeader::GetFileName(bool bClearBuffer)
{
  if (m_fileName.HasString())
  {
    return m_fileName.GetString();
  }
  m_fileName.AllocateString();
  ConvertFileName(m_fileName.GetString());
  // don't keep it in memory
  if (bClearBuffer)
  {
    m_fileName.ClearBuffer();
  }
  return m_fileName.GetString();
}

bool CZipFileHeader::IsDirectory()
{
  return ZipPlatform::IsDirectory(GetSystemAttr());
}

DWORD CZipFileHeader::GetSystemAttr()
{
  if (ZipCompatibility::IsPlatformSupported(GetSystemCompatibility()))
  {		
    DWORD uAttr = GetSystemCompatibility() == ZipCompatibility::zcUnix ? (m_uExternalAttr >> 16) : (m_uExternalAttr & 0xFFFF);
    DWORD uConvertedAttr = ZipCompatibility::ConvertToSystem(uAttr, GetSystemCompatibility(), ZipPlatform::GetSystemID());
    if (m_uComprSize == 0 && !ZipPlatform::IsDirectory(uConvertedAttr) && CZipPathComponent::HasEndingSeparator(GetFileName()))			
      // can happen, a folder can have attributes set and no dir attribute (Python modules)
      // TODO: [postponed] fix and cache after reading from central dir, but avoid calling GetFileName() there to keep lazy name conversion
      return ZipPlatform::GetDefaultDirAttributes() | uConvertedAttr; 
    else
    {						
#ifdef _ZIP_SYSTEM_LINUX
      // converting from Windows attributes may create a not readable linux directory
      if (GetSystemCompatibility() != ZipCompatibility::zcUnix && ZipPlatform::IsDirectory(uConvertedAttr))
        return ZipPlatform::GetDefaultDirAttributes();
#endif
      return uConvertedAttr;
    }
  }
  else
    return CZipPathComponent::HasEndingSeparator(GetFileName()) ? ZipPlatform::GetDefaultDirAttributes() : ZipPlatform::GetDefaultAttributes();
}

bool CZipFileHeader::SetSystemAttr(DWORD uAttr)
{
  // The high-word should no be set in attributes, 
  // It will be overwritten by Unix attributes, which are stored in high-word.
  ASSERT((uAttr & 0xFFFF0000) == 0);
  // make it readable under Unix as well, since it stores its attributes in HIWORD(uAttr)
  DWORD uNewAtrr = ZipCompatibility::ConvertToSystem(uAttr, ZipPlatform::GetSystemID(), GetSystemCompatibility());
  if (GetSystemCompatibility() == ZipCompatibility::zcUnix)
  {		
    uNewAtrr <<= 16;
    if (ZipPlatform::IsDirectory(uAttr))
      uNewAtrr |= 0x10; // make it recognizable under other systems (all use 0x10 for directory)
  }
  else
    // make it readable under linux
    uNewAtrr |= (ZipCompatibility::ConvertToSystem(uAttr, ZipPlatform::GetSystemID(), ZipCompatibility::zcUnix) << 16);		

  if (uNewAtrr != m_uExternalAttr)
  {
    if (m_pCentralDir)
    {
      if (!m_pCentralDir->OnFileCentralChange())
      {
        return false;
      }
    }
    m_uExternalAttr = uNewAtrr;
  }
  return true;
}

CZipFileHeader& CZipFileHeader::operator=(const CZipFileHeader& header)
{
  m_uVersionMadeBy = header.m_uVersionMadeBy;	
  m_uVersionNeeded = header.m_uVersionNeeded;
  m_iSystemCompatibility = header.m_iSystemCompatibility;
  m_uFlag = header.m_uFlag;
  m_uMethod = header.m_uMethod;
  m_uModTime = header.m_uModTime;
  m_uModDate = header.m_uModDate;
  m_tModificationTime = header.m_tModificationTime;
  m_tCreationTime = header.m_tCreationTime;
  m_tLastAccessTime = header.m_tLastAccessTime;
  m_uCrc32 = header.m_uCrc32;
  m_uComprSize = header.m_uComprSize;
  m_uUncomprSize = header.m_uUncomprSize;
  m_uVolumeStart = header.m_uVolumeStart;
  m_uInternalAttr = header.m_uInternalAttr;
  m_uLocalComprSize = header.m_uLocalComprSize;
  m_uLocalHeaderSize = header.m_uLocalHeaderSize;
  m_uLocalUncomprSize = header.m_uLocalUncomprSize;
  m_uExternalAttr = header.m_uExternalAttr;	
  m_uLocalFileNameSize = header.m_uLocalFileNameSize;;
  m_uOffset = header.m_uOffset;
  m_aLocalExtraData = header.m_aLocalExtraData;
  m_aCentralExtraData = header.m_aCentralExtraData;
  m_uEncryptionMethod = header.m_uEncryptionMethod;
  m_fileName = header.m_fileName;
  m_comment = header.m_comment;
  m_state = header.m_state;

#ifdef _ZIP_UNICODE_CUSTOM
  m_stringSettings = header.m_stringSettings;
#endif
  m_pCentralDir = header.m_pCentralDir;
  
  return *this;
}

void CZipFileHeader::WriteSmallDataDescriptor(char* pDest, bool bLocal)
{
  WriteCrc32(pDest);
  if (bLocal)
  {
    CBytesWriter::WriteBytes(pDest + 4, m_uLocalComprSize, 4);
    CBytesWriter::WriteBytes(pDest + 8, m_uLocalUncomprSize, 4);
  }
  else
  {
    CBytesWriter::WriteBytes(pDest + 4, m_uComprSize, 4);
    CBytesWriter::WriteBytes(pDest + 8, m_uUncomprSize, 4);
  }
}


void CZipFileHeader::WriteDataDescriptor(CZipStorage* pStorage)
{
  if (!IsDataDescriptor())
    return;
  bool signature = NeedsSignatureInDataDescriptor(pStorage);
  CZipAutoBuffer buf;
  buf.Allocate(GetDataDescriptorSize(signature));
  char* pBuf;
  if (signature)
  {
    memcpy(buf, CZipStorage::m_gszExtHeaderSignat, 4);
    pBuf = (char*)buf + 4;
  }
  else
    pBuf = buf;
  WriteCrc32(pBuf);
#ifdef _ZIP_ZIP64
  if (m_uComprSize >= UINT_MAX || m_uUncomprSize >= UINT_MAX)
  {
    CBytesWriter::WriteBytes(pBuf + 4, m_uComprSize, 8);
    CBytesWriter::WriteBytes(pBuf + 12, m_uUncomprSize, 8);
  }
  else
  {
#endif
    CBytesWriter::WriteBytes(pBuf + 4, m_uComprSize, 4);
    CBytesWriter::WriteBytes(pBuf + 8, m_uUncomprSize, 4);
#ifdef _ZIP_ZIP64
  }
#endif
  pStorage->Write(buf, buf.GetSize(), true);
}

void CZipFileHeader::UpdateLocalHeader(CZipStorage* pStorage)
{
  if (pStorage->IsSegmented() || IsDataDescriptor())
    // there is nothing to fix 
    return;
  pStorage->Flush();
  ZIP_FILE_USIZE uPos = pStorage->m_pFile->GetPosition();	
#ifdef _ZIP_ZIP64		
  bool bUpdateLocal = false;
  if (!m_aLocalExtraData.HasHeader(ZIP_EXTRA_PKZIP))
  {
    // check if badly predicted zip64 version
    if ((m_uComprSize >= UINT_MAX && m_uLocalComprSize < UINT_MAX) || (m_uUncomprSize >= UINT_MAX && m_uLocalUncomprSize < UINT_MAX))
    {
      // we have no place to an put extra header, so we will use a data descriptor
      CZipAutoBuffer buf(12);			
      if (m_uVersionNeeded < ZIP_ZIP64_ARCHIVE_VERSION)
        m_uVersionNeeded = ZIP_ZIP64_ARCHIVE_VERSION;
      m_uFlag  |= 8; // data descriptor present			
      CBytesWriter::WriteBytes(buf,  m_uVersionNeeded);
      CBytesWriter::WriteBytes((char*)buf + 2,  m_uFlag);
      pStorage->Seek(m_uOffset + 4);
      pStorage->m_pFile->Write(buf, 4);			
      int iToWrite = 4;
      WriteCrc32(buf);
      if (m_uLocalComprSize != 0 || m_uLocalUncomprSize != 0)
      {
        m_uLocalComprSize = m_uLocalUncomprSize = 0;
        CBytesWriter::WriteBytes((char*)buf + 4, m_uLocalComprSize, 4);
        CBytesWriter::WriteBytes((char*)buf + 8, m_uLocalUncomprSize, 4);
        iToWrite += 8;
      }
      pStorage->m_pFile->Seek(6, CZipAbstractFile::current);
      pStorage->m_pFile->Write(buf, iToWrite);
    }
    else
      bUpdateLocal = true;
  }
  // they would be set to UINT_MAX in PrepareData, if they were greater than that
  // and written as such in the local header, so the local header update would not 
  // be necessary in such a case
  else if (m_uLocalComprSize < UINT_MAX || m_uLocalUncomprSize < UINT_MAX)
  {
    // find the offset of the local zip64 extra header
    int offset = 4;
    ZIP_SIZE_TYPE uLocalComprSize = 0;
    for (int i = 0; i < m_aLocalExtraData.GetCount(); i++)
    {
      CZipExtraData* pExtra = m_aLocalExtraData.GetAt(i);
      if (pExtra->GetHeaderID() == ZIP_EXTRA_PKZIP)
      {
        offset += 4;
        // check what compressed size was written in the local zip64 extra field
        // the uncompressed size is in m_uLocalUncomprSize
        CBytesWriter::ReadBytes(uLocalComprSize, pExtra->m_data + 8, 8);
        // if uLocalComprSize < UINT_MAX then it should equal to m_uLocalComprSize
        break;
      }
      offset += pExtra->GetTotalSize();
    }
    
    // we could add correcting the local header if m_uLocalCompSize != m_uCompSize, but the library
    // calls PrepareData and WriteLocal in a one top-level call, so m_uLocalCompSize should equal uLocalCompSize
    if ( (m_uLocalUncomprSize < UINT_MAX && m_uUncomprSize != m_uLocalUncomprSize) || (uLocalComprSize < UINT_MAX && m_uComprSize != uLocalComprSize))
    {
      // we have extra headers, but they may hold wrong values - we mask local header values if needed and fix extra header
      m_uLocalUncomprSize = CBytesWriter::WriteSafeU32(m_uUncomprSize);
      m_uLocalComprSize = CBytesWriter::WriteSafeU32(m_uComprSize);			

      CZipAutoBuffer buf(16);			
      WriteSmallDataDescriptor(buf);
      pStorage->Seek(m_uOffset + 14);
      pStorage->m_pFile->Write(buf, 12);

      // fix the extra header
      CBytesWriter::WriteBytes(buf, m_uUncomprSize, 8);
      CBytesWriter::WriteBytes((char*)buf + 8, m_uComprSize, 8);
      pStorage->m_pFile->Seek(offset + m_uLocalFileNameSize, CZipAbstractFile::current);
      pStorage->m_pFile->Write(buf, 16);
    }
  }
  else 
    // update crc anyway
    bUpdateLocal = true;

  if (bUpdateLocal)
  {
#endif
    // update crc and sizes, the sizes may already be all right,
    // but 8 more bytes won't make a difference, we need to update crc32 anyway
    CZipAutoBuffer buf(12);		
    m_uLocalComprSize = CBytesWriter::WriteSafeU32(m_uComprSize);
    m_uLocalUncomprSize = CBytesWriter::WriteSafeU32(m_uUncomprSize);
    WriteSmallDataDescriptor(buf);
    pStorage->Seek(m_uOffset + 14);
    pStorage->m_pFile->Write(buf, 12);
#ifdef _ZIP_ZIP64
  }
#endif

  pStorage->m_pFile->SafeSeek(uPos);
}

void CZipFileHeader::WriteCrc32(char* pBuf) const
{
  DWORD uCrc = m_bIgnoreCrc32 ? 0 : m_uCrc32;
  CBytesWriter::WriteBytes(pBuf, uCrc);
}

#ifdef _ZIP_AES
void CZipFileHeader::UpdateCrc32Handling()
{
  // use AE-1 for compatibility and AE-2 for small files
  // to avoid discovering encrypted file content 
  // based on CRC32
  if (IsWinZipAesEncryption() && m_uLocalUncomprSize < 20)
    m_bIgnoreCrc32 = true;

}
#endif

void CZipFileHeader::ClearFileName()
{	
#ifdef _ZIP_UNICODE_CUSTOM	
    if (m_state.IsSetAny(sfCustomUnicode) && m_stringSettings.m_bStoreNameInExtraData)
      // we are keeping m_pszFileName, clear the buffer, we need the original, when writing extra header in central directory (can happen many times - better performance)
      // and when accessing the filename
      m_fileName.ClearBuffer();
    else
#endif
#ifdef _ZIP_UNICODE
    if (m_state.IsSetAny(sfFileNameExtra))
      m_fileName.ClearBuffer();
    else
#endif
      m_fileName.ClearString();
}

bool CZipFileHeader::UpdateFileNameFlags(const CZipString* szNewFileName, bool bAllowRemoveCDir)
{
#if defined _ZIP_UNICODE || defined _ZIP_UNICODE_CUSTOM
  CBitFlag iMode = m_pCentralDir->GetUnicodeMode();
#endif	
  // move the buffer to the name, to ensure it is converted properly now
  GetComment();
  bool centralDirChanged = false;
#ifdef _ZIP_UNICODE
  bool changed = m_state.ChangeWithCheck(sfStringsUnicode, iMode.IsSetAny(CZipArchive::umString));
  if (changed)
  {
    centralDirChanged = true;
  }
  bool isExtra = iMode.IsSetAny(CZipArchive::umExtra);
  if (isExtra)
  {
    if (!m_fileName.HasString() || IsStringAscii(szNewFileName != NULL ? *szNewFileName : m_fileName.GetString()))
      isExtra = false;

    if (!m_state.IsSetAny(sfCommentExtra) && m_comment.HasString() && !IsStringAscii(m_comment.GetString()))
    {
      m_state.Set(sfCommentExtra);
      centralDirChanged = true;
    }
  }
  else if (m_state.IsSetAny(sfCommentExtra))
  {
    m_state.Clear(sfCommentExtra);
    centralDirChanged = true;
  }

  changed |= m_state.ChangeWithCheck(sfFileNameExtra, isExtra);			
#else
  bool changed = false;
#endif
#ifdef _ZIP_UNICODE_CUSTOM	
  bool isCustom = iMode.IsSetAny(CZipArchive::umCustom);
  const CZipStringStoreSettings& stringStoreSettings = m_pCentralDir->GetStringStoreSettings();
  if (m_state.ChangeWithCheck(sfCustomUnicode, isCustom))
  {		
    // the mode changed
    if (isCustom)
    {
      // changed to custom
      m_stringSettings.m_bStoreNameInExtraData = stringStoreSettings.m_bStoreNameInExtraData;
      m_stringSettings.m_uNameCodePage = stringStoreSettings.m_uNameCodePage;
      if (!m_stringSettings.m_bStoreNameInExtraData && m_stringSettings.m_uNameCodePage != GetDefaultFileNameCodePage())
        // the local filename needs to be rewritten, the name code page has changed
        changed = true;
    }
    else
    {
      // changed from custom
      if (!m_stringSettings.m_bStoreNameInExtraData && m_stringSettings.m_uNameCodePage != GetDefaultFileNameCodePage())
        changed = true;

      m_stringSettings.m_bStoreNameInExtraData = stringStoreSettings.m_bStoreNameInExtraData;
      m_stringSettings.m_uNameCodePage = stringStoreSettings.m_uNameCodePage;
    }
    centralDirChanged = true;
  }
  else if (isCustom)
  {		
    if (stringStoreSettings.m_bStoreNameInExtraData != m_stringSettings.m_bStoreNameInExtraData)
    {
      if (m_stringSettings.m_bStoreNameInExtraData)
      {
        changed |= (stringStoreSettings.m_uNameCodePage == GetDefaultFileNameCodePage());
      }
      else
      {
        changed |= (m_stringSettings.m_uNameCodePage == GetDefaultFileNameCodePage());
      }
    }
    else if (!m_stringSettings.m_bStoreNameInExtraData)
      changed |= (stringStoreSettings.m_uNameCodePage != m_stringSettings.m_uNameCodePage);

    m_stringSettings.m_bStoreNameInExtraData = stringStoreSettings.m_bStoreNameInExtraData;
    m_stringSettings.m_uNameCodePage = stringStoreSettings.m_uNameCodePage;
  }
#endif
  if (changed || centralDirChanged)
  {
    m_comment.ClearBuffer();
  }
  // remove the central directory in case the filename did not changed, but the comment flags changed
  if (!changed && centralDirChanged && bAllowRemoveCDir && m_pCentralDir && m_comment.HasString())
    m_pCentralDir->OnFileCentralChange();
  return changed;
}

bool CZipFileHeader::UpdateCommentFlags(const CZipString* szNewComment)
{
#if defined _ZIP_UNICODE || defined _ZIP_UNICODE_CUSTOM
  CBitFlag iMode = m_pCentralDir->GetUnicodeMode();
#endif
#ifdef _ZIP_UNICODE
  bool isExtra = iMode.IsSetAny(CZipArchive::umExtra);
  if (isExtra)
  {
    if (!m_comment.HasString()
      || m_state.IsSetAny(sfStringsUnicode)
#ifdef _ZIP_UNICODE_CUSTOM		
      || m_state.IsSetAny(sfCustomUnicode)
#endif
      || IsStringAscii(szNewComment != NULL ? *szNewComment : m_comment.GetString()))			
      isExtra = false;
  }
  bool changed = m_state.ChangeWithCheck(sfCommentExtra, isExtra);
#else
  bool changed = false;
#endif
#ifdef _ZIP_UNICODE_CUSTOM		
#ifdef _ZIP_UNICODE
  if (!m_state.IsSetAny(sfStringsUnicode) && !m_state.IsSetAny(sfFileNameExtra))
  {
#endif
    bool isCustom = iMode.IsSetAny(CZipArchive::umCustom);
    changed |= m_state.ChangeWithCheck(sfCustomUnicode, isCustom);
    if (isCustom)
    {
      const CZipStringStoreSettings& stringStoreSettings = m_pCentralDir->GetStringStoreSettings();
      changed |= (m_stringSettings.m_uCommentCodePage != stringStoreSettings.m_uCommentCodePage);
      m_stringSettings.m_uCommentCodePage = stringStoreSettings.m_uCommentCodePage;
    }
#ifdef _ZIP_UNICODE
  }
#endif
#endif
  return changed;
}

////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

#ifdef _ZIP_BZIP2

/**
* \file Bzip2Compressor.h
*	Includes the ZipArchiveLib::CBzip2Compressor class.
*
*/


#if !defined(ZIPARCHIVE_BZIP2COMPRESSOR_DOT_H)
#define ZIPARCHIVE_BZIP2COMPRESSOR_DOT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "_features.h"

#include "ZipExport.h"
#include "BaseLibCompressor.h"
#include "ZipException.h"

#ifdef _ZIP_BZIP2_INTERNAL	
	#include "bzip2/bzlib.h"
#else
	#include <bzlib.h>
#endif

namespace ZipArchiveLib
{
/**
	A compressor that uses the bzip2 algorithm.

	\see
		<a href="kb">0610231446|bzip2</a>
*/
class ZIP_API CBzip2Compressor : public CBaseLibCompressor
{
public:		 
    /**
		Represents options of the CBzip2Compressor.

		\see
			<a href="kb">0610231446|options</a>
		\see
			CZipArchive::SetCompressionOptions
	*/
	struct ZIP_API COptions : CBaseLibCompressor::COptions
	{
		COptions()
		{
			m_bUseSlowDecompression = false;
		}
		int GetType() const
		{
			return typeBzip2;
		}
		CZipCompressor::COptions* Clone() const
		{
			return new COptions(*this);
		}

		/**
			\c true, if the slow decompression method should be used; \c false otherwise.
			The slow decompression method uses less memory than the normal decompression method (but is slower).
		*/
		bool m_bUseSlowDecompression;
	};

	/**
		Initializes a new instance of the CBzip2Compressor class.

		\param pStorage
			The current storage object.
	 */
	CBzip2Compressor(CZipStorage* pStorage);

	bool CanProcess(WORD uMethod) {return uMethod == methodBzip2;}

	void InitCompression(int iLevel, CZipFileHeader* pFile, CZipCryptograph* pCryptograph);
	void InitDecompression(CZipFileHeader* pFile, CZipCryptograph* pCryptograph);

	void Compress(const void *pBuffer, DWORD uSize);
	DWORD Decompress(void *pBuffer, DWORD uSize);
	
	void FinishCompression(bool bAfterException);
	void FinishDecompression(bool bAfterException);

	const CZipCompressor::COptions* GetOptions() const
	{
		return &m_options;
	}

protected:
	void UpdateOptions(const CZipCompressor::COptions* pOptions)
	{
		m_options = *(COptions*)pOptions;
	}
	int ConvertInternalError(int iErr) const
	{
		switch (iErr)
		{
		case BZ_SEQUENCE_ERROR:
			return CZipException::bzSequenceError;
		case BZ_PARAM_ERROR:
			return CZipException::bzParamError;
		case BZ_MEM_ERROR:
			return CZipException::bzMemError;
		case BZ_DATA_ERROR:
			return CZipException::bzDataError;
		case BZ_DATA_ERROR_MAGIC:
			return CZipException::bzDataErrorMagic;
		case BZ_IO_ERROR:
			return CZipException::bzIoError;
		case BZ_UNEXPECTED_EOF:
			return CZipException::bzUnexpectedEof;
		case BZ_OUTBUFF_FULL:
			return CZipException::bzOutbuffFull;
		case BZ_CONFIG_ERROR:
			return CZipException::bzConfigError;
		default:
			return CZipException::genericError;
		}
	}
	bool IsCodeErrorOK(int iErr) const
	{
		return iErr == BZ_OK || iErr == BZ_RUN_OK || iErr == BZ_FINISH_OK || iErr == BZ_FLUSH_OK;
	}	
private:
	COptions m_options;
	bz_stream m_stream;
	ZIP_SIZE_TYPE GetStreamTotalOut()
	{
#ifdef _ZIP_ZIP64
			return (ZIP_SIZE_TYPE)(((ZIP_SIZE_TYPE)m_stream.total_out_hi32 << 32) + m_stream.total_out_lo32);
#else
			return (ZIP_SIZE_TYPE)m_stream.total_out_lo32;
#endif
	}

	ZIP_SIZE_TYPE GetStreamTotalIn()
	{
#ifdef _ZIP_ZIP64
			return (ZIP_SIZE_TYPE)(((ZIP_SIZE_TYPE)m_stream.total_in_hi32 << 32) + m_stream.total_in_lo32);
#else
			return (ZIP_SIZE_TYPE)m_stream.total_in_lo32;
#endif
	}
};

} //namespace

#endif

#endif

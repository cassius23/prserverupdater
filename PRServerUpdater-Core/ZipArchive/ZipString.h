////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

/**
* \file ZipString.h
*	Includes the CZipString class.
*
*/


#ifndef ZIPARCHIVE_ZIPSTRING_DOT_H
#define ZIPARCHIVE_ZIPSTRING_DOT_H

#if _MSC_VER > 1000
	#pragma once
#endif

#include "_platform.h"

#ifdef _ZIP_IMPL_STL
	#include "ZipString_stl.h"
#else
	#include "ZipString_mfc.h"
#endif

namespace ZipArchiveLib
{
	ZIP_API bool IsStringAscii(const CZipString& value);	
};

#endif  /* ZIPARCHIVE_ZIPSTRING_DOT_H */

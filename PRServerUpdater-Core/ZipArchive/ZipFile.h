////////////////////////////////////////////////////////////////////////////////
// This source file is part of the ZipArchive library source distribution and
// is Copyrighted 2000 - 2013 by Artpol Software - Tadeusz Dracz
//
// This source code is licensed as closed source and its use is 
// strictly subject to the terms and conditions of the 
// The ZipArchive Library Commercial License.
// The license is distributed with the source code in the License.txt file.
//
// Web Site: http://www.artpol-software.com
////////////////////////////////////////////////////////////////////////////////

/**
* \file ZipFile.h
*	Includes the CZipFile class.
*
*/


#if !defined(ZIPARCHIVE_ZIPFILE_DOT_H)
#define ZIPARCHIVE_ZIPFILE_DOT_H

#if defined _ZIP_ZIP64
	#if defined __BORLANDC__
		#if _ZIP_FILE_IMPLEMENTATION != ZIP_ZFI_WIN
			#error Zip64 cannot be used under C++Builder without _ZIP_FILE_IMPLEMENTATION not set to ZIP_ZFI_WIN
			#undef _ZIP_ZIP64
		#endif
	#elif defined _MSC_VER && _MSC_VER < 1300 && defined _ZIP_IMPL_MFC
		#if _ZIP_FILE_IMPLEMENTATION != ZIP_ZFI_WIN && _ZIP_FILE_IMPLEMENTATION != ZIP_ZFI_STL
			#error Zip64 cannot be used under Visual Studio 6.0 MFC without _ZIP_FILE_IMPLEMENTATION not set to ZIP_ZFI_WIN or ZIP_ZFI_STL
			#undef _ZIP_ZIP64
		#endif
	#endif 
#endif

#if _MSC_VER > 1000
	#pragma once
#endif

#include "_features.h"

#if (defined _ZIP_IMPL_STL && (!defined _ZIP_FILE_IMPLEMENTATION || _ZIP_FILE_IMPLEMENTATION == ZIP_ZFI_DEFAULT)) || _ZIP_FILE_IMPLEMENTATION == ZIP_ZFI_STL
	#include "ZipFile_stl.h"
#elif _ZIP_FILE_IMPLEMENTATION == ZIP_ZFI_WIN
	#include "ZipFile_win.h"
#else
	#include "ZipFile_mfc.h"
#endif

#endif // !defined(ZIPARCHIVE_ZIPFILE_DOT_H)

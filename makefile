CXX=g++
CXXFLAGS=-Wall -s -Os

DEFINES=-DCURL_STATICLIB

INCLUDE_PATH_CORE=-IPRServerUpdater-Core -I/usr/local/include
INCLUDE_PATH=-IPRServerUpdater -I/usr/local/include

SOURCES_PATH_CORE=PRServerUpdater-Core/
SOURCES_CORE_ZIPARCHIVE=$(SOURCES_PATH_CORE)ZipArchive/Aes.cpp $(SOURCES_PATH_CORE)ZipArchive/BaseLibCompressor.cpp $(SOURCES_PATH_CORE)ZipArchive/Bzip2Compressor.cpp $(SOURCES_PATH_CORE)ZipArchive/DeflateCompressor.cpp $(SOURCES_PATH_CORE)ZipArchive/DirEnumerator.cpp $(SOURCES_PATH_CORE)ZipArchive/FileFilter.cpp $(SOURCES_PATH_CORE)ZipArchive/Hmac.cpp $(SOURCES_PATH_CORE)ZipArchive/RandomPool.cpp $(SOURCES_PATH_CORE)ZipArchive/Sha1.cpp $(SOURCES_PATH_CORE)ZipArchive/Wildcard.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipAesCryptograph.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipArchive.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipAutoBuffer.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipCentralDir.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipCompatibility.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipCompressor.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipCrc32Cryptograph.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipCryptograph.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipException.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipExtraData.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipExtraField.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipFileHeader.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipFile_mfc.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipFile_stl.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipFile_win.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipMemFile.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipPathComponent_lnx.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipPathComponent_win.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipPlatformComm.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipPlatform_lnx.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipPlatform_win.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipStorage.cpp $(SOURCES_PATH_CORE)ZipArchive/ZipString.cpp $(SOURCES_PATH_CORE)ZipArchive/bzip2/blocksort.c $(SOURCES_PATH_CORE)ZipArchive/bzip2/bzcompress.c $(SOURCES_PATH_CORE)ZipArchive/bzip2/bzlib.c $(SOURCES_PATH_CORE)ZipArchive/bzip2/crctable.c $(SOURCES_PATH_CORE)ZipArchive/bzip2/decompress.c $(SOURCES_PATH_CORE)ZipArchive/bzip2/huffman.c $(SOURCES_PATH_CORE)ZipArchive/bzip2/randtable.c $(SOURCES_PATH_CORE)ZipArchive/zlib/adler32.c $(SOURCES_PATH_CORE)ZipArchive/zlib/compress.c $(SOURCES_PATH_CORE)ZipArchive/zlib/crc32.c $(SOURCES_PATH_CORE)ZipArchive/zlib/deflate.c $(SOURCES_PATH_CORE)ZipArchive/zlib/infback.c $(SOURCES_PATH_CORE)ZipArchive/zlib/inffast.c $(SOURCES_PATH_CORE)ZipArchive/zlib/inflate.c $(SOURCES_PATH_CORE)ZipArchive/zlib/inftrees.c $(SOURCES_PATH_CORE)ZipArchive/zlib/trees.c $(SOURCES_PATH_CORE)ZipArchive/zlib/uncompr.c $(SOURCES_PATH_CORE)ZipArchive/zlib/zutil.c
SOURCES_CORE=$(SOURCES_PATH_CORE)main.cpp $(SOURCES_PATH_CORE)BinaryPatch.cpp $(SOURCES_PATH_CORE)Build.cpp $(SOURCES_PATH_CORE)ConsoleUtils.cpp $(SOURCES_PATH_CORE)Directories.cpp $(SOURCES_PATH_CORE)Download.cpp $(SOURCES_PATH_CORE)EncryptedData.cpp $(SOURCES_PATH_CORE)License.cpp $(SOURCES_PATH_CORE)Utils.cpp $(SOURCES_PATH_CORE)Version.cpp $(SOURCES_CORE_ZIPARCHIVE)

SOURCES_PATH=PRServerUpdater/
SOURCES=$(SOURCES_PATH)main.cpp $(SOURCES_PATH_CORE)ConsoleUtils.cpp $(SOURCES_PATH)Download.cpp

LIBRARIES_BOOST_PATH=/usr/local/lib/
LIBRARIES_CRYPTOPP_PATH=/usr/local/lib/
LIBRARIES_CURL_PATH=/usr/local/lib/

LIBRARIES_BOOST=$(LIBRARIES_BOOST_PATH)libboost_filesystem.a $(LIBRARIES_BOOST_PATH)libboost_system.a
LIBRARIES_BOOST_CORE=$(LIBRARIES_BOOST_PATH)libboost_filesystem.a $(LIBRARIES_BOOST_PATH)libboost_program_options.a $(LIBRARIES_BOOST_PATH)libboost_random.a $(LIBRARIES_BOOST_PATH)libboost_regex.a $(LIBRARIES_BOOST_PATH)libboost_system.a
LIBRARIES_CRYPTOPP=$(LIBRARIES_CRYPTOPP_PATH)libcryptopp.a
LIBRARIES_CURL=$(LIBRARIES_CURL_PATH)libcurl.a -lssh2 -lssl -lcrypto -ldl -lz -lrt
LIBRARIES=$(LIBRARIES_CURL) $(LIBRARIES_BOOST) -lpthread
LIBRARIES_CORE=$(LIBRARIES_CURL) $(LIBRARIES_CRYPTOPP) $(LIBRARIES_BOOST_CORE) -lpthread

OUTPUT=Release/PRServerUpdater-linux
OUTPUT_CORE=Release/PRServerUpdater-Core-linux

all:
	@echo Please use "make x86" or "make x64"

x86:
	@echo Building Release x86...
	@$(CXX) -m32 -DLINUX -DNDEBUG $(DEFINES) $(INCLUDE_PATH) $(CXXFLAGS) $(SOURCES) -o $(OUTPUT)32 $(LIBRARIES) -static
	@$(CXX) -m32 -DLINUX -DNDEBUG $(DEFINES) $(INCLUDE_PATH_CORE) $(CXXFLAGS) $(SOURCES_CORE) -o $(OUTPUT_CORE)32 $(LIBRARIES_CORE) -static
	@echo Done!

x64:
	@echo Building Release x64...
	@$(CXX) -DLINUX64 -DNDEBUG $(DEFINES) $(INCLUDE_PATH) $(CXXFLAGS) $(SOURCES) -o $(OUTPUT)64 $(LIBRARIES) -static
	@$(CXX) -DLINUX64 -DNDEBUG $(DEFINES) $(INCLUDE_PATH_CORE) $(CXXFLAGS) $(SOURCES_CORE) -o $(OUTPUT_CORE)64 $(LIBRARIES_CORE) -static
	@echo Done!